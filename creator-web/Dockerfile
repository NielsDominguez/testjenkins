# Using node as intermediate to build
FROM node:8.9.4 as built
LABEL maintainer Narat S <narat.s@hbot.io>

WORKDIR /home/node/app
COPY ./package.json package.json
RUN npm install

COPY . ./
RUN npm run build

RUN cp ./src/app/app.constant.docker.js ./public/app/app.constant.js 

# Using nginx as a final image to run app
FROM nginx:1.13.7-alpine
COPY ./docker.entrypoint.sh /usr/share/nginx/
RUN chmod 777 /usr/share/nginx/docker.entrypoint.sh

WORKDIR /usr/share/nginx/html
COPY --from=built /home/node/app/public .
RUN cat /usr/share/nginx/docker.entrypoint.sh

ENV API_ENDPOINT_URL= FACEBOOK_APP_ID= SITE_URL= FIREBASE_APIKEY= FIREBASE_AUTH_DOMAIN= FIREBASE_DATABASE_URL= FIREBASE_PROJECT_ID= FIREBASE_STORAGE_BUCKET= FIREBASE_MESSAGING_SENDER_ID= ENABLE_SID=

EXPOSE 80
ENTRYPOINT [ "/usr/share/nginx/docker.entrypoint.sh" ]
CMD ["nginx", "-g", "daemon off;"]
