## H-Creator 2019/02

This app allow you to create your own chatbot in an elegant way.

## Prerequsite
- NPM

## Start local server
1. Install all required package using `npm install --dev`
2. Start local server with `npm start`. This will start http server with a config pointed to [creator-server](https://git.unicornonzen.com/hbot-buildabot/creator-server) (http://127.0.0.1:5000)

## Production
**TODO**

## API connection configuration
As an interim solution, we create two `app.constant.js` each per environment.
1. `./src/app/app.constant.prod.js`
2. `./src/app/app.constant.local.js`

The start script for local will make a copy of `.local.js` for you. So does the CI script. Before starting any other server, especially without any well-defined script, make sure you have the right `app.constant.js` placed in `./public/app/`.
