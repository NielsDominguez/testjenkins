(function () {
	'use strict';

	angular
		.module('app')
		.factory('Facebook', Facebook);

	Facebook.$inject = ['$http', '$q', '$cookies', 'ApiEndpointURL', 'Auth', 'AlertService'];

	String.prototype.trunc =
     function( n, useWordBoundary ){
         if (this.length <= n) { return this; }
         var subString = this.substr(0, n-1);
         return (useWordBoundary 
            ? subString.substr(0, subString.lastIndexOf(' ')) 
            : subString) + " ...";
      };

	function Facebook($http, $q, $cookies, ApiEndpointURL, Auth, AlertService) {
		var facebook = {
			fetchFbPages: fetchFbPages,
			getAccessToken: getAccessToken,
			fbLogin: fbLogin,
			fbLogout: fbLogout,
			accountData: '',
			shortLiveToken: '',
			longLiveToken: '',
			userData: '',
			fbLoginStatus: 'notLogined',
			fbPages: '',
			fbAccountAccessToken: '',
			connectPage: connectPage,
			disconnectPage: disconnectPage,
			getLongLiveToken: getLongLiveToken,
			checkToken: checkToken,
			checkTokenPage: checkTokenPage,
			callGetStartButton: callGetStartButton
		};
		return facebook;

		function getAccessToken() {
			return Auth.getApiToken().then(function (token) {
				let option = { 
					headers: { Authorization: token }
				};
				return $http.get(ApiEndpointURL + '/user/facebook', option)
				.then(function (data) {
					if (angular.equals(data.data, {})) {
						return $q.reject();
					}
					return data.data;
				})
				.catch(function (error) {
					AlertService.push(error);
					return $q.reject(error);
				});
			});
		}

		function checkToken(accessTokenAccount) {
			return $http.get('https://graph.facebook.com/me?access_token=' + accessTokenAccount)
				.then(function (data) {
					return data.data
				})
				.catch(function (error) {
					AlertService.push(error.message);
					return $q.reject(error);
				});
		}

		function checkTokenPage(accessTokenPage) {
			return $http.get('https://graph.facebook.com/me?access_token=' + accessTokenPage)
				.then(function (data) {
					return data.data;
				})
				.catch(function (error) {
					AlertService.push(error.message);
					return $q.reject(error);
				});
		}

		function fbLogin() {
			var defer = $q.defer();
			FB.login(function (response) {
				defer.resolve(response.authResponse);
			}, { 
				//scope: 'public_profile,user_friends,email,manage_pages,pages_messaging,publish_pages,read_page_mailboxes' 
				scope: 'email,manage_pages,pages_show_list,publish_pages,read_page_mailboxes,pages_messaging,pages_messaging_phone_number,pages_messaging_subscriptions,public_profile,business_management'
			});
			return defer.promise;
		}

		function getLongLiveToken(fbData) {
			return Auth.getApiToken().then(function (token) {
				let option = { 
					params: { token: fbData.accessToken , userId: fbData.userID }, 
					headers: { Authorization: token }
				};
				return $http
					.get(ApiEndpointURL + '/longLiveToken', option)
					.then(function (data) {
						return data.data;
					})
					.catch(function (error) {
						AlertService.push(error);
						return $q.reject(error);
					});
			});	
		}

		function connectPage(fbPage,  bot_id) {
			const data = {
				bot_id: bot_id,
				page_name: fbPage.name,
				page_id: fbPage.id,
				access_token: fbPage.access_token
			};
			return $http
				.post(ApiEndpointURL + "/graph/facebook/subscribed_apps", data)
				.then((data) => {
					return data.data;
				})
				.catch(function (error) {
					AlertService.push(error);
					return $q.reject(error);
				});
		}

		function disconnectPage(bot_id) {
			return Auth.getApiToken().then(function (token) {
				let option = {
					params: { bot_id: bot_id }, 
					headers: { Authorization: token , "Content-type": "application/json;charset=utf-8"}
				};
				return $http
					.delete(ApiEndpointURL + "/channel/facebook", option)
					.then(function (data) {
						window.location.reload();
						return data;
					})
					.catch(function (error) {
						AlertService.push(error);
						return $q.reject(error);
					});
				});
		}

		function fetchFbPages(accessTokenAccount,bots) {
			//alert(bots);
			//accessTokenAccount = "EAALisuas8kUBAOCqVF8mHGQVtNHxoAlOrZA0cQdqE5jBVLoRkmeUarRV0YXd5FuVN0C5WS0SjOTDJExxZCr6sbPnRAZBfgvIzE0ehgdfeM9oG1ZAGnIoQp5tHKRqZBNl0iP7qaEa4oSlnpwrFutGPafZCGx4SRmDpBd6lGpUZByMOAvy92OXoVnDllnpisaVCXqGzi1C7ciTQZDZD";
			//accessTokenAccount = "EAALisuas8kUBAGct277HsaVdNmOqHv4LRpQqE2zcQUciVFHaCCUM3lViuwsr8qHmOe09ZClbg6AgwIW7ZBk2PXprLZCX2bcx3XiylGpvnWNwe37DzMwqDDoLHcIFkm3jlgZARxvPhAJtVALcZBZClG1sxEdXKH5VdBtZAZBFJze7oDHGZBivbPtPP1ZAD5ZAG5miJuo3yDO7u4CCrZCX5NcbmOPH";
			return $http
				.get('https://graph.facebook.com/v3.2/me/accounts?access_token=' + accessTokenAccount + '&limit=10000000')
				.then(function (data) {
					let botPageId = []
					let botPageName = []
					let botFirebaseId = []
					for (let i=0; i<bots.length; i++) {
						if (bots[i].channels.FACEBOOK.id != "" && bots[i].channels.FACEBOOK.id != undefined) {
							botPageId.push(bots[i].channels.FACEBOOK.id)
							botPageName.push(bots[i].bot_name)
							botFirebaseId.push(bots[i].bot_id)
						}
					}
					let arrayPageFacebook = data.data
					let pageFilter = arrayPageFacebook.data.filter(function( o){
						return botPageId.indexOf(o.id) == -1;
					});
					console.log("botPageName:");
					console.log(botPageName);

					arrayPageFacebook.data.map(page => {
						if(botPageId.indexOf(page.id) != -1) {
							page.isBotConnected = true
							//console.log(botPageName);
							page.botName = botPageName[botPageId.indexOf(page.id)];
							page.firebaseId = botFirebaseId[botPageId.indexOf(page.id)];
							page.connected = "connected";
							page.botUrl = "https://demo.hbot.io/#/bot/" + page.firebaseId + "/blocks";

							
						} else {
							page.isBotConnected = false
							page.firebaseId = ""
							page.botName = ""
							page.connected = ""
							page.botUrl = ""
						}
						let thePageName = page.name
						page.name = thePageName.trunc(21,false)

						return page;

							
					})

					
					arrayPageFacebook.data.forEach(page => {
						//console.log(page);
					})
					//arrayPageFacebook.data = pageFilter
					return arrayPageFacebook;
						
				})
				.catch(function (error) {
					AlertService.push(error.message);
					return error;
				});
		}

		function fbLogout() {
			return Auth.getApiToken().then(function (token) {
				let option = {
					headers: { Authorization: token, "Content-type": "application/json;charset=utf-8"}
				};
				return $http
					.delete(ApiEndpointURL + '/user/facebook', option)
					.then(function (data) {
						AlertService.push('Oh no.... you unlink me from facebook T^T');
						return data.data;
					})
					.catch(function (error) {
						AlertService.push(error);
						return $q.reject(error);
					});
			});	
		}

		function callGetStartButton(fbPage) {
			return Auth.getApiToken().then(function (token) {
				let option = {
						headers: { Authorization: token}
				};
				let data = {
					accessTokenPage: fbPage.access_token,
					pageName: fbPage.name,
				}
				return $http
					.post(ApiEndpointURL + '/graph/facebook/get_started', data,option)
					.then(function (data) {
						return data.data;
					})
					.catch(function (error) {
						AlertService.push(error);
						return $q.reject(error);
					});
			});
		}

	}
})();
