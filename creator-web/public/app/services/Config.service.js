(function () {
  'use strict';

  angular
    .module('app')
    .factory('Config', Config);

  Config.$inject = ['$http', '$q', '$cookies', 'ApiEndpointURL', 'Auth', '$firebaseObject', 'AlertService'];


  function Config($http, $q, $cookies, ApiEndpointURL, Auth, $firebaseObject, AlertService) {
    var BOT_REFERECE = firebase.database().ref('bots');
    var config = {
      getGreeting: getGreeting,
      getBot: getBot,
      changeGreeting: changeGreeting,
      getMenuBurger: getMenuBurger,
      setMenuBurger: setMenuBurger,
      revokeApiToken: revokeApiToken,
      exportBot: exportBot,
      importBot: importBot,
      setWebHook: setWebHook,
      setLivechat: setLivechat,
      getLivechat: getLivechat,
    };
    return config;

    function getBot(bot_id) {
      return Auth.getApiToken().then(function (token) {
        return $http.get(ApiEndpointURL + '/bot/' + bot_id, { headers: { Authorization: token } })
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            AlertService.push(error);
            return error;
          });
      });
    }

    function getMenuBurger(accessTokenPage) {
      var defer = $q.defer();
      var url = 'https://graph.facebook.com/v3.2/me/messenger_profile?fields=persistent_menu&access_token=' + accessTokenPage;

      $http.get(url)
        .then(function (data) {
          defer.resolve(data);
        })
        .catch(function (error) {
          AlertService.push('Some error occur');
          defer.reject(error);
        });

      return defer.promise;
    }

    function setMenuBurger(accessTokenPage, menuBurger) {
      var defer = $q.defer();
      if (menuBurger.persistent_menu[0].call_to_actions.length == 0) {
        var config = {
          "headers": {
            "Content-Type": "application/json"
          },
          "data": {
            "fields": [ "persistent_menu" ]
          }
        }
        $http.delete('https://graph.facebook.com/v3.2/me/messenger_profile?access_token=' + accessTokenPage, config)
          .then(function (data) {
            defer.resolve(data);
          })
          .catch(function (error) {
            console.log(error);
            console.log(error.message);
            AlertService.push('Some error occur.');
            defer.reject(error);
          });
      } else {
        // console.log(menuBurger);
        // menuBurger.persistent_menu[0].call_to_actions.push({"title":"Powered by Hbot.io 🤖","type":"web_url","url":"https://hbot.io","webview_height_ratio":"full"});
        // console.log(menuBurger);
        menuBurger.whitelisted_domains = [];
        menuBurger.whitelisted_domains[0] = "https://hbot.io";
        menuBurger.whitelisted_domains[1] = "https://my.hbot.io";
        menuBurger.whitelisted_domains[2] = "https://sid.hbot.io";
        $http.post('https://graph.facebook.com/v3.2/me/messenger_profile?access_token=' + accessTokenPage, menuBurger)
          .then(function (data) {
            defer.resolve(data);
          })
          .catch(function (error) {
            console.log(error.message);
            AlertService.push('Some error occur.');
            defer.reject(error);
          });
      }

      return defer.promise;
    }

    function getGreeting(accessTokenPage) {
      var defer = $q.defer();

      $http.get('https://graph.facebook.com/v3.2/me/messenger_profile?fields=greeting&access_token=' + accessTokenPage)
        .then(function (data) {
          defer.resolve(data);
        })
        .catch(function (error) {
          AlertService.push('Cannot get greeting message from Facebook.');
          defer.reject(error);
        });

      return defer.promise;
    }

    function changeGreeting(accessTokenPage, greeting) {
      var defer = $q.defer();

      $http.post('https://graph.facebook.com/v3.2/me/messenger_profile?access_token=' + accessTokenPage, greeting)
        .then(function (data) {
          defer.resolve(data);
        })
        .catch(function (error) {
          AlertService.push('Cannot change greeting message.');
          defer.reject(error);
        });

      return defer.promise;
    }

    function revokeApiToken(botId) {
      return Auth.getApiToken().then(function (token) {
        return $http.delete(ApiEndpointURL + '/bot/' + botId + '/revoke', { headers: { Authorization: token } })
          .then(function (data) {
            AlertService.push("REVOKE!!");
            return data.data;
          })
          .catch(function (error) {
            AlertService.push(error);
            return error;
          });
      });
    }

    function exportBot(bot_id, exportOption) {
      return Auth.getApiToken().then(function (token) {
        let option = {
          headers: { Authorization: token }
        };
        
        //if () {
          var exportOption = {
            flow: $("input[name='flow']").attr("class") == "active" ? "t" : "f",
            ai: $("input[name='ai']").attr("class") == "active" ? "t" : "f"
          }
          console.log(exportOption);
        //}
        return $http.get(ApiEndpointURL + '/bot/' + bot_id + '/export?flow=' + exportOption.flow + "&ai=" + exportOption.ai, option)
          .then(function (data) {
            var now = new Date();
            var filename = now.getDate() + "" + now.getMonth() + "" + now.getFullYear() + "_" + now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + ".hbot";
            var anchor = document.createElement('a');
            anchor.href = 'data:attachment;charset=utf-8,' + encodeURI(data.data);
            anchor.target = '_blank';
            anchor.download = filename;
            document.body.appendChild(anchor);
            anchor.click();
            anchor.remove();
            return 'success';
          })
          .catch(function (error) {
            AlertService.push(error);
            return $q.reject(error);
          });
      });
    }

    function importBot(bot_id, file) {
      return Auth.getApiToken().then(function (token) {
        let option = {
          headers: { Authorization: token }
        };
        return $http.post(ApiEndpointURL + '/bot/' + bot_id + '/import', { data: file }, option)
          .then(function (data) {
            AlertService.push('Wow! You have new bot data O_O');
            return 'success';
          })
          .catch(function (error) {
            AlertService.push(error);
            return $q.reject(error);
          });
      });
    }

    function setWebHook(webHookUrl, webHookHeaderKey, webHookHeaderValue, bot_id, api_token) {
      return Auth.getApiToken().then(function (token) {
        let option = {
          headers: { Authorization: token }
        };
        return $http.post(ApiEndpointURL + '/bot/' + bot_id + '/webHook', { url: webHookUrl, headerKey: webHookHeaderKey, headerValue: webHookHeaderValue, apiToken: api_token }, option)
          .then(function (data) {
            AlertService.push("Oh yeah!, web hook has been set");
            return 'success';
          })
          .catch(function (error) {
            AlertService.push(error);
            return $q.reject(error);
          });
      });
    }

    function getLivechat(bot_id) {
      var defer = $q.defer();
      var ref = BOT_REFERECE.child('bot_'+bot_id).child('livechat');
      var obj = $firebaseObject(ref);

      obj.$loaded()
      .then(function (data) {
        defer.resolve(data);
      })
      .catch(function (error) {
        AlertService.push(error.message);
        defer.reject(error);
      });

      return defer.promise;
    }

    function setLivechat(startKeyword, stopKeyword, timeout, enable, bot_id, api_token) {
      var ref = BOT_REFERECE.child('bot_'+bot_id).child('livechat');
      ref.set({
        "start_keyword": startKeyword,
        "stop_keyword": stopKeyword,
        "timeout": timeout,
        "enable": enable,
      }).then(function(){
        return firebase.database().ref('isUpdate').child('bot_' + bot_id).set(true);
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  }
})();
