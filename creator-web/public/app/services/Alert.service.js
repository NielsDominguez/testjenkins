(function () {
  'use strict';

  angular
    .module('app')
    .service('AlertService', AlertService);

  AlertService.$inject = ['$timeout'];

  function AlertService($timeout) {
    var self = this;
    self.messages = [];
    self.callback;
    self.push = push;
    self.get = get;
    self.subscribe = subscribe;

    function get() {
      return self.messages;
    }

    function push(message) {
      self.messages.push(message);
      if (self.callback) {
        self.callback(self.messages);
      }
      $timeout(() => {
        self.messages.shift();
        if (self.callback) {
          self.callback(self.messages);
        }
      }, 8000);
    }

    function subscribe(callback) {
      self.callback = callback;
    }
  }

})();