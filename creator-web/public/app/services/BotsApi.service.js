(function () {
  angular.module('app')
    .service('BotsApi', BotsApi);

  BotsApi.$inject = ['$http', 'ApiEndpointURL', 'Auth', 'AlertService'];

  function BotsApi($http, ApiEndpointURL, Auth, AlertService) {
    var self = this;
    self.addAdmin = addAdmin;
    self.removeAdmin = removeAdmin;
    self.getBot = getBot;

    function addAdmin(email, botId) {
      return Auth.getApiToken().then(function (token) {
        return $http.put(ApiEndpointURL + '/bot/' + botId + '/admin', { email: email }, getAuthorizationHeader(token))
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            AlertService.push(error);
          });
      });
    }

    function removeAdmin(userId, botId) {
      return Auth.getApiToken().then(function (token) {
        return $http.delete(ApiEndpointURL + '/bot/' + botId + '/admin', getAuthorizationHeader(token, { uid: userId }))
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            AlertService.push(error)
          });
      });

    }

    function getBot(bot_id) {
      return Auth.getApiToken().then(function (token) {
        return $http.get(ApiEndpointURL + '/bot/' + bot_id, { headers: { Authorization: token } })
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            AlertService.push(error);
            return error;
          });
      });
    }

    function getAuthorizationHeader(token, params) {
      return {
        headers: {
          Authorization: token
        },
        params: params
      };
    }
  }
})();