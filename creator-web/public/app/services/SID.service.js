(function () {
  'use strict';

  angular
    .module('app')
    .service('SIDService', SIDService);

  SIDService.$inject = ['$location'];

  function SIDService($location) {
    var self = this;
    self.isSID = isSID;
    self.getLogo = getLogo;
    self.register = register;

    function isSID() {
      return $location.host() === "sid.hbot.io";
    }
    function getLogo() {
      if (isSID()) {
        return {
          src: "./assets/images/hbot-x-sid.png",
          width: 210
        };
      } else {
        return {
          src: "./assets/images/logo-hbot.png",
          width: 140
        };
      }
    }
    function register(uid) {
      if (isSID()) {
        return firebase.database().ref("sidusers/" + uid).set(true); 
      }
    }
  }

})();