(function () {
	'use strict';

	angular
		.module('app')
		.factory('Bots', Bots);

	Bots.$inject = ['$firebaseObject', '$firebaseArray', '$q', '$state', '$http', 'ApiEndpointURL', 'AlertService', 'Auth', 'SIDService'];

	function Bots($firebaseObject, $firebaseArray, $q, $state, $http, ApiEndpointURL, AlertService, Auth, SIDService) {
		/**
		 * Bot Referece is refer to the based path for bot
		 * database in firebase.
		 */
		var BOT_REFERECE = firebase.database().ref('bots');
		var bot = {};

		var botServices = {
			init: init,
			getBot: getBot,
			deleteBot: deleteBot,
			getBlock: getBlock,
			addBlock: addBlock,
			addCopyBlock: addCopyBlock,
			addNode: addNode,
			addNodeBroadcast: addNodeBroadcast,
			getNodes: getNodes,
			getBroadcastNodes: getBroadcastNodes,
			deleteNode: deleteNode,
			addCarousel: addCarousel,
			addAIRule: addAIRule,
			addBroadcast: addBroadcast,
			getBroadcast: getBroadcast,
			getSubscribMessagingStatus: getSubscribMessagingStatus,
			getPageId: getPageId,
			update: update,
		}
		return botServices;

		/**
		 * Initial bot if there is not bot id in the db.
		 * @param bot_id
		 */
		function init(bot, uid) {
			var ref = BOT_REFERECE.child('bot_' + bot.bot_id);
			// var obj = $firebaseObject(ref);
			var obj = {};
			// Initial value
			obj.bot_id = bot.bot_id;
			obj.bot_name = bot.bot_name;
			obj.start_block = '0';
			obj.default_block = ['1'];
			update('bot_' + bot.bot_id)
			obj.sid = SIDService.isSID();
			obj.users = {
				[uid]: true
			}

			// Welcom message blocks
			var b_block_0_nodeID = _guid();
			var b_block_0_content = {
				['N0_' + b_block_0_nodeID]: {
					node_id: '0_' + b_block_0_nodeID,
					order: 0,
					node_type: 'node',
					nodeResponse: {
						type: 'text',
						response: 'This is \"Welcome Message\" block. This block normally shows greeting message and leads the users to understand how chatbot could help you.'
					}
				}
			}

			// Default message block
			var b_block_1_nodeID = _guid();
			var b_block_1_content = {
				['N0_' + b_block_1_nodeID]: {
					node_id: '0_' + b_block_1_nodeID,
					order: 0,
					node_type: 'node',
					nodeResponse: {
						type: 'text',
						response: 'This is message from \"Default Message\" block'
					}
				}
			}

			// Default live chat block
			var b_block_2_nodeID = _guid();
			var b_block_2_content = {
				['N0_' + b_block_2_nodeID]: {
					node_id: '0_' + b_block_2_nodeID,
					order: 0,
					node_type: 'node',
					nodeResponse: {
						type: 'text',
						response: 'live chat finished'
					}
				}
			}

			// Initial Buildin Blocks
			obj.blocks = {
				block0: {
					block_id: '0',
					block_name: 'Welcome Message',
					bot_id: bot.bot_id,
					start_node: 'N0_' + b_block_0_nodeID,
					nodes: b_block_0_content,
					group: 'build0'
				},
				block1: {
					block_id: '1',
					block_name: 'Default Message',
					bot_id: bot.bot_id,
					start_node: 'N0_' + b_block_1_nodeID,
					nodes: b_block_1_content,
					group: 'build0'
				},
				block2: {
					block_id: '2',
					block_name: 'Default Live Chat',
					bot_id: bot.bot_id,
					start_node: 'N0_' + b_block_2_nodeID,
					nodes: b_block_2_content,
					group: 'build0'
				}
			};
			obj.groups = [{
				"key": 'build0',
				"name": "Build-in Blocks",
					"blocks": [{
						block_id: '0',
						block_name: 'Welcome Message'
					},{
						block_id: '1',
						block_name: 'Default Message'
					},{
						block_id: '2',
						block_name: 'Default Live Chat'
					}]
			},{
				"key": _guid(),
				"name": "Default Group"
			}]
			obj.attributes = {
				[_guid()]: 'first_name',
				[_guid()]: 'last_name',
				[_guid()]: 'profile_pic',
				[_guid()]: 'locale',
				[_guid()]: 'timezone',
				[_guid()]: 'gender'
			}
			return ref.set(obj);
		}

		/**
		 * Get but by bot_id
		 * @param bot_id
		 */
		function getBot(bot_id) {
			var ref = BOT_REFERECE.child('bot_' + bot_id);
			var obj = $firebaseObject(ref);
			if (!obj.bot_id) {
				obj.$loaded()
					.catch(function() {
						$state.go('dashboard');
					})
				this.bot = obj;
			}
			return this.bot;
		}

		function deleteBot(bot_id) {
			return firebase.database().ref('bots').child('bot_' + bot_id)
				.remove()
				.then(function() {
					AlertService.push('Bot has been deleted');
				});
		}

		/**
		 * Get block content by block_id
		 * @param block_id
		 */
		function getBlock(block_id) {
			var defer = $q.defer();
			var ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child('block' + block_id);

			var obj = $firebaseObject(ref);
			obj.$loaded()
				.then(function (data) {
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		function getNodes(block_id) {
			var defer = $q.defer();
			var ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child('block' + block_id).child('nodes');

			var obj = $firebaseArray(ref);
			obj.$watch((event) => {
				obj.sort((a, b) => {
					return a.order - b.order;
				});
			});
			obj.$loaded()
				.then(function (data) {
					data.sort((a, b) => {
						return a.order - b.order;
					});
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});
			return defer.promise;
		}
		function getBroadcastNodes(broadcast_id) {
			var defer = $q.defer();
			var ref = BOT_REFERECE.child(this.bot.$id).child('broadcast').child('BR_' + broadcast_id).child('nodes');

			var obj = $firebaseArray(ref);
			obj.$loaded()
				.then(function (data) {
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		/**
		 * Add new block to the blocks.
		 * @param block
		 */
		function addBlock(group_key) {
			var defer = $q.defer();
			var content;

			if(group_key === 'build0'){
				content = _initLiveChatContent(this.bot);
			}
			else 
				content = _initBlockContent(this.bot);

			content.body.group = group_key;

			var ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child(content.key);
			var bot = this.bot;
			ref.set(content.body)
				.then(function (data) {
					defer.resolve(content.body);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		function addCopyBlock(group_key, nodes) {
			var defer = $q.defer();
			var content = _initBlockCopy(this.bot, nodes);
			content.body.group = group_key;

			var ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child(content.key);
			var bot = this.bot;
			ref.set(content.body)
				.then(function (data) {
					defer.resolve(content.body);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		function addNode(type, block) {
			var node_content = _initNodeContent(type, block);
			var block_ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child(block.$id);

			if (block.nodes) {
				var last_node = findLast(block.nodes);
				if (last_node.node_type === 'gotoblock') {
					var ref = block_ref.child('nodes').child('N' + last_node.node_id).child('directions').child(last_node.directions.length);
					ref.set({
						"goto": node_content.key,
						"condition": null
					}).catch(function (error) {
						AlertService.push(error.message);
						console.log(error);
					});
				} else {
					var ref = block_ref.child('nodes').child('N' + last_node.node_id).child('directions');
					ref.set([{
						"goto": node_content.key,
						"condition": null
					}]).catch(function (error) {
						AlertService.push(error.message);
						console.log(error);
					});
				}
			}
			ref = block_ref.child('nodes').child(node_content.key);
			ref.set(node_content.body)
				.then(function (data) {
					if (_.size(block.nodes) == 1) {
						block_ref.child('start_node').set(node_content.key);
					}
				})
				.catch(function (error) {
					AlertService.push(error.message);
					console.log(error);
				})
		}

		function addNodeBroadcast(type, broadcast) {
			var node_content = _initNodeContent(type, broadcast);
			if(!broadcast.nodes) {
				broadcast.nodes = [];
			}
			broadcast.nodes.push(node_content.body);
			broadcast.$save()
				.catch(function (error) {
					AlertService.push(error.message);
					console.log(error);
				})
		}

		function deleteNode(node, block) {
			var n = 'N' + node.node_id;
			var block_ref = BOT_REFERECE.child(this.bot.$id).child('blocks').child(block.$id);

			var ref = block_ref.child('nodes').child(n);
			ref.remove()
				.then(function (data) {
					nodeReorder(block_ref);
					var last_node = findLast(block.nodes);
					var ref = block_ref.child('nodes').child('N' + last_node.node_id).child('directions');
					ref.set([{
						"goto": null,
						"condition": null
					}]).catch(function (error) {
						AlertService.push(error.message);
						console.log(error);
					})
				})
				.catch(function (error) {
					AlertService.push(error.message);
					console.log(error);
				})

		}

		function addCarousel(node) {
			node.nodeResponse.response.push({
				title: '',
				subtitle: '',
				buttons: []
			});
		}

		function nodeReorder(block_ref) {
			var obj = $firebaseObject(block_ref);
			obj.$loaded()
				.then(function (block) {
					var i = 0;
					var first_node = '';
					var prevNode = null;

					for (var node in block.nodes) {
						if (prevNode) {
							block.nodes[prevNode].directions[0] = {
								"goto": node,
								"condition": ""
							}
						}
						if (i === 0) {
							first_node = node;
						}
						block.nodes[node].order = i;
						i++;
						prevNode = node;
					}
					if (first_node.length > 0) {
						block.start_node = first_node;
					}
					block.$save()
						.catch(function (error) {
							AlertService.push(error.message);
						});
				})
				.catch(function (error) {
					AlertService.push(error.message);
				});
		}

		/**
		 * @Private
		 * Generate Unique IDs for later use.
		 */
		function _guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			}
			return s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4();
		}

		/**
		 * @Private
		 * Gererate block content.
		 */
		function _initBlockContent(bot) {
			var blockLength = (bot.blocks) ? _.size(bot.blocks) : '0';
			var block_id = _guid();
			var block = 'block' + blockLength + '_' + block_id;

			return {
				key: block,
				body: {
					block_id: blockLength + '_' + block_id,
					block_name: "Untitled block",
					bot_id: bot.bot_id
				}
			}
		}

		function _initLiveChatContent(bot) {
			var blockLength = (bot.blocks) ? _.size(bot.blocks) : '0';
			var block_id = "2";
			var block = 'block2';

			return {
				key: block,
				body: {
					block_id: block_id,
					block_name: "Default Live Chat",
					bot_id: bot.bot_id
				}
			}
		}


		function _initBlockCopy(bot,nodes){
			var copyNode = {};
			var blockLength = (bot.blocks) ? _.size(bot.blocks) : '0';
			var block_id = _guid();
			var block = 'block' + blockLength + '_' + block_id;

			let buffer;
			for(var i=0;i<nodes.length;i++){

				var nodeLength = (block.nodes) ? _.size(block.nodes) : 0;
				var node_id = i + '_' + _guid();
				var node = 'N' + node_id;
				
				buffer = _.cloneDeep(nodes[i]);
				buffer.node_id = node_id;
				delete buffer.$id;
				delete buffer.$priority;
							
				copyNode[node] = buffer;
			}

			return {
				key: block,
				body: {
					block_id: blockLength + '_' + block_id,
					block_name: "Copy block",
					bot_id: bot.bot_id,
					nodes: copyNode
				}
			}
		}

		function _initNodeContent(type, block) {
			var nodeLength = (block.nodes) ? _.size(block.nodes) : 0;
			var node_id = _guid();
			var node = 'N' + nodeLength + '_' + node_id;
			var retval = {};

			if (type === 'input') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						nodeResponse: {
							type: "text",
							response: ""
						},
						userResponse: {
							type: "text",
							save_to_variable: ""
						},
						nodeErrorResponse: {
							response: "Please make sure your input is number and try again"
						}
					}
				}
			} else if (type === 'set_attr') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						todo: [{
							'attr_name': '',
							'attr_value': ''
						}],
					},
				}
			} else if (type === 'gotoblock') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						directions: [{
							conditions: [{
								connection: "and",
								first_var: "",
								second_var: "",
								operation: ""
							}],
							goto: []
						}]
					}
				}
			} else if (type === 'carousel') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						image_aspect_ratio: 'horizontal',
						node_type: type,
						nodeResponse: {
							type: type,
							response: [{
								title: '',
								subtitle: '',
							}]
						}
					}
				}
			} else if (type === 'typing') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						nodeResponse: {
							type: type,
							response: ''
						},
						directions: [],
						delaytime: 0
					}
				}
			} else if (type === 'text') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: 'node',
						nodeResponse: {
							type: type,
							response: ''
						},
						directions: []
					}
				}
			} else if (type === 'jsonapi') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						method: 'get',
						url: '',
						url_param: [{
							key: '',
							value: ''
						}]
					}
				}
			} else if (type === 'timestamp') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						'attr_name': '',
					}
				}
			} else if (type === 'gettime') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
					}
				} 
			} else if (type === 'math') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						first_value: '',
						second_value: '',
						result_attr: '',
						operation: '',
					}
				}
			} else if (type === 'gmail') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						email_username: '',
						email_password: '',
						email_to: '',
						email_subject: '',
						email_body: '',
						email_body_type: 'text'
					}
				}
			} else if (type === 'email') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						email_username: '',
						email_password: '',
						email_to: '',
						email_subject: '',
						email_body: '',
						email_body_type: 'text',
						email_smtp_host: '',
						email_smtp_port: '',
						email_smtp_ssl: true
					}
				}
			} else if (type === 'livechat') {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: type,
						sleepTime: 5,
					}
				}
			} else {
				retval = {
					key: node,
					body: {
						node_id: nodeLength + '_' + node_id,
						order: nodeLength,
						node_type: 'node',
						nodeResponse: {
							type: type,
							response: ''
						}
					}
				}
			}
			return retval;
		}

		function addAIRule() {
			var ref = {};
			if (this.bot.ai) {
				ref = BOT_REFERECE.child(this.bot.$id).child('ai').child(this.bot.ai.length);
			} else {
				ref = BOT_REFERECE.child(this.bot.$id).child('ai').child('0');
			}

			var obj = $firebaseObject(ref);
			// obj.filters = ['Welcome'];
			obj.replyType = 'text';
			// obj.blocks = [{ name: 'Welcome Message', block_id: '0' }];
			obj.$save()
				.catch(function (error) {
					AlertService.push(error.message);
				});
		}

		function _initBroadcastContent(bot_id) {
			var broadcast_id = _guid();

			var retval = {
				key: 'BR_' + broadcast_id,
				body: {
					bot_id: bot_id,
					enable: false,
					broadcast_id: broadcast_id,
					broadcast_type: "one_time",
					broadcast_time: new Date().getTime(),
					broadcast_interval: "0",
					broadcast_conditions: [
						{
							connection: "and",
							first_var: "",
							second_var: "",
							operation: ""
						}
					],
					is_random: false,
				}
			}

			return retval;
		}

		function getBroadcast(broadcast_id) {
			var defer = $q.defer();

			var ref = BOT_REFERECE.child(this.bot.$id).child('broadcast').child('BR_' + broadcast_id);
			var obj = $firebaseObject(ref);
			obj
				.$loaded()
				.then(function (data) {
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		function getSubscribMessagingStatus(accessTokenPage){
			var defer = $q.defer();
			var uri = `https://graph.facebook.com/v2.11/me/messaging_feature_review?access_token=${accessTokenPage}`
			
			$http.get(uri)
			  .then(function (data) {
				defer.resolve(data.data);
			  })
			  .catch(function (error) {
				console.error('Cannot get subscribtion messaging status')
				// AlertService.push('Cannot get subscribtion messaging status');
				defer.reject(error);
			  });
	  
			return defer.promise;
		}

		function addBroadcast(bot_id) {
			var defer = $q.defer();
			var broadcastContent = _initBroadcastContent(bot_id);

			var ref = BOT_REFERECE.child(this.bot.$id).child('broadcast').child(broadcastContent.key);

			var obj = $firebaseObject(ref);
			obj.bot_id = broadcastContent.body.bot_id;
			obj.broadcast_id = broadcastContent.body.broadcast_id;
			obj.broadcast_type = broadcastContent.body.broadcast_type;
			obj.broadcast_time = broadcastContent.body.broadcast_time;
			obj.broadcast_interval = broadcastContent.body.broadcast_interval;
			obj.broadcast_conditions = broadcastContent.body.broadcast_conditions;
			obj.is_random = broadcastContent.body.is_random;
			obj
				.$save()
				.catch(function (error) {
					AlertService.push(error.message);
					console.log(error);
				})
		}

		function getPageId(bot_id, token) {
			var defer = $q.defer();
			$http.get(ApiEndpointURL + '/bot/' + bot_id, { headers: { Authorization: token } })
				.then(function (response) {
					defer.resolve(response.data.channels.FACEBOOK.id)
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});

			return defer.promise;
		}

		function findLast(nodes) {
			let nodeLength = _.size(nodes);
			return _.find(nodes, { order: nodeLength - 1 });
		}

		function update(bot_id) {
			if(!bot_id) {
				return $q.reject();
			}

			var ref = firebase.database().ref('isUpdate').child(bot_id)
			return ref.set(true);
		}

	}
})();
