(function () {
	'use strict';

	angular.module('app')
		.service('Analytics', Analytics);

	Analytics.$inject = ['$q', '$http', 'ApiEndpointURL', 'Auth', 'AlertService', '$filter'];

	function Analytics($q, $http, ApiEndpointURL, Auth, AlertService, $filter) {
		var self = this;
		self.getUsers = getUsers;
		self.getUsersByFilter = getUsersByFilter;
		self.getReport = getReport;
		self.getPager = getPager;


		function getReport(botId) {
			return Auth.getApiToken().then(function (token) {
				return $http.get(ApiEndpointURL + '/bot/' + botId + '/report/summary', getAuthorizationHeader(token))
				.then(function (res) {
					var returnValue = {
						time: [],
						countNewUser: [],
						countMessageIn: [],
						countMessageOut: [],
						activeUserCount: []
					};
					res.data.result.forEach(function(d) {
						returnValue.time.push($filter("date")(d.time, "dd/MM/yyyy"));
						returnValue.countNewUser.push(d.countNewUser);
						returnValue.countMessageIn.push(d.countMessageIn);
						returnValue.countMessageOut.push(d.countMessageOut);
						returnValue.activeUserCount.push(d.activeUserCount);
					})
					
					return returnValue;
				});
			})
		}

		function getPager(totalItems, currentPage, pageSize) {
			// default to first page
			currentPage = currentPage || 1;
		
			// default page size is 10
			pageSize = pageSize || 10;
		
			// calculate total pages
			var totalPages = Math.ceil(totalItems / pageSize);
		
			var startPage, endPage;
			if (totalPages <= 10) {
				// less than 10 total pages so show all
				startPage = 1;
				endPage = totalPages;
			} else {
				// more than 10 total pages so calculate start and end pages
				if (currentPage <= 6) {
					startPage = 1;
					endPage = 10;
				} else if (currentPage + 4 >= totalPages) {
					startPage = totalPages - 9;
					endPage = totalPages;
				} else {
					startPage = currentPage - 5;
					endPage = currentPage + 4;
				}
			}
		
			// calculate start and end item indexes
			var startIndex = (currentPage - 1) * pageSize;
			var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
		
			// create an array of pages to ng-repeat in the pager control
			var pages = _.range(startPage, endPage + 1);
		
			// return object with all pager properties required by the view
			return {
				totalItems: totalItems,
				currentPage: currentPage,
				pageSize: pageSize,
				totalPages: totalPages,
				startPage: startPage,
				endPage: endPage,
				startIndex: startIndex,
				endIndex: endIndex,
				pages: pages
			};
		}

		function getUsers(botId, page, size) {
			return Auth.getApiToken().then(function (token) {
				return $http.get(`${ApiEndpointURL}/bot/${botId}/report/user?page=${page}&size=${size}`, getAuthorizationHeader(token))
					.then(function (res) {
						return res.data;
					});
			});
		}

		function getUsersByFilter(filter, botId, page) {
			return Auth.getApiToken().then(function(token) {
				return $http.post(ApiEndpointURL + '/bot/' + botId + `/report/user?page=${page}`, {filter: filter}, getAuthorizationHeader(token))
				.then(function (res) {
					return res.data;
				})
				.catch(function (error) {
					AlertService.push(error);
				});
			});
		}

		function getAuthorizationHeader(token) {
			return { headers: { Authorization: token } }
		}
	}
})();


