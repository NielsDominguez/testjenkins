(function () {
	'use strict';

	angular
		.module('app')
		.service('Auth', Auth);

	Auth.$inject = ['$http', '$q', '$cookies', 'ApiEndpointURL', 'FirebaseAuth', '$state', 'SiteURL', 'AlertService', 'SIDService'];

	function Auth($http, $q, $cookies, ApiEndpointURL, firebaseAuth, $state, SiteURL, AlertService, SIDService) {
		var stateChangedCallbacks = new Set([defaultStateChanged]);
		var authServices = {
			signUpFirebaseEmail: signUpFirebaseEmail,
			signInFirebaseEmail: signInFirebaseEmail,
			subscribeAuthStateChanged: subscribeAuthStateChanged,
			signOutFirebase: signOutFirebase,
			forgetPassword: forgetPassword,
			updatePassword: updatePassword,
			getUser: getUser,
			getApiToken: getApiToken,
			requireSignInWithEmailVerification: requireSignIn,
			sendEmailVerification: sendEmailVerification,
			linkFacebook: linkFacebook,
			unlinkFacebook: unlinkFacebook,
			getUserFacebook: getUserFacebook
		};

		firebaseAuth.$onAuthStateChanged(function (firebaseUser) {
			stateChangedCallbacks.forEach(function (callback) {
				callback(firebaseUser);
			});
		});

		function signUpFirebaseEmail(email, password) {
			var defer = $q.defer();
			firebaseAuth.$createUserWithEmailAndPassword(email, password)
				.then(function (user) {
					SIDService.register(user.uid);
					sendEmailVerification();
					user.getIdToken()
						.then(function (token) {
							$http.post(ApiEndpointURL + '/signup', user, { headers: { Authorization: token } })
								.then(function (data) {
									defer.resolve(data.data)
								})
								.catch(function (error) {
									AlertService.push(error);
									defer.reject(error);
								});
						});
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(convertSignUpErrorMessage(error));
				});
			return defer.promise;
		}

		function signInFirebaseEmail(email, password) {
			return firebaseAuth.$signInWithEmailAndPassword(email, password)
				.catch(function (error) {
					return $q.reject(convertSignInErrorMessage(error));
				});
		}

		function signOutFirebase() {
			return firebaseAuth.$signOut();
		}

		function forgetPassword(email) {
			return firebaseAuth.$sendPasswordResetEmail(email)
				.then(function() {
					AlertService.push("Password reset email sent successfully!");
				}).catch(function(error) {
					AlertService.push(error);
					return error;
				});
		}

		function getUser() {
			return firebaseAuth.$getAuth();
		}

		function getApiToken() {
			var defer = $q.defer();
			getUser().getIdToken().then(function (data) {
				defer.resolve(data);
			}, function (err) {
				defer.reject(err);
			});
			return defer.promise;
		}

		function requireSignIn() {
			return firebaseAuth.$requireSignIn(true).then(function (auth) {
				if (!auth.emailVerified) {
					AlertService.push("Verify your email, please... :D");
					return $q.reject("EMAIL_VERIFICATION_REQUIRED");
				}
				return auth;
			});
		}

		function sendEmailVerification() {
			var defer = $q.defer();
			var verificationSetting;
			if (SiteURL) {
				verificationSetting = { url: SiteURL, handleCodeInApp: false };
			}
			getUser().sendEmailVerification(verificationSetting)
				.then(function (data) {
					AlertService.push('Email has sent :P');
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error.message);
				});
			return defer.promise;
		}

		function updatePassword(newPassword) {
			return firebaseAuth
				.$updatePassword(newPassword)
				.then(function () {
					AlertService.push('Your password has been updated :P');
				})
				.catch(function (error) {
					AlertService.push(error.message);
					return error;
				});
		}

		function linkFacebook() {
			var defer = $q.defer();
			var provider = new firebase.auth.FacebookAuthProvider();
			provider.addScope('user_friends');
			provider.addScope('email');
			provider.addScope('manage_pages');
			provider.addScope('pages_show_list');
			provider.addScope('pages_messaging');
			//manage_pages,pages_show_list
			alert("linkFacebook()");
			getUser().linkWithPopup(provider)
				.then(function(data) {
					AlertService.push('Facebook has been linked');
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});
			return defer.promise;
		}

		function unlinkFacebook() {
			var defer = $q.defer();
			getUser().unlink('facebook.com')
				.then(function(data) {
					AlertService.push('Oh no.... you unlink me from facebook T^T');
					defer.resolve(data);
				})
				.catch(function (error) {
					AlertService.push(error.message);
					defer.reject(error);
				});
			return defer.promise;
		}

		function getUserFacebook() {
			return getUser().providerData.find(function (provider) {
				return provider.providerId === 'facebook.com';
			});
		}

		function subscribeAuthStateChanged(callback) {
			stateChangedCallbacks.add(callback);
			return unsubscribeAuthStateChanges(callback);
		}

		function unsubscribeAuthStateChanged(callback) {
			return function () {
				stateChangedCallbacks.delete(callback);
			};
		}

		function defaultStateChanged(user) {
			if (!user) {
				if ($state.is('auth.signup')) {
					return;
				}
				$state.go('auth.signin');
			}
		}

		function convertSignUpErrorMessage(error) {
			return signUpErrorMessage[error.code] || 'Please contact HBot customer service team with this error code 501.';
		}

		function convertSignInErrorMessage(error) {
			return signInErrorMessage[error.code] || 'Please contact HBot customer service team with this error code 501.'
		}

		var signUpErrorMessage = {
			'auth/email-already-in-use': 'There already exists an account with the given email address.',
			'auth/invalid-email': 'The email address is not valid.',
			'auth/weak-password': 'The password is not strong enough.',
		};

		var signInErrorMessage = {
			'auth/invalid-email': 'The email address is not valid.',
			'auth/user-disabled': 'The user corresponding to the given email has been disabled.',
			'auth/user-not-found': 'There is no user corresponding to the given email.',
			'auth/wrong-password': 'The password is invalid for the given email, or the account corresponding to the email does not have a password set.',
		};

		return authServices;
	}
})();