(function () {
	'use strict';

	angular
		.module('app')
		.controller('botCtrl', botCtrl);

	botCtrl.$inject = ['$scope', '$state', '$stateParams', '$http', '$timeout', 'Bots', '$cookies', 'ApiEndpointURL', 'Config', 'currentAuth', '$window', 'AlertService'];

	function botCtrl($scope, $state, $stateParams, $http, $timeout, Bots, $cookies, ApiEndpointURL, Config, currentAuth, $window, AlertService) {
		var vm = this;

		vm.timer;

		// Initial variables
		vm.bot_id = $scope.bot_id;
		vm.bot = {};
		vm.currentBlock = {};
		vm.pageId = '';
		vm.currentGroupKey;
		vm.currentFromGotoblock = [];

		// Initial Methods
		// Group
		vm.addGroup = addGroup;
		vm.deleteGroup = deleteGroup;

		// Blocks
		vm.addBlock = addBlock;
		vm.deleteBlock = deleteBlock;
		vm.deleteBlockGotoBlock = deleteBlockGotoBlock
		vm.saveBlock = saveBlock;
		vm.saveGroup = saveGroup;
		vm.copySuccess = copySuccess;
		vm.copyBlock = copyBlock;
		vm.saveIndexBlock = saveIndexBlock;

		// Node
		vm.getBlock = getBlock;
		vm.addNode = addNode;
		vm.deleteNode = deleteNode;
		vm.saveNode = saveNode;
		vm.addReply = addReply;
		vm.toggleAI = toggleAI;
		vm.deleteReply = deleteReply;
		vm.addCarousel = addCarousel;
		vm.deleteCarousel = deleteCarousel;
		vm.toggleImageAspect = toggleImageAspect;
		vm.addAttrJSON = addAttrJSON;
		vm.saveGotoBlock = saveGotoBlock;
		vm.genAttrBracket = genAttrBracket;

		// Button
		vm.showPanel = showPanel;
		vm.closePanel = closePanel;
		vm.addNodeButton = addNodeButton;
		vm.addNodeCarouselButton = addNodeCarouselButton;
		vm.deleteButton = deleteButton;
		vm.filterButton = filterButton;
		vm.showEdit = showEdit;
		vm.editButton = editButton;
		vm.editNodeCarouselButton = editNodeCarouselButton;
		vm.showEditCarouselButton = showEditCarouselButton;
		vm.editReply = editReply;
		vm.showEditReply = showEditReply;
		// Util
		vm.copyText = copyText;
		vm.upload = upload;
		vm.uploadCarousel = uploadCarousel;
		vm.removeReplacementChar = removeReplacementChar;
		vm.typingOption = function (node) {
			return {
				floor: 0,
				ceil: 12,
				step: 0.1,
				precision: 2,
				onEnd: function () {
					vm.saveNode(node);
				}
			}
		}

		vm.sortableOptions = {
			axis: 'y',
			handle: '> .card-drag',
			'ui-floating': false,
			items: '.sortable-card',
			stop: function (event, ui) {
				_nodeReorder();
			}
		};

		vm.sortableGroupBlock = {
			connectWith: '.connect',
			start: function () {
				_.forEach(vm.groups, function (group) {
					if (!group.blocks) {
						group.blocks = [];
					}
				});
			},
			stop: function (event, ui) {
				vm.saveGroup()
					.then(function () {
						const block_id = ui.item[0].getAttribute('data-block-id');
						var group = vm.groups.find(function (group) {
							if (!group.blocks) {
								return null;
							}
							return group.blocks.find(function (block) {
								return block.block_id == block_id;
							});
						});
						vm.bot.blocks['block' + block_id].group = group.key;
						vm.bot.$save();
					});

			}
		}

		vm.sortableCarousel = {
			axis: 'x',
			handle: '> .carousel-drag',
			'ui-floating': true,
			items: '.sortable-carousel',
			stop: function (event, ui) {
				_nodeReorder();
			}
		}

		vm.sortableQuickReply = {
			items: '.sortable-quickreply',
			stop: function (event, ui) {
				_nodeReorder();
			}
		}

		vm.sortableGroups = {
			axis: 'y',
			'ui-floating': false,
			items: '.sortable-group:not(.not-sortable)',
			stop: function (event, ui) {
				vm.saveGroup();
			}
		}

		init();

		function init() {
			getBot();
		}

		function toggleImageAspect(node) {
			if (node.image_aspect_ratio == 'horizontal') {
				node.image_aspect_ratio = 'square';
			} else {
				node.image_aspect_ratio = 'horizontal';
			}
		}

		function addAttrJSON(node) {
			if (!node.url_param) {
				node.url_param = [];
			}
			if (node.attr) {
				node.url_param.push({
					key: node.attr.key,
					value: node.attr.value
				})
			}
			saveNode(node);
		}

		function copyText() {
            document.getElementById("broadcastAPI").select();
            document.execCommand('copy');
            popup()
		}


        function popup() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");

            $timeout(function () {
                popup.classList.remove('show');
            }, 500);
        }


		function saveGotoBlock(node, block) {
			if (typeof node.directions[0].goto === 'string') {
				node.directions.unshift({'goto': [], conditions: []});
			}
			if (!node.directions[0].goto) {
				node.directions[0].goto = [];
			}
			if (!node.directions[0].blocks) {
				node.directions[0].blocks = [];
			}
			node.directions[0].blocks.push({ block_select_name: block.block_name });
			node.directions[0].block.block_name = ''
			node.directions[0].goto.push(block.block_id);
			vm.saveNode(node);
		}

		function deleteBlockGotoBlock(node, index) {
			node.directions[0].blocks.splice(index, 1);
			node.directions[0].goto.splice(index, 1);
			vm.saveNode(node)
		}

		/**
		 * Get bot according to the bot id from the url
		 */
		function getBot() {
			if (vm.bot_id) {
				currentAuth.getIdToken().then(function (token) {
					Bots.getPageId(vm.bot_id, token)
						.then(function (page_id) {
							vm.page_id = page_id;
						});
				});
				vm.bot = Bots.getBot(vm.bot_id);
				firebase.database().ref('bots/bot_' + vm.bot_id + '/groups')
					.on('value', function (snap) {
						vm.groups = snap.val();
					});
				getBlock();
			}
		}

		/**
		 * Get current block specify by block_id from the url
		 */
		function getBlock(blockId) {
			if (blockId === undefined) {
				getBlock(0);
				return;
			}
			Bots.getBlock(blockId)
				.then(function (block) {
					vm.currentFromGotoblock = [];
					vm.currentBlock = block;
					Bots.getNodes(blockId)
						.then(function (nodes) {
							vm.nodes = nodes;
							if(!vm.currentBlock.start_node) {
								_nodeReorder();
							}
						});
					_initFormGotoblock();
				});
		}

		function _initFormGotoblock(){
			_.forEach(vm.bot.blocks,function(block){
				_.forEach(block.nodes,function(node){
					if(node.node_type === "gotoblock"){
							if(node.directions){
								if(node.directions[0].goto){
									if(node.directions[0].goto[0] === vm.currentBlock.block_id){
									vm.currentFromGotoblock.push({ name: block.block_name, id: block.block_id });
									} 
								}
							}
					} else if(node.node_type === "node"){
						if(node.buttons){
							_.forEach(node.buttons,function(button){
								if(button.blocks){
									_.forEach(button.blocks,function(bblock){
											if(bblock.block_id === vm.currentBlock.block_id){
												vm.currentFromGotoblock.push({ name: block.block_name, id: block.block_id });
											}
									})
								}
							})
						} else if(node.panel){
							_.forEach(node.nodeResponse.response, function(res){
								if(res.blocks){
									_.forEach(res.blocks,function(bblock){
										if(bblock.block_id === vm.currentBlock.block_id){
											vm.currentFromGotoblock.push({ name: block.block_name, id: block.block_id });
										}
									})
								}
							})
						}
					} else if(node.node_type === "carousel"){
						_.forEach(node.nodeResponse.response,function(res){
							if(res.buttons){
								_.forEach(res.buttons,function(button){
									if(button.blocks){
										if(button.blocks[0].block_id === vm.currentBlock.block_id){
										vm.currentFromGotoblock.push({ name: block.block_name, id: block.block_id });
										}
									}
								})
							}
						});
					}
				})
				vm.currentFromGotoblock = _.uniqBy(vm.currentFromGotoblock,'id');
			})
		}

		/**
		 * Add group
		 */
		function addGroup() {
			if (!vm.bot.groups) {
				vm.bot.groups = [];
			}
			vm.bot.groups.push({
				'name': 'Untitled Group',
				'key': _guid()
			});
			vm.bot.$save();
		}

		function copyBlock() {
			let groupIndex;

			for(var i=0;i< vm.bot.groups.length;i++){
				if(vm.bot.groups[i].key === vm.currentGroupKey){
				   groupIndex = i;
				}
			}

			Bots.getNodes(vm.currentBlock.block_id)
		    .then(function (nodes){
		
				vm.bot_loading = true;
			
				Bots.addCopyBlock(vm.currentGroupKey, nodes)
					.then(function (block) {
						if (!vm.bot.groups[groupIndex].blocks) {
							vm.bot.groups[groupIndex].blocks = [];
						}
						vm.bot.groups[groupIndex].blocks.push({ block_name: block.block_name, block_id: block.block_id });
						vm.saveBlock();
						vm.bot_loading = false;
						vm.getBlock(block.block_id);
						AlertService.push("Duplicate Complete");
					});	
			});		
		}

		function deleteGroup(index, group_key) {
			if (!$window.confirm("Are you sure to delete group ?")) {
				return;
			}
			_.forEach(vm.bot.blocks, function (block, key) {
				if (block.group === group_key) {
					vm.bot.blocks[key] = null;
				}
			});
			vm.groups.splice(index, 1);
			vm.saveBlock();
			vm.saveGroup();
			AlertService.push('Your group is gone. BYE!');
		}

		/**
		 * Add block
		 */
		function addBlock(index, key) {
			vm.bot_loading = true;
			Bots.addBlock(key)
				.then(function (block) {
					if (!vm.bot.groups[index].blocks) {
						vm.bot.groups[index].blocks = [];
					}
					vm.bot.groups[index].blocks.push({ block_name: block.block_name, block_id: block.block_id });
					vm.saveBlock();
					vm.bot_loading = false;
				});
		}

		function genAttrBracket(attr, type){
				if(type === 'attr')
					attr.attr_name = attr.attr_name.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
				else if(type === 'input')
					attr.save_to_variable = attr.save_to_variable.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
				else if(type === 'quickreply')
					attr.attr_name = attr.attr_name.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
				else if(type === 'gotoblock')
					attr.first_var = attr.first_var.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
				else if(type === 'math'){
					attr.result_attr = attr.result_attr.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");

					if(attr.first_value >= 0 || attr.first_value <= 0){

					} else{
						attr.first_value = attr.first_value.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
					}
					if(attr.second_value >= 0 || attr.second_value <= 0){

					} else{
						attr.second_value = attr.second_value.replace(/{*([a-zA-Z0-9_ก-๙]+)}*/g, "{{$1}}");
					}	
				}
		}

		/**
		 * Delete block
		 */
		function deleteBlock() {
			// Find block's group in groups
			let groupOfBlock = vm.groups.filter(function (group) {
				return group.key == vm.currentBlock.group;
			});
			// Find block in group's blocks
			var blockIndex;
			_.forEach(groupOfBlock[0].blocks, function (block, index) {
				if (block.block_id == vm.currentBlock.block_id) {
					blockIndex = index;
				}
			});
			groupOfBlock[0].blocks.splice(blockIndex, 1);
			vm.saveGroup();
			vm.currentBlock.$remove()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
			vm.getBlock('0');
		}

		/**
		 * Save block
		 */
		function saveBlock() {
			vm.bot.$save().then(function () {
				if (vm.currentBlock.block_id) {
					vm.currentBlock.$save()
						.then(function() {
							Bots.update('bot_' + vm.bot.bot_id)
						});
				} else {
					vm.getBlock('0');
				}
			});
		}

		function saveGroup() {
			return firebase.database().ref('bots/bot_' + vm.bot_id + '/groups')
				.set(vm.groups);
		}

		/**
		 * Add new node to the block
		 * @param type
		 */
		function addNode(type) {
			Bots.addNode(type, vm.currentBlock);
		}

		/**
		 * Delete node from block
		 * @param node
		 */
		function deleteNode(node, _) {
			vm.nodes.$remove(node)
				.then(function () {
					_nodeReorder();
				});;
		}

		function saveNode(node) {
			vm.nodes.$save(node)
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		function saveIndexBlock(key){
			vm.currentGroupKey = key;
		}

		function removeReplacementChar(text) {
			text = text.replace(/\uFFFD/g, '');
			return text;
		}

		/**
		 * Add reply to the list
		 * @param node
		 * @param button
		 */
		function addReply(node, blocks, button) {
			var b = {};
			if (node.panel.type == 'show_block') {
				b = {
					type: 'show_block',
					title: button.title,
					blocks: blocks,
				}
			}
			if (!node.nodeResponse.response || node.nodeResponse.response === '') {
				node.nodeResponse.response = [];
			}
			node.nodeResponse.response.push(b);

			vm.tempNode = {};
			delete node.panel;
			saveNode(node);
		}
		function editReply(node) {
			var b = {};
			b = {
				type: node.nodeResponse.response[node.panel.index].type,
				title: node.nodeResponse.response[node.panel.index].title,
				blocks: node.nodeResponse.response[node.panel.index].blocks
			}
			node.nodeResponse.response[node.panel.index] = b;
			node.panel.edit = false;
			saveNode(node);
		}

		function toggleAI(node) {
			node.use_ai = !node.use_ai;
			saveNode(node);
		}

		/**
		 * Delete reply from the list
		 * @param node
		 * @param button_index
		 */
		function deleteReply(node, button_index) {
			node.nodeResponse.response.splice(button_index, 1);
			saveNode(node);
		}

		/**
		 * Add carousel to the list.
		 * @param node
		 */
		function addCarousel(node) {
			Bots.addCarousel(node);
			saveNode(node);
		}

		/**
		 * Delete carousel from the list
		 * @param node
		 * @param carousel_index
		 */
		function deleteCarousel(node, carousel_index) {
			if (node.nodeResponse.response.length > 1) {
				node.nodeResponse.response.splice(carousel_index, 1);
				saveNode(node);
			} else {
				deleteNode(node);
			}
		}

		/**
		 * Show button panel
		 * @param node
		 */
		function showPanel(node) {
			node.panel = {};
			node.panel.show = true;
			node.panel.type = 'show_block';
		}

		function showEdit(node, index) {
			node.panel = {};
			node.panel.edit = true;
			node.panel.index = index;
			if (!node.buttons[index].blocks) {
				node.buttons[index].blocks = [];
			}
		}

		function showEditReply(node, index) {
			node.panel = {};
			node.panel.edit = true;
			node.panel.index = index;
			if (!node.nodeResponse.response[index].blocks) {
				node.nodeResponse.response[index].blocks = [];
			}
		}

		/**
		 * Close button panel
		 * @param node
		 */
		function closePanel(node) {
			node.panel.show = false;
			node.panel.edit = false;
		}

		/**
		 * Add button to the buttons set
		 * @param node
		 * @param button
		 */
		function addNodeButton(node, button, blocks, webview_height_ratio) {
			var b = {};
			if (node.panel.type == 'show_block') {
				b = {
					type: node.panel.type,
					title: button.title,
					blocks: blocks,
				}
			} else if (node.panel.type == 'web_url') {
				b = {
					type: node.panel.type,
					title: button.title,
					url: button.filter,
					webview_height_ratio: webview_height_ratio
				}
			} else {
				b = {
					type: node.panel.type,
					title: button.title,
					phone_number: button.filter
				}
			}

			if (!node.buttons) {
				node.buttons = [];
			}

			if (node.buttons.length <= 3) {
				node.buttons.push(b);
			}


			vm.tempNode = {};
			delete node.panel;
			saveNode(node);
		}

		function editButton(node) {
			var b = {};
			if (node.buttons[node.panel.index].type == 'show_block') {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					blocks: node.buttons[node.panel.index].blocks
				}
			} else if (node.buttons[node.panel.index].type == 'web_url') {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					url: node.buttons[node.panel.index].url,
					webview_height_ratio: node.buttons[node.panel.index].webview_height_ratio
				}
			} else {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					phone_number: node.buttons[node.panel.index].phone_number,
				}
			}
			node.buttons[node.panel.index] = b;
			node.panel.edit = false;
			saveNode(node);
		}

		function addNodeCarouselButton(carousel, button, blocks, node, webview_height_ratio) {
			var b = {};
			if (carousel.panel.type == 'show_block') {
				b = {
					type: carousel.panel.type,
					title: button.title,
					blocks: blocks
				}
			} else if (carousel.panel.type == 'web_url') {
				b = {
					type: carousel.panel.type,
					title: button.title,
					url: button.filter,
					webview_height_ratio: webview_height_ratio
				}
			} else {
				b = {
					type: carousel.panel.type,
					title: button.title,
					phone_number: button.filter
				}
			}

			if (!carousel.buttons) {
				carousel.buttons = [];
			}

			if (carousel.buttons.length <= 3) {
				carousel.buttons.push(b);
			}
			vm.tempNode = {};
			delete carousel.panel;
			saveNode(node);
		}
		function showEditCarouselButton(carousel, index) {
			carousel.panel = {};
			carousel.panel.edit = true;
			carousel.panel.index = index;
			carousel.panel.type = carousel.buttons[carousel.panel.index].type;
			if (!carousel.buttons[index].blocks) {
				carousel.buttons[index].blocks = [];
			}
		}
		function editNodeCarouselButton(carousel, node, index) {
			var b = {};
			if (carousel.buttons[index].type == 'show_block') {
				b = {
					type: carousel.buttons[index].type,
					title: carousel.buttons[index].title,
					blocks: carousel.buttons[index].blocks
				}
			} else if (carousel.buttons[index].type == 'web_url') {
				b = {
					type: carousel.buttons[index].type,
					title: carousel.buttons[index].title,
					url: carousel.buttons[index].url,
					webview_height_ratio: carousel.buttons[index].webview_height_ratio
				}
			} else {
				b = {
					type: carousel.buttons[index].type,
					title: carousel.buttons[index].title,
					phone_number: carousel.buttons[index].phone_number,
				}
			}
			carousel.buttons[carousel.panel.index] = b;
			carousel.panel.edit = false;
			saveNode(node);
		}

		/**
		 * Delete button from the buttons set
		 * @param node
		 * @param button_index
		 */
		function deleteButton(node, button_index) {
			node.buttons.splice(button_index, 1);
			saveNode(node);
		}

		/**
		 * Filter Block for button suggestion
		 * @param blocks
		 * @param filter
		 */
		function filterButton(blocks, filter) {
			var retval = _.filter(blocks, function (v, k) {
				if (v && v.block_name && filter) {
					return v.block_name.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
				}
			});
			return retval;
		}

		function uploadCarousel(file, node, item, index) {
			var isImage = file.type.indexOf("image") > -1;
			var isGif = file.type.indexOf("gif") > -1;
			item.image_url = true;
			if(isImage && !isGif) {
				new ImageCompressor(file, {
					quality: .75,
					maxWidth: 500,
					success(result) {
						var uploadTask = firebase.storage().ref().child(_guid()).put(result);
						uploadTask.then(function (data) {
							item.image_url = data.downloadURL;
							node.nodeResponse.response[index] = item;
							saveNode(node);
							vm.image_aspect = {
								node: node,
								show: true
							}
						});
					},
					error(e) {
						console.log(e.message);
					}
				});
			} else {
				var uploadTask = firebase.storage().ref().child(_guid()).put(file);
				uploadTask.then(function (data) {
					item.image_url = data.downloadURL;
					node.nodeResponse.response[index] = item;
					saveNode(node);
					vm.image_aspect = {
						node: node,
						show: true
					}
				});
			}
		}

		/**
		 * Upload file (image) to the server
		 * @param file
		 * @param node
		 */
		function upload(file, node) {
			var isImage = file.type.indexOf("image") > -1;
			var isGif = file.type.indexOf("gif") > -1;
			node.nodeResponse.response = true;
			if(isImage && !isGif) {
				new ImageCompressor(file, {
					quality: 1,
					maxWidth: 500,
					success(result) {
						var uploadTask = firebase.storage().ref().child(_guid()).put(result);
						uploadTask.then(function (data) {
							node.nodeResponse.response = data.downloadURL;
							saveNode(node);
						})
					},
					error(e) {
						console.log(e.message);
					}
				});
			} else {
				var uploadTask = firebase.storage().ref().child(_guid()).put(file);
				uploadTask.then(function (data) {
					node.nodeResponse.response = data.downloadURL;
					saveNode(node);
				})
			}
		}

		function copySuccess(e) {
			vm.copied = true;
			$timeout(() => {
				vm.copied = false;
			}, 500);
		}

		/**
		 * Private function
		 * Use to generete uniqe id
		 */
		function _guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			}
			return s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4();
		}

		function _nodeReorder() {
			var nodesPromises = []
			vm.nodes.forEach(function (node, idx) {
				if (node.panel) {
					closePanel(node);
				}
				if (node.node_type == 'carousel') {
					if (!node.nodeResponse.response[0].panel) {
						node.nodeResponse.response[0].panel = {};
					}
					node.nodeResponse.response[0].panel.show = false;
					node.nodeResponse.response[0].panel.edit = false;
				}
				if (node.directions) {
					for(var i = 2; i < node.directions.length; i ++) {
						node.directions[i] = [];
					}
				}
				node.order = vm.nodes.indexOf(node);
				if (idx === 0) {
					vm.currentBlock.start_node = node.$id;
					vm.currentBlock.$save();
				}
				if (idx > 0) {
					if (!vm.nodes[idx - 1].directions) {
						vm.nodes[idx - 1].directions = [{ 'goto': '' }];
					}
					if(vm.nodes[idx - 1].node_type == 'gotoblock') {
						if(vm.nodes[idx - 1].directions[vm.nodes[idx - 1].directions.length - 1].conditions) {
							vm.nodes[idx - 1].directions.push({'goto': node.$id});
							vm.nodes.$save(vm.nodes[idx - 1]);				
						} else {
							vm.nodes[idx - 1].directions[vm.nodes[idx - 1].directions.length - 1].goto = node.$id;
							vm.nodes.$save(vm.nodes[idx - 1]);
						}
					} else {
						vm.nodes[idx - 1].directions[vm.nodes[idx - 1].directions.length - 1].goto = node.$id;
						nodesPromises.push(vm.nodes.$save(vm.nodes[idx - 1]));
					}
				}
				nodesPromises.push(vm.nodes.$save(node));
			});
			
			/* Set last node last direction to be empty */
			var last_node = vm.nodes[vm.nodes.length - 1];
			if(last_node['directions']) {
				var last_direction = last_node.directions.length - 1;
				if (last_node.directions && !last_node.directions[last_direction].conditions) {
					last_node.directions[last_direction] = [];
					nodesPromises.push(vm.nodes.$save(last_node));
				}
			}
			
			Promise.all(nodesPromises)
				.then(function (n) {
					vm.saveBlock();
				})
				.catch(function (e) {
					console.log(e);
				});
		}

	}
})();
