(function () {
    'use strict';

    angular
        .module('app')
        .controller('configureCtrl', configureCtrl);


    configureCtrl.$inject = ['$state', '$stateParams', '$timeout', '$http', 'Bots', 'Config', '$cookies', 'ApiEndpointURL', 'BotsApi', '$scope', 'Facebook', 'Auth', 'currentAuth'];

    //let vm;

    function configureCtrl($state, $stateParams, $timeout, $http, Bots, Config, $cookies, ApiEndpointURL, BotsApi, $scope, Facebook, Auth, currentAuth) {

        var vm = this;

        // Initial variables
        vm.bots = []
        vm.bot_id = $stateParams.bot_id;
        vm.block_id = $stateParams.block_id;
        vm.bot = {};
        vm.accessTokenPage = '';
        vm.greetingText = '';
        vm.greeting = [];
        vm.buttons = [];
        vm.setSubMenu = false;
        vm.index = '';
        vm.setSubMenu2 = false;
        vm.showSubMenu = false;
        vm.showSubMenu2 = false;
        vm.showSetMenu = false;
        vm.showSuggest = false;
        vm.type = '';
        vm.filter = '';
        vm.copyText = copyText;
        vm.apiToken = '';
        vm.admins = [];
        vm.addedAdminUsername = '';

        //button
        vm.changeGreeting = changeGreeting;
        vm.addMenuItem = addMenuItem;
        vm.addSubMenuItem = addSubMenuItem;
        vm.addSubMenuItem2 = addSubMenuItem2;
        vm.getIndex = getIndex;
        vm.getIndex2 = getIndex2;
        vm.getEditIndex = getEditIndex;
        vm.getEditIndexSubMenu1 = getEditIndexSubMenu1;
        vm.getEditIndexSubMenu2 = getEditIndexSubMenu2;
        vm.editMenuItem = editMenuItem;
        vm.editSubMenuItem1 = editSubMenuItem1;
        vm.editSubMenuItem2 = editSubMenuItem2;
        vm.setMenuBurger = setMenuBurger;
        vm.changePayloadToArray = changePayloadToArray;
        vm.changePayloadToArraySubMenu1 = changePayloadToArraySubMenu1;
        vm.changePayloadToArraySubMenu2 = changePayloadToArraySubMenu2;
        vm.filterButton = filterButton;
        vm.revokeApiToken = revokeApiToken;
        vm.addAdmin = addAdmin;
        vm.removeAdmin = removeAdmin;

        // facebook
        vm.fbLoginStatus = false;
        vm.connectPage = connectPage;
        vm.pageSelectStatus = false;
        vm.pageSeleted = '';
        vm.disconnectPage = disconnectPage;

        // web hook url
        vm.webHookUrl = '';
        vm.webHookHeaderKey = '';
        vm.webHookHeaderValue = '';
        vm.setWebHook = setWebHook;

        // live chat
        vm.startKeyword = '';
        vm.stopKeyword = '';
        vm.timeout = '';
        vm.enable = false;
        vm.setLivechat = setLivechat;
        vm.toggleLivechat = toggleLivechat;
        vm.checkDisable = checkDisable;

        //export
        vm.exportBot = exportBot,
        vm.importBot = importBot,
        vm.file = '',
        vm.exportOption = {
            flow: true,
            ai: true
        };
        // vm.onFileChange = onFileChange

            init();

        function init() {
            currentAuth.getIdToken().then(function (token) {
				$http.get(ApiEndpointURL + '/bot', { headers: { Authorization: token } })
					.then(function success(bots) {
						vm.user = Auth.getUser();
                        vm.bots = bots.data;
                        getAccessTokenAccount(vm.bots);
					}, function error(response) {

					});
			});
            Config.getBot(vm.bot_id)
                .then(function (bot) {
                    if (bot.web_hook_url){
                        vm.webHookUrl = bot.web_hook_url;
                    }
                    if (bot.web_hook_custom_headers) {
                        vm.webHookHeaderKey = bot.web_hook_custom_headers.key;
                        vm.webHookHeaderValue = bot.web_hook_custom_headers.value;
                    }
                    vm.apiToken = bot.api_token;
                    let botPage = bot.channels.FACEBOOK;
                    vm.accessTokenPage = bot.channels.FACEBOOK.accessToken;
                    vm.user = Auth.getUser();

                    console.log("bot.channels.FACEBOOK");
                    console.log(bot.channels.FACEBOOK);
                    
                    vm.admins = bot.admins;
                    if (vm.accessTokenPage) {
                        checkTokenPage(vm.accessTokenPage,botPage);
                        vm.pageSelectStatus = true;
                        vm.pageSelected.id = bot.channels.FACEBOOK.id;
                        vm.pageSelected.name = bot.channels.FACEBOOK.name;
                    } 
                });
            getBot(vm.bot_id);
            getLivechat(vm.bot_id);
        }

        function checkTokenPage(accessTokenPage,botPage) {
            Facebook.checkTokenPage(accessTokenPage)
                .then(function (response) {
                    console.log("fbResponse");
                    console.log(response);
                    getGreeting(accessTokenPage);
                    getMenuBurger(accessTokenPage);
                    getPageConnect(botPage);
                });
        }

        function _guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			}
			return s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4();
		}

        function checkDisable(){
            if(vm.enable){
                if(!vm.startKeyword || !vm.stopKeyword)
                    return true;
            }
        }

        function getBot(botId) {
            vm.bot = Bots.getBot(botId);
        }

        function getLivechat(botId){
            Config.getLivechat(botId)
                .then(function (data){
                    vm.startKeyword = data.start_keyword;
                    vm.stopKeyword = data.stop_keyword;
                    vm.enable = data.enable;

                    if(!data.timeout)
                        vm.timeout = "10";
                    else 
                        vm.timeout = data.timeout;
                });
        }

        function getGreeting(accessTokenPage) {
            Config.getGreeting(accessTokenPage)
                .then(function (data) {
                    if (data.data.data[0]) {
                        vm.greetingText = data.data.data[0].greeting[0].text;
                    }
                });
        }

        function toggleLivechat(node){
            vm.enable = !vm.enable;
            if(vm.bot.groups[0].blocks.length < 3){
                Bots.addBlock('build0')
				.then(function (block) {
					if (!vm.bot.groups[0].blocks) {
						vm.bot.groups[0].blocks = [];
					}
					vm.bot.groups[0].blocks.push({ block_name: block.block_name, block_id: block.block_id });
                    vm.bot.$save().then(function () {
                        var b_block_2_nodeID = _guid();
                        var b_block_2_content = {
                            ['N0_' + b_block_2_nodeID]: {
                                node_id: '0_' + b_block_2_nodeID,
                                order: 0,
                                node_type: 'node',
                                nodeResponse: {
                                    type: 'text',
                                    response: 'live chat finished'
                                }
                            }
                        }
                        vm.bot.blocks["block2"].nodes = b_block_2_content;
                        vm.bot.$save().then(function () {
                            Bots.update('bot_' + vm.bot.bot_id)
                        });
                    });
                });
            }
            if(vm.enable){
                if(!vm.startKeyword)
                    vm.startKeyword = 'livechat on';
                if(!vm.stopKeyword)
                    vm.stopKeyword = 'livechat off';
            }
        }

        function getMenuBurger(accessTokenPage) {
            Config.getMenuBurger(accessTokenPage)
                .then(function (data) {
                    if (data.data.data[0]) {
                        vm.MenuBurger = data.data.data[0].persistent_menu;
                        var menuBurger = vm.MenuBurger.filter(function (index) { return index.locale == 'default'; });
                        vm.buttons = menuBurger[0].call_to_actions;
                    }
                });
        }

        function getEditIndex(index) {
            vm.editIndex = index;
        }

        function changePayloadToArray(index) {
            if (vm.buttons[index].type === 'postback') {
                var payload = JSON.parse(vm.buttons[index].payload);
                vm.buttons[index].payload = payload;
            }
        }

        function changePayloadToArraySubMenu1(index) {
            if (vm.buttons[vm.index].call_to_actions[index].type === 'postback') {
                var payload = JSON.parse(vm.buttons[vm.index].call_to_actions[index].payload);
                vm.buttons[vm.index].call_to_actions[index].payload = payload;
            }
        }

        function changePayloadToArraySubMenu2(index) {
            if (vm.buttons[vm.index].call_to_actions[vm.index2].call_to_actions[index].type === 'postback') {
                var payload = JSON.parse(vm.buttons[vm.index].call_to_actions[vm.index2].call_to_actions[index].payload);
                vm.buttons[vm.index].call_to_actions[vm.index2].call_to_actions[index].payload = payload;
            }
        }

        function filterButton(block, filter) {
            var retval = _.filter(block, function (v) {
                if (v.block_name) {
                    return v.block_name.toLowerCase().indexOf(filter) >= 0;
                }
            });
            return retval;
        }

        function getEditIndexSubMenu1(index) {
            vm.editIndexSubMenu1 = index
        }

        function getEditIndexSubMenu2(index) {
            vm.editIndexSubMenu2 = index
        }

        function getIndex(index) {
            vm.index = index;
        }

        function getIndex2(index) {
            vm.index2 = index;
        }

        function deleteButton(index) {
            vm.buttons.splice(index, 1);
            setMenuBurger(vm.buttons);
        }

        function editMenuItem(title, type, link_url, payload, webview_height_ratio, call_to_actions, index) {
            var b = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                b = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio
                }
            }
            else if (type == 'postback') {
                b = {
                    title: title,
                    type: type,
                    payload: payload
                }
            } else {
                b = {
                    title: title,
                    type: type,
                    call_to_actions: [{
                        title: 'Edit Buttons',
                        type: 'postback',
                        payload: '[]'
                    }]
                }
            }
            vm.buttons[index] = b;
            setMenuBurger(vm.buttons);
        }

        function editSubMenuItem1(title, type, link_url, payload, webview_height_ratio, call_to_actions, index, index2) {
            var b = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                b = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio
                }
            }
            else if (type == 'postback') {
                b = {
                    title: title,
                    type: type,
                    payload: payload
                }
            } else {
                b = {
                    title: title,
                    type: type,
                    call_to_actions: [{
                        title: 'Edit Buttons',
                        type: 'postback',
                        payload: '[]'
                    }]
                }
            }
            vm.buttons[index].call_to_actions[index2] = b;
            setMenuBurger(vm.buttons);
        }

        function editSubMenuItem2(title, type, link_url, payload, webview_height_ratio, index, index2, index3) {
            var b = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                b = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio
                }
            }
            else if (type == 'postback') {
                b = {
                    title: title,
                    type: type,
                    payload: payload
                }
            }
            vm.buttons[index].call_to_actions[index2].call_to_actions[index3] = b;
            setMenuBurger(vm.buttons);
        }

        function addMenuItem(title, type, link_url, payload, webview_height_ratio) {
            var b = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                b = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio
                }
            }
            else if (type == 'postback') {
                b = {
                    title: title,
                    type: type,
                    payload: payload
                }
            } else {
                b = {
                    title: title,
                    type: type,
                    call_to_actions: [{
                        title: 'Edit Buttons',
                        type: 'postback',
                        payload: '[]'
                    }
                    ]
                }
            }
            if (!vm.buttons) {
                vm.buttons = [];
            }
            if (vm.buttons.length <= 3) {
                vm.buttons.push(b);
                setMenuBurger(vm.buttons);
            }
        }

        function addSubMenuItem(title, type, link_url, payload, webview_height_ratio, index) {
            var sb1 = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                sb1 = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio,
                }
            }
            else if (type == 'postback') {
                sb1 = {
                    title: title,
                    type: type,
                    payload: payload,
                }
            }
            else {
                sb1 = {
                    title: title,
                    type: type,
                    call_to_actions: [{
                            title: 'Edit Buttons',
                            type: 'postback',
                            payload: '[]'
                        }],
                }
            }
            vm.buttons[index].call_to_actions.push(sb1);
            setMenuBurger(vm.buttons);
        }

        function addSubMenuItem2(title, type, link_url, payload, webview_height_ratio, index, index2) {
            var sb2 = {};
            var payload = JSON.stringify(payload);
            if (type == 'web_url') {
                sb2 = {
                    title: title,
                    type: type,
                    url: link_url,
                    webview_height_ratio: webview_height_ratio
                }
            }
            else if (type == 'postback') {
                sb2 = {
                    title: title,
                    type: type,
                    payload: payload
                }
            }
            else {
                sb2 = {
                    title: title,
                    type: type,
                }
            }
            vm.buttons[index].call_to_actions[index2].call_to_actions.push(sb2);
            setMenuBurger(vm.buttons);
        }

        function setMenuBurger(menuBurger) {
            vm.menuBurger = {
                'persistent_menu': [
                    {
                        "locale": "default",
                        "composer_input_disabled": false,
                        "call_to_actions": menuBurger
                    }
                ]
            }
            Config.setMenuBurger(vm.accessTokenPage, vm.menuBurger)
                .then(function (data) {
                });
        }

        function changeGreeting(greetingText) {
            vm.greeting = {
                setting_type: "greeting",
                greeting: [{
                    "locale": 'default',
                    "text": greetingText
                }
                ]
            };
            Config.changeGreeting(vm.accessTokenPage, vm.greeting)
                .then(function (data) {
                    getGreeting(vm.accessTokenPage);

                });
        }

        function copyText() {
            document.getElementById("broadcastAPI").select();
            document.execCommand('copy');
            popup()

        }

        function popup() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");

            $timeout(function () {
                popup.classList.remove('show');
            }, 5000);
        }

        function revokeApiToken() {
            Config.revokeApiToken(vm.bot_id)
                .then(function (apiToken) {
                    vm.apiToken = apiToken;
                });
        }

        function addAdmin(email) {
            BotsApi.addAdmin(email, vm.bot_id).then(function (bot) {
                vm.admins = bot.admins;
                vm.addedAdminUsername = '';
            });
        }

        function removeAdmin(admin) {
            BotsApi.removeAdmin(admin.uid, vm.bot_id).then(function (bot) {
                vm.admins = bot.admins;
            });
        }

        function getAccessTokenAccount(bots) {
            console.log("vm.fbLoginStatus")
            Facebook.getAccessToken().then(function (tokenData) {
                fetchFbPages(tokenData.token, bots);
                vm.fbLoginStatus = true;
                console.log(vm.fbLoginStatus)
            }, function (error) {
                console.log(vm.fbLoginStatus)
                console.log(error);
            });
        }

        function fetchFbPages(accessToken, bots) {
            Facebook.fetchFbPages(accessToken,bots)
                .then(function (response) {
                    vm.fbPages = response.data;
                    console.log(vm.fbPages);
                });
        }

        function getPageConnect(botPage) {
            console.log(botPage.id)
            console.log(botPage.accessToken)

            //if(botPage.id && botPage.accessToken) {
                vm.pageSelectStatus = true;
                vm.pageSelected = botPage;
            // } else {
            //     vm.pageSelectStatus = false;
            // }
        }

        function connectPage(fbPage) {
            vm.pageSelectStatus = true;
            vm.pageSelected = fbPage;
            vm.accessTokenPage = fbPage.access_token;
            Facebook.connectPage(fbPage, vm.bot_id)
                .then((response) => {
                    Facebook.callGetStartButton(fbPage)
                        .then(function () {
                            getGreeting(fbPage.access_token);
                            getMenuBurger(fbPage.access_token);
                        })
                })
        }

        function disconnectPage() {
            Facebook.disconnectPage(vm.bot_id)
                .then((response) => {
                    vm.pageSelectStatus = false;
                    vm.pageSelected = '';
                }, function (err) {
                    alert(err);
                });
        }

        function exportBot() {
            Config.exportBot(vm.bot_id, vm.exportOption);
        }

        function importBot() {
            var f = document.getElementById('file').files[0],
                r = new FileReader();

            r.onloadend = function (e) {
                var data = e.target.result;
                Config.importBot(vm.bot_id, data)
            }

            r.readAsBinaryString(f);
        }

        function setWebHook(webHookUrl, webHookHeaderKey, webHookHeaderValue) {
            Config.setWebHook(webHookUrl, webHookHeaderKey, webHookHeaderValue, vm.bot_id, vm.apiToken);
        }

        function setLivechat(startKeyword, stopKeyword, timeout, enable) {
            Config.setLivechat(startKeyword, stopKeyword, timeout, enable, vm.bot_id, vm.apiToken);
        }
    }
})();
