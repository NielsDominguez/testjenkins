(function () {
	'use strict';

	angular
		.module('app')
		.controller('authCtrl', authCtrl);

	authCtrl.$inject = ['$state', '$stateParams', 'Auth', 'ApiEndpointURL', 'SIDService', 'enableSID'];

	function authCtrl($state, $stateParams, Auth, ApiEndpointURL, SIDService, enableSID) {
		var vm = this;

		vm.signin = signin;
		vm.signup = signup;
		vm.signout = signout;
		vm.forgetPassword = forgetPassword;
		vm.now = Date.now().toString();
		vm.errorMessage = '';
		vm.logo = SIDService.getLogo();
		vm.isSID = SIDService.isSID();
		vm.enableSID = enableSID;

		init();

		function init() {
			if (Auth.getUser() && !$stateParams.mode) {
				redirectToApp();
			}
		}

		function signin(user) {
			if (!user || !user.email || !user.password) {
				vm.errorMessage = 'Email and password need to specify!';
				return;
			}
			Auth.signInFirebaseEmail(user.email, user.password)
				.then(function () {
					redirectToApp();
				})
				.catch(function (error) {
					vm.errorMessage = error;
				});
		}

		function signup(user) {
			if (!user || !user.email || !user.password || !user.name) {
				vm.errorMessage = 'Please enter required data';
				return;
			}
			Auth.signUpFirebaseEmail(user.email, user.password)
				.then(function () {
					redirectToEmailVerification();
				})
				.catch(function (error) {
					vm.errorMessage = error;
				});
		}

		function signout() {
			Auth.signOutFirebase()
				.catch(function (error) {
					vm.errorMessage = error;
				});
		}

		function forgetPassword(user) {
			Auth.forgetPassword(user.email)
				.catch(function (error) {
					vm.errorMessage = error;
				})
		}

		function redirectToApp() {
			$state.go('dashboard');
		}

		function redirectToEmailVerification() {
			$state.go('verify');
		}
	}
})();