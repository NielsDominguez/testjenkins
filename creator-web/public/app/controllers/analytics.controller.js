(function() {
	'use strict';

	angular.module("app").controller("analyticsCtrl",analyticsCtrl);

	analyticsCtrl.$inject = ['$state', '$stateParams', '$http', 'Bots','$scope','Analytics', 'ApiEndpointURL', 'AlertService','$window'];

	function analyticsCtrl($state, $stateParams, $http, Bots,$scope,Analytics, ApiEndpointURL, AlertService,$window) {
		var vm = this;

		// Initial variables
		vm.bot_id = $stateParams.bot_id;
		vm.checkFilterDate = checkFilterDate;
		vm.addFilter = addFilter;
		vm.getUsersByFilter = getUsersByFilter;
		vm.export = {};
		vm.filter = {};
		vm.errorMessage;
		vm.users;
		vm.rows = [];
		vm.totalPages;
		vm.pager = {};
		vm.setPage = setPage;
		vm.setPageFirst = setPageFirst;
		vm.numberOfUsers;
		vm.numberOfUsersFilter;
		vm.getCsvFile = getCsvFile;
		vm.deleteUser = deleteUser;

		vm.setPageFirst(1);
		
		function getUsers(page, size) {
			Analytics.getUsers(vm.bot_id, page, size).then(function (data) {
				var users = data.users;
				var totalPage = data.totalPages;
				var totalUser = data.totalUsers;
				var buffer = [];

				vm.totalPages = totalPage;
				vm.numberOfUsers = totalUser;
				vm.numberOfUsersFilter = totalUser;
				vm.users = users;
				prepareExport(vm.users);
				_.forEach(vm.users, function (user) {
					var createDateTime = convertDateTime(user,'create_at');
					var lastUpdateTime = convertDateTime(user,'last_update');
					if(!user.attr.name) {
						user.attr.name = user.attr.first_name + " " + user.attr.last_name;
					}
					buffer.push({ fullname: user.attr.name, profile_pic: user.attr.profile_pic, create_at: createDateTime, last_update: lastUpdateTime, attr: user.attr });
				})
				vm.rows = buffer;
				vm.pager = Analytics.getPager(vm.numberOfUsersFilter, page, 10);
			}, function (error) {
				alert(error);
			});
		}

		function deleteUser(user) {
			if ($window.confirm("Are you sure to delete " + user.attr.name + "?")) {
				$.getJSON("https://api-my.hbot.io/v1/bot/"+user.bot_id+"/delete/"+user.attr.fb_id+"",function(data){
					$("#" + user.attr.fb_id).remove();
				});
			}
		}

		function getUsersByFilter(page, size) {
			Analytics.getUsersByFilter(vm.filter, vm.bot_id, page)
				.then(function (data) {
					var users = data.users;
					var buffer = [];
					var totalPage = data.totalPages;
					var totalUser = data.totalUsers;
					
					vm.totalPages = totalPage;
					vm.numberOfUsersFilter = totalUser;
					vm.users = users;

					vm.pager = Analytics.getPager(vm.numberOfUsersFilter, page, 10);

					prepareExport(vm.users);

					_.forEach(vm.users, function (user) {
						var createDateTime = convertDateTime(user,'create_at');
						var lastUpdateTime = convertDateTime(user,'last_update');
						if(!user.attr.name) {
							user.attr.name = user.attr.first_name + " " + user.attr.last_name;
						}
						buffer.push({ fullname: user.attr.name, profile_pic: user.attr.profile_pic, create_at: createDateTime, last_update: lastUpdateTime, attr: user.attr });
					})
					vm.rows = buffer;
				});
		}

		function getCsvFile(){
			if(_.isEmpty(vm.filter)){
			   window.location = `${ApiEndpointURL}/bot/${vm.bot_id}/report/allUser`;
			} else {
			   const filterUser = JSON.stringify(vm.filter);
			   window.location = `${ApiEndpointURL}/bot/${vm.bot_id}/report/filterUser?filterUser=${filterUser}`;
			}
		}
		
		function setPageFirst(page) {
			if (page < 1 || page > vm.pager.totalPages) {
				return;
			}
		
			// get current page of items
			getUsers(page, 10);
			
			// get pager object from service
			vm.pager = Analytics.getPager(vm.numberOfUsersFilter, page, 10);
		}

		function setPage(page) {
			if (page < 1 || page > vm.pager.totalPages) {
				return;
			}
		
			// get current page of items
			getUsersByFilter(page, 10);
			
			// get pager object from service
			vm.pager = Analytics.getPager(vm.numberOfUsersFilter, page, 10);
		}

		function convertDateTime(user,attr){
			var date = new Date(user[attr] * 1000);
			var dateString = date.toDateString();
		
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			var dateTime = dateString + " " + "|" + " " + strTime;
			
			return dateTime;
		}
		
		
		Analytics.getReport(vm.bot_id).then(function (result) {
			drawGraph(result);
		});

		function drawGraph(data) {
			$scope.time = data.time;
			$scope.countNewUser = [data.countNewUser];
			$scope.countMessageIn = [data.countMessageIn];
			$scope.countMessageOut = [data.countMessageOut];
			$scope.activeUserCount = [data.activeUserCount];
			$scope.colors = ["#08f1e2"];

			$scope.labels = $scope.time
			$scope.options = {
				scales: {
					yAxes: [
						{
							id: 'y-axis-1',
							type: 'linear',
							display: true,
							position: 'left',
							ticks: {
								min: 0
							}
						},
					]
				}
			};
		}

		function getValuesOfKey(data,key){
			var values = data.map(function(value) {
				  return value[key];
			});

			return values;
		}

		function getLengthValuesOfKey(data,key){
			var values = data.map(function(value) {
				if (value[key]==undefined) {
					return 0;
				}
			    return value[key].length;
			});

			return values;
		}

		function checkFilterDate(date) {
			if (date.from > date.to) {
				AlertService.push("Noo!, your date range is incorrect");
				AlertService.push("Please check it again before query the result 😇");
				vm.errorMessage = "Date range is incorrect";
			} else {
				vm.errorMessage = '';
			}
		}

		function addFilter() {
			if (!vm.filter.filters) {
				vm.filter.filters = [];
			}
			const filter = {
				connection: 'and',
				first_var: '',
				second_var: '',
				operation: ''
			}
			vm.filter.filters.push(filter);
		}

		function prepareExport(users) {
			// Get attributes
			vm.export = {};
			vm.export.header = ['index', 'create_at', 'last_update'];
			_.forEach(users, function (user) {
				_.forEach(user.attr, function (val, key) {
					if(vm.export.header.indexOf(key) < 0) {
						vm.export.header.push(key);
					}
				})
			});

			// Get value
			_.forEach(users, function (user, index) {
				if(!vm.export.body) {
					vm.export.body = [];
				}
				vm.export.body[index] = [
					index + 1,
					new Date(user['create_at']*1000).toLocaleDateString(),
					new Date(user['last_update']*1000).toLocaleDateString()
				];
				
				_.forEach(vm.export.header, function(attr, idx) {
					if(idx >= 3) {
						vm.export.body[index].push(user.attr[attr] || '');
					}
				});
			});
		}
	}

})();
