(function () {
	'use strict';

	angular
		.module('app')
		.controller('broadcastCtrl', broadcastCtrl);

	broadcastCtrl.$inject = ['$scope', '$state', '$stateParams', '$http', 'Bots', '$cookies', 'ApiEndpointURL', '$q', 'BotsApi'];

	function broadcastCtrl($scope, $state, $stateParams, $http, Bots, $cookies, ApiEndpointURL, $q, BotsApi) {
		var vm = this;

		// Initial variables
		vm.bot_id = $stateParams.bot_id;
		vm.broadcast_id = $stateParams.broadcast_id;
		vm.bot = {};
		vm.modalStatus = '';
		vm.pageAccessToken = undefined;

		vm.selectBroadcast = selectBroadcast;
		vm.addBroadcast = addBroadcast;
		vm.deleteBroadcast = deleteBroadcast;
		vm.saveBroadcast = saveBroadcast;
		vm.changeBroadcastType = changeBroadcastType;
		vm.saveDateTime = saveDateTime;
		vm.saveInterval = saveInterval;

		// Node
		vm.addNode = addNode;
		vm.deleteNode = deleteNode;
		vm.saveNode = saveNode;
		vm.addReply = addReply;
		vm.toggleAI = toggleAI;
		vm.deleteReply = deleteReply;
		vm.addCarousel = addCarousel;
		vm.deleteCarousel = deleteCarousel;
		vm.addNodeCarouselButton = addNodeCarouselButton;
		vm.toggleImageAspect = toggleImageAspect;

		// Button
		vm.showPanel = showPanel;
		vm.showEdit = showEdit;
		vm.editButton = editButton;
		vm.showEditReply = showEditReply;
		vm.closePanel = closePanel;
		vm.addNodeButton = addNodeButton;
		vm.deleteButton = deleteButton;
		vm.filterButton = filterButton;

		// Util
		vm.uploadCarousel = uploadCarousel;
		vm.upload = upload;
		vm.sortableOptions = {
			axis: 'y',
			handle: '> .card-drag',
			'ui-floating': false,
			items: '.sortable-card',
			stop: function (event, ui) {
				vm.currentBroadcast.nodes.forEach(function (node, idx) {
					if (node.panel) {
						closePanel(node);
					}
					if (node.node_type == 'carousel') {
						if (!node.nodeResponse.response[0].panel) {
							node.nodeResponse.response[0].panel = {};
						}
						node.nodeResponse.response[0].panel.show = false;
						node.nodeResponse.response[0].panel.edit = false;
					}
				})
				vm.currentBroadcast.$save();
			}
		};

		function toggleImageAspect(node) {
			if (node.image_aspect_ratio == 'horizontal') {
				node.image_aspect_ratio = 'square';
			} else {
				node.image_aspect_ratio = 'horizontal';
			}
		}

		vm.sortableCarousel = {
			axis: 'x',
			'ui-floating': true,
			items: '.sortable-carousel',
			stop: function (event, ui) {
				vm.saveBroadcast();
			}
		}

		vm.sortableQuickReply = {
			axis: 'x',
			'ui-floating': true,
			items: '.sortable-quickreply',
			stop: function (event, ui) {
				vm.saveBroadcast();
			}
		}

		init();

		function init() {
			getBot();
		}

		/**
		 * Get bot according to the bot id from the url
		 */
		function getBot() {
			if (vm.bot_id) {
				vm.bot = Bots.getBot(vm.bot_id);
				if (vm.bot) {
					getBroadcast().then(function () {
						vm.currentBroadcast.$watch(function () {
							selectBroadcast();
						});
					});
				}

				BotsApi.getBot(vm.bot_id)
					.then(function (bot) {
						vm.pageAccessToken = bot.channels.FACEBOOK.accessToken;
					});
			}
		}

		function getBroadcast() {
			var defer = $q.defer();
			if (!vm.broadcast_id) {
				defer.reject();
			}
			Bots.getBroadcast(vm.broadcast_id)
				.then(function (broadcast) {
					vm.currentBroadcast = broadcast;
					selectBroadcast();
					defer.resolve()
				});
			return defer.promise;
		}

		function selectBroadcast() {
			var datetime = new Date(vm.currentBroadcast.broadcast_time);
			vm.currentBroadcast.date = datetime;
			vm.currentBroadcast.time = datetime;

			if (vm.currentBroadcast.broadcast_interval === '0') {
				vm.currentBroadcast.interval = 'one_time';
			} else {
				var intervalList = [60, 60, 24, 7, 4];
				var ret = vm.currentBroadcast.broadcast_interval;
				for (var i = 0; i < intervalList.length; i++) {
					ret = ret / intervalList[i];
					if (ret == 1) {
						switch (i) {
							case 1: vm.currentBroadcast.interval = 'hour'; break;
							case 2: vm.currentBroadcast.interval = 'day'; break;
							case 3: vm.currentBroadcast.interval = 'week'; break;
							case 4: vm.currentBroadcast.interval = 'month'; break;
						}
					}
				}
			}
			if (!vm.nodes) {
				Bots
					.getBroadcastNodes(vm.broadcast_id)
					.then(function (nodes) {
						vm.nodes = nodes;
					})
					.catch(function (err) {
						console.log(err);
					});
			}
		}

		function addBroadcast() {
			Bots
				.addBroadcast(vm.bot_id);
		}

		/**
		 * Delete block
		 */
		function deleteBroadcast() {
			vm.currentBroadcast.$remove()
				.catch(function (error) {
					console.log(error)
				});
		}

		function changeBroadcastType() {
			if (vm.currentBroadcast.enable === false) {
				Bots.getSubscribMessagingStatus(vm.pageAccessToken)
					.then(function (messagingFeatureReviews) {
						var subscriptionMessaging = undefined;
						messagingFeatureReviews.data.forEach(function (messagingFeatureReview) {
							if (messagingFeatureReview.feature === 'subscription_messaging') {
								subscriptionMessaging = messagingFeatureReview.status;
								vm.saveBroadcast();
								break;
							}
						});
						if (subscriptionMessaging === 'approved') {
							vm.currentBroadcast.enable = true;
							vm.saveBroadcast();
						} else if (subscriptionMessaging === 'pending') {
							vm.modalStatus = 'pending'
							vm.saveBroadcast();
						} else {
							vm.currentBroadcast.enable = true;
							vm.modalStatus = 'review'
							vm.saveBroadcast();
						}
					})
					.catch(function () {
						vm.currentBroadcast.enable = true;
						vm.modalStatus = 'review'
						// show modal app review
						vm.saveBroadcast();
					});
			} else {
				vm.currentBroadcast.enable = false;
			}
			vm.saveBroadcast();
		}

		function saveDateTime() {
			if (vm.currentBroadcast.date && vm.currentBroadcast.time) {
				var splitDate = vm.currentBroadcast.date.toDateString();
				var splitTime = vm.currentBroadcast.time.toTimeString();
				var dateObj = splitDate + ' ' + splitTime;
				vm.currentBroadcast.broadcast_time = new Date(dateObj).getTime()
			}
			vm.saveBroadcast();
		}

		function saveInterval(interval) {
			vm.currentBroadcast.broadcast_type = 'interval';
			if (interval === 'hour') {
				vm.currentBroadcast.broadcast_interval = 60 * 60;
			} else if (interval === 'day') {
				vm.currentBroadcast.broadcast_interval = 60 * 60 * 24;
			} else if (interval === 'week') {
				vm.currentBroadcast.broadcast_interval = 60 * 60 * 24 * 7;
			} else if (interval === 'month') {
				vm.currentBroadcast.broadcast_interval = 60 * 60 * 24 * 7 * 4;
			} else if (interval === 'one_time') {
				vm.currentBroadcast.broadcast_interval = 0;
				vm.currentBroadcast.broadcast_type = 'one_time';
			}
			vm.saveBroadcast();
		}

		/**
		 * Save block
		 */
		function saveBlock() {
			vm.bot.$save().then(function () {
				if (vm.currentBroadcast.broadcast_id) {
					vm.currentBroadcast.$save()
						.then(function() {
							Bots.update('bot_' + vm.bot.bot_id)
						});
				} else {
					vm.getBlock('0');
				}
			});
		}

		/**
		 * Add new node to the block
		 * @param type
		 */
		function addNode(type) {
			Bots.addNodeBroadcast(type, vm.currentBroadcast);
		}

		/**
		 * Delete node from block
		 * @param node
		 */
		function deleteNode(node, index) {
			vm.currentBroadcast.nodes.splice(index, 1);
			vm.currentBroadcast.$save();
		}

		function saveBroadcast() {
			vm.currentBroadcast.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		function saveNode(node) {
			vm.currentBroadcast.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		/**
		 * Add reply to the list
		 * @param node
		 * @param button
		 */
		function addReply(node, button) {
			var b = {};
			b = {
				type: 'show_block',
				title: button.title
			}

			if (node.panel.type == 'show_block' && button.data) {
				b.blocks = [{
					block_id: button.data.block_id,
					block_name: button.data.block_name
				}]
			}
			if (!node.nodeResponse.response || node.nodeResponse.response === '') {
				node.nodeResponse.response = [];
			}
			node.nodeResponse.response.push(b);

			vm.tempNode = {};
			delete node.panel;
			saveNode(node)
		}


		function toggleAI(node) {
			node.use_ai = !node.use_ai;
			saveNode(node);
		}

		/**
		 * Delete reply from the list
		 * @param node
		 * @param button_index
		 */
		function deleteReply(node, button_index) {
			node.nodeResponse.response.splice(button_index, 1);
			saveNode(node);
		}

		/**
		 * Add carousel to the list.
		 * @param node
		 */
		function addCarousel(node) {
			Bots.addCarousel(node);
			saveNode(node);
		}

		/**
		 * Delete carousel from the list
		 * @param node
		 * @param carousel_index
		 */
		function deleteCarousel(node, carousel_index, node_index) {
			if (node.nodeResponse.response.length > 1) {
				node.nodeResponse.response.splice(carousel_index, 1);
				saveNode(node);
			} else {
				deleteNode(node, node_index);
			}
		}

		/**
		 * Show button panel
		 * @param node
		 */
		function showPanel(node) {
			node.panel = {};
			node.panel.show = true;
			node.panel.type = 'show_block';
		}

		function showEdit(node, index) {
			node.panel = {};
			node.panel.edit = true;
			node.panel.index = index;
			if (!node.buttons[index].blocks) {
				node.buttons[index].blocks = [];
			}
		}

		function showEditReply(node, index) {
			node.panel = {};
			node.panel.edit = true;
			node.panel.index = index;
			if (!node.nodeResponse.response[index].blocks) {
				node.nodeResponse.response[index].blocks = [];
			}
		}

		/**
		 * Close button panel
		 * @param node
		 */
		function closePanel(node) {
			node.panel.show = false;
		}

		/**
		 * Add button to the buttons set
		 * @param node
		 * @param button
		 */
		function addNodeButton(node, button, blocks, webview_height_ratio) {
			var b = {};
			if (node.panel.type == 'show_block') {
				b = {
					type: node.panel.type,
					title: button.title,
					blocks: blocks,
				}
			} else if (node.panel.type == 'web_url') {
				b = {
					type: node.panel.type,
					title: button.title,
					url: button.filter,
					webview_height_ratio: webview_height_ratio
				}
			} else {
				b = {
					type: node.panel.type,
					title: button.title,
					phone_number: button.filter
				}
			}

			if (!node.buttons) {
				node.buttons = [];
			}

			if (node.buttons.length <= 3) {
				node.buttons.push(b);
			}

			vm.tempNode = {};
			delete node.panel;
			saveNode(node);
		}

		function addNodeCarouselButton(carousel, button, blocks, node, webview_height_ratio) {
			var b = {};
			if (carousel.panel.type == 'show_block') {
				b = {
					type: carousel.panel.type,
					title: button.title,
					blocks: blocks
				}
			} else if (carousel.panel.type == 'web_url') {
				b = {
					type: carousel.panel.type,
					title: button.title,
					url: button.filter,
					webview_height_ratio: webview_height_ratio
				}
			} else {
				b = {
					type: carousel.panel.type,
					title: button.title,
					phone_number: button.filter
				}
			}

			if (!carousel.buttons) {
				carousel.buttons = [];
			}

			if (carousel.buttons.length <= 3) {
				carousel.buttons.push(b);
			}
			vm.tempNode = {};
			delete carousel.panel;
			saveNode(node);
		}

		function editButton(node) {
			var b = {};
			if (node.buttons[node.panel.index].type == 'show_block') {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					blocks: node.buttons[node.panel.index].blocks
				}
			} else if (node.buttons[node.panel.index].type == 'web_url') {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					url: node.buttons[node.panel.index].url,
					webview_height_ratio: node.buttons[node.panel.index].webview_height_ratio
				}
			} else {
				b = {
					type: node.buttons[node.panel.index].type,
					title: node.buttons[node.panel.index].title,
					phone_number: node.buttons[node.panel.index].phone_number
				}
			}
			node.buttons[node.panel.index] = b;
			node.panel.edit = false;
			saveNode(node);
		}

		/**
		 * Delete button from the buttons set
		 * @param node
		 * @param button_index
		 */
		function deleteButton(node, button_index) {
			node.buttons.splice(button_index, 1);
			saveNode(node);
		}

		/**
		 * Filter Block for button suggestion
		 * @param blocks
		 * @param filter
		 */
		function filterButton(blocks, filter) {
			var retval = _.filter(blocks, function (v, k) {
				if (v && v.block_name && filter) {
					return v.block_name.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
				}
			});
			return retval;
		}

		function uploadCarousel(file, node, item, index) {
			var isImage = file.type.indexOf("image") > -1;
			var isGif = file.type.indexOf("gif") > -1;
			item.image_url = true;
			if(isImage && !isGif) {
				new ImageCompressor(file, {
					quality: .75,
					maxWidth: 500,
					success(result) {
						var uploadTask = firebase.storage().ref().child(_guid()).put(result);
						uploadTask.then(function (data) {
							item.image_url = data.downloadURL;
							node.nodeResponse.response[index] = item;
							saveNode(node);
							vm.image_aspect = {
								node: node,
								show: true
							}
						});
					},
					error(e) {
						console.log(e.message);
					}
				});
			} else {
				var uploadTask = firebase.storage().ref().child(_guid()).put(file);
				uploadTask.then(function (data) {
					item.image_url = data.downloadURL;
					node.nodeResponse.response[index] = item;
					saveNode(node);
					vm.image_aspect = {
						node: node,
						show: true
					}
				});
			}
		}

		/**
		 * Upload file (image) to the server
		 * @param file
		 * @param node
		 */
		function upload(file, node) {
			var isImage = file.type.indexOf("image") > -1;
			var isGif = file.type.indexOf("gif") > -1;
			node.nodeResponse.response = true;
			if(isImage && !isGif) {
				new ImageCompressor(file, {
					quality: .75,
					maxWidth: 500,
					success(result) {
						var uploadTask = firebase.storage().ref().child(_guid()).put(result);
						uploadTask.then(function (data) {
							node.nodeResponse.response = data.downloadURL;
							saveNode(node);
						})
					},
					error(e) {
						console.log(e.message);
					}
				});
			} else {
				var uploadTask = firebase.storage().ref().child(_guid()).put(file);
				uploadTask.then(function (data) {
					node.nodeResponse.response = data.downloadURL;
					saveNode(node);
				})
			}
		}

		/**
		 * Private function
		 * Use to generete uniqe id
		 */
		function _guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			}
			return s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4();
		}
	}
})();
