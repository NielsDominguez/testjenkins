(function () {
	'use strict';

	angular
		.module('app')
		.controller('passwordCtrl', passwordCtrl);

	passwordCtrl.$inject = ['$state', '$stateParams', 'AlertService'];

	function passwordCtrl($state, $stateParams, AlertService) {
		var vm = this;

		vm.resetPassword = resetPassword;
		vm.now = Date.now().toString();
		vm.errorMessage = '';

		function resetPassword(user) {
			vm.errorMessage = "";
			if(!user.password) {
				vm.errorMessage = "Please type new password";
			}
			if(user.password.length < 6) {
				vm.errorMessage = "Password must have more then 6 charactors";
			}
			if($stateParams.mode === 'resetPassword') {
				var actionCode = $stateParams.oobCode;
				firebase.auth().verifyPasswordResetCode(actionCode)
          .then(function(email) {
            firebase.auth().confirmPasswordReset(actionCode, user.password).then(function(resp) {
            	$state.go('auth.signin');
            }).catch(function(error) {
              AlertService.push(error);
              return error;
            });
          }).catch(function(error) {
            AlertService.push(error);
            return error;
          });
			}
		}
	}
})();