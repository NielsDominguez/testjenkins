(function () {
  'use strict';

  angular
    .module('app')
    .controller('VerifyEmailController', verifyEmailController);

  verifyEmailController.$inject = ['Auth', 'AlertService', 'SIDService'];

  function verifyEmailController(Auth, AlertService, SIDService) {
    var vm = this;
    vm.hasSent = false;
    vm.resendEmail = resendEmail;
    vm.signOut = signOut;
    vm.logo = SIDService.getLogo();

    function resendEmail() {
      if (vm.hasSent) {
        AlertService.push('Calm down!, check your inbox please..😫');
        return;
      }
      Auth.sendEmailVerification()
        .then(function () {
          vm.hasSent = true;
        });
    }

    function signOut() {
      Auth.signOutFirebase();
    }
  }
})();
