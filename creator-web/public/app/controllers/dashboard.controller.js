(function () {
	'use strict';

	angular
		.module('app')
		.controller('dashboardCtrl', dashboardCtrl);

	dashboardCtrl.$inject = ['$state', '$rootScope', 'Auth', '$http', '$cookies', '$window', 'ApiEndpointURL', '$interval', 'currentAuth', 'Bots', 'Facebook', 'AlertService', 'SIDService'];

	function dashboardCtrl($state, $rootScope, Auth, $http, $cookies, $window, ApiEndpointURL, $interval, currentAuth, Bots, Facebook, AlertService, SIDService) {
		var vm = this;

		vm.bots = {};
		vm.signout = signout;
		vm.createBot = createBot;
		vm.deleteBot = deleteBot;
		vm.updatePassword = updatePassword;
		vm.linkFacebook = linkFacebook;
		vm.unlinkFacebook = unlinkFacebook;
		vm.isModal = false;
		vm.init = init;
		vm.fbLoginStatus = false;
		vm.fbName = '';
		vm.profilePicUrl = 'https://firebasestorage.googleapis.com/v0/b/bot-platform-a5a5a.appspot.com/o/0056-7110-e83a-62bd-c31c?alt=media&token=88378c8e-e075-4788-b5dd-a338d3b13fb1';
		vm.fbId = '';
		vm.logo = SIDService.getLogo();


		init();

		function init() {
			currentAuth.getIdToken().then(function (token) {
				$http.get(ApiEndpointURL + '/bot', { headers: { Authorization: token } })
					.then(function success(bots) {
						vm.user = Auth.getUser();
						vm.bots = bots.data;
					}, function error(response) {

					});
			});
			checkFbLoginStatus();
		}

		function signout() {
			Auth.signOutFirebase()
				.catch(function (error) {
					$rootScope.errorMessage = error;
				});
		}

		function createBot(bot) {
			if(!bot || !bot.name || bot.name.length === 0) {
				AlertService.push('Please specify your bot name.');
				return false;
			}
			currentAuth.getIdToken().then(function (token) {
				$http.post(ApiEndpointURL + "/bot", bot, { headers: { Authorization: token } })
					.then(function success(bot) {
						vm.isModal = false;
						Bots.init(bot.data, currentAuth.uid).then(function () {
							vm.init();
						});
						AlertService.push('Bot has been created :P');
						vm.bot = { "name": "" }
						vm.isModel = false;
					})
					.catch(function(error) {
						AlertService.push(error);
						vm.bot = { "name": "" }
						vm.isModel = false;
					});
			});
		}

		function deleteBot(bot) {
			if ($window.confirm("Are you sure to delete " + bot.bot_name + "?")) {
				currentAuth.getIdToken().then(function (token) {
					Bots.deleteBot(bot.bot_id)
						.then(function () {
							$http.delete(ApiEndpointURL + "/bot/" + bot.bot_id, { headers: { Authorization: token } })
								.then(function success(response) {
									vm.init();
								});
						})
				}, function error(sub_response) {
					$rootScope.errorMessage = sub_response;
				});
			}
		}

		function updatePassword(newPassword) {
			Auth.updatePassword(newPassword);
		}

		function checkFbLoginStatus() {
			Facebook.getAccessToken()
				.then(function (response) {
					if(response.token) {
						Facebook.checkToken(response.token)
							.then(function (response) {
								vm.fbName = response.name;
								vm.fbId = response.id;
								vm.profilePicUrl = 'https://graph.facebook.com/v2.10/'+ vm.fbId +'/picture';
								vm.fbLoginStatus = true;
							})
					}
				})
		}

		function linkFacebook() {
			Facebook.fbLogin()
			.then(function (response) {
				AlertService.push('Facebook has been linked');
				checkFbLoginStatus();
				Facebook.getLongLiveToken(response)
					.then(function(response) {
						Facebook.checkToken(response)
							.then(function (response) {
								vm.fbName = response.name;
								vm.fbId = response.id;
								vm.profilePicUrl = 'https://graph.facebook.com/v2.10/'+ vm.fbId +'/picture';
								vm.fbLoginStatus = true;
							})
					})
			})
		}

		function unlinkFacebook() {
			Facebook.fbLogout()
				.then(function (response) {
					vm.fbName = '';
					vm.fbId = '';
					vm.profilePicUrl = 'https://firebasestorage.googleapis.com/v0/b/bot-platform-a5a5a.appspot.com/o/0056-7110-e83a-62bd-c31c?alt=media&token=88378c8e-e075-4788-b5dd-a338d3b13fb1';
					vm.fbLoginStatus = false;
				});
		}

	}
})();
