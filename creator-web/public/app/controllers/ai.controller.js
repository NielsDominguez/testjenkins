(function () {
	'use strict';

	angular
		.module('app')
		.controller('aiCtrl', aiCtrl);

	aiCtrl.$inject = ['$state', '$stateParams', '$http', 'Bots', '$cookies', 'ApiEndpointURL'];

	function aiCtrl($state, $stateParams, $http, Bots, $cookies, ApiEndpointURL) {
		var vm = this;

		// Initial variables
		vm.bot_id = $stateParams.bot_id;
		vm.bot = {};

		vm.addRule = addRule;
		vm.deleteItem = deleteItem;
		vm.addFilter = addFilter;
		vm.addAction = addAction;
		vm.addText = addText;

		vm.filterButton = filterButton;

		init();

		function init() {
			getBot();
		}

		/**
		 * Get bot according to the bot id from the url
		 */
		function getBot() {
			if (vm.bot_id) {
				vm.bot = Bots.getBot(vm.bot_id);
			}
		}

		function addRule() {
			Bots.addAIRule();
		}

		function addFilter(intent) {
			if (!intent.filters) {
				intent.filters = [];
			}
			if(!intent.tempFilter) {
				return false;
			}
			if (_.indexOf(intent.filters, intent.tempFilter) < 0) {
				intent.filters.push(intent.tempFilter);
			}
			delete intent.tempFilter;
			vm.bot.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		function deleteItem(items, index) {
			items.splice(index, 1);
			vm.bot.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		function addAction(intent) {
			if (!intent.blocks) {
				intent.blocks = [];
			}
			if(!intent.tempAction) {
				return false;
			}
			if (_.indexOf(intent.blcoks, intent.tempAction) < 0) {
				intent.blocks.push(intent.tempAction);
			}
			delete intent.tempAction;
			delete intent.showSuggestion;
			vm.bot.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		function addText(intent) {
			if (!intent.texts) {
				intent.texts = [];
			}
			if(!intent.tempText || intent.tempText.text.length <= 0) {
				return false;
			}
			if (_.indexOf(intent.texts, intent.tempText.text) < 0) {
				intent.texts.push(intent.tempText.text);
			}
			delete intent.tempText;
			vm.bot.$save()
				.then(function () {
					Bots.update('bot_' + vm.bot.bot_id)
				});
		}

		/**
		 * Filter Block for button suggestion
		 * @param blocks 
		 * @param filter 
		 */
		function filterButton(blocks, filter) {
			if(!filter) {
				return false;
			}
			var retval = _.filter(blocks, function (v, k) {
				return v.block_name.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
			});
			return retval;
		}

	}

})();
