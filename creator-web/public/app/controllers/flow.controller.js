(function () {
	'use strict';

	angular
		.module('app')
		.controller('flowController', flowController);

	flowController.$inject = ['$scope', '$stateParams', 'Auth', 'Facebook', 'SIDService'];

	function flowController($scope, $stateParams, Auth, Facebook, SIDService) {
		var vm = this;
		vm.profilePicUrl = 'https://firebasestorage.googleapis.com/v0/b/bot-platform-a5a5a.appspot.com/o/0056-7110-e83a-62bd-c31c?alt=media&token=88378c8e-e075-4788-b5dd-a338d3b13fb1';
		vm.signout = signout;
		// Initial variables
		$scope.bot_id = $stateParams.bot_id;
		vm.user = Auth.getUser();
		vm.logo = SIDService.getLogo();
		
		function signout() {
			Auth.signOutFirebase()
				.catch(function (error) {
					vm.errorMessage = error;
				});
		}
		

	}
})();
