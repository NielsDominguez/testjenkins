(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeJson', nodeJson);

	nodeJson.$inject = [];

	function nodeJson() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.json.directive/node.json.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
