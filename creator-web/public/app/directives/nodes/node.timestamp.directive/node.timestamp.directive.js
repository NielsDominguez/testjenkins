(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeTimeStamp', nodeTimeStamp);

	nodeTimeStamp.$inject = [];

	function nodeTimeStamp() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.timestamp.directive/node.timestamp.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {

		};


	}
})();
