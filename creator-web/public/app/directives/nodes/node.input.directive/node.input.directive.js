(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeInput', nodeInput);

	nodeInput.$inject = [];

	function nodeInput() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.input.directive/node.input.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
