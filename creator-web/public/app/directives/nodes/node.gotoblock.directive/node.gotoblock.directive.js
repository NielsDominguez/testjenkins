(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeGotoBlock', nodeGotoBlock);

	nodeGotoBlock.$inject = [];

	function nodeGotoBlock() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.gotoblock.directive/node.gotoblock.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
