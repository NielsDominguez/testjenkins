(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeLiveChat', nodeLiveChat);

			nodeLiveChat.$inject = [];

	function nodeLiveChat() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.liveChat.directive/node.liveChat.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
