(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeLocation', nodeLocation);

			nodeLocation.$inject = [];

	function nodeLocation() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.location.directive/node.location.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
