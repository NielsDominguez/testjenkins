(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeButton', nodeButton);

	nodeButton.$inject = [];

	function nodeButton() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.button.directive/node.button.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
