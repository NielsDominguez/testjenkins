(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeVdo', nodeVdo);

	nodeVdo.$inject = [];

	function nodeVdo() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.vdo.directive/node.vdo.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
