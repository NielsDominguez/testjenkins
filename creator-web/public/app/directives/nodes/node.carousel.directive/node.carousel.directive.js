(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeCarousel', nodeCarousel);

	nodeCarousel.$inject = [];

	function nodeCarousel() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.carousel.directive/node.carousel.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
