(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeAudio', nodeAudio);

	nodeAudio.$inject = [];

	function nodeAudio() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.audio.directive/node.audio.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
