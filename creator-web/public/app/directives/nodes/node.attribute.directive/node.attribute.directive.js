(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeAttribute', nodeAttribute);

	nodeAttribute.$inject = [];

	function nodeAttribute() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.attribute.directive/node.attribute.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
