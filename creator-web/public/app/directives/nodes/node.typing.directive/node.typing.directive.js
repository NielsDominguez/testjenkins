(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeTyping', nodeTyping);

	nodeTyping.$inject = [];

	function nodeTyping() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.typing.directive/node.typing.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
