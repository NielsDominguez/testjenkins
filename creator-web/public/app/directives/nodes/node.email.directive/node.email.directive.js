(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeEmail', nodeEmail);

	nodeEmail.$inject = [];

	function nodeEmail() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.email.directive/node.email.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
