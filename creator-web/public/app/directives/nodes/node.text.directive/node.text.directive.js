(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeText', nodeText);

	nodeText.$inject = [];

	function nodeText() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.text.directive/node.text.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
