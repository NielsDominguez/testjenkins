(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeGettime', nodeGettime);

	nodeGettime.$inject = [];

	function nodeGettime() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.gettime.directive/node.gettime.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
