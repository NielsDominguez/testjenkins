(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeImage', nodeImage);

	nodeImage.$inject = [];

	function nodeImage() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.image.directive/node.image.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
