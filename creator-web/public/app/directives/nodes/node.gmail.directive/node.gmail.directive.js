(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeGmail', nodeGmail);

	nodeGmail.$inject = [];

	function nodeGmail() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.gmail.directive/node.gmail.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
