(function() {
	'use strict';

	angular
			.module('app')
			.directive('nodeFile', nodeFile);

	nodeFile.$inject = [];

	function nodeFile() {
		var directive = {
			link: link,
			restrict: 'EA',
			templateUrl: '/app/directives/nodes/node.file.directive/node.file.tpl.html',
		};
		return directive;

		function link(scope, elem, attrs) {
				
		};


	}
})();
