(function () {
  'use strict';

  angular
    .module('app')
    .directive('alert', alert);

  alert.$inject = ['AlertService'];

  function alert() {
    var directive = {
      link: link,
      restrict: 'EA',
      templateUrl: '/app/directives/alert.directive/alert.directive.tpl.html',
      controller: alertController,
      controllerAs: 'vm'
    };
    return directive;

    function link(scope, elem, attrs) {
    };

  }

  function alertController(AlertService) {
    var vm = this;
    vm.messages = [];
    
    AlertService.subscribe(setMessages);

    function setMessages(messages) {
      vm.messages = messages;
    }
  }
})();
