(function() {
	'use strict';

	angular
			.module('app')
			.directive('loading', loading);

	loading.$inject = ['$http', '$rootScope', '$timeout'];

	function loading($http, $rootScope,	$timeout) {
		var directive = {
			link: link,
			restrict: 'A',
		};
		return directive;

		function link(scope, elem, attrs) {
			$rootScope.$on('$stateChangeStart', function() {
				$rootScope.layout.loading = true;
			});

			$rootScope.$on('$stateChangeSuccess', function () {
				$timeout(function(){
					$rootScope.layout.loading = false;
				}, 2000);
			});

      scope.isLoading = () => {
        return $http.pendingRequests.length > 0;
      }

      scope.$watch(scope.isLoading, (isLoading) => {
        if (isLoading && $rootScope.layout.loading) {
          elem[0].style.display = 'flex';
        } else {
          elem[0].style.display = 'none';
        }
      })
		};


	}
})();
