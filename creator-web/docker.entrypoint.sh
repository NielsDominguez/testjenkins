#!/bin/sh
env_vars='API_ENDPOINT_URL FACEBOOK_APP_ID SITE_URL FIREBASE_APIKEY FIREBASE_AUTH_DOMAIN FIREBASE_DATABASE_URL FIREBASE_PROJECT_ID FIREBASE_STORAGE_BUCKET FIREBASE_MESSAGING_SENDER_ID ENABLE_SID'

for var in $env_vars; do
    eval value=\$$var
    echo "\$$var=$value"

    escaped_var=$(echo $value | sed 's/\./\\./g')
    escaped_var=$(echo $escaped_var | sed 's=/=\\/=g')

    sed -i -e "s/\$$var/$escaped_var/g" /usr/share/nginx/html/app/app.constant.js
done

exec "$@"