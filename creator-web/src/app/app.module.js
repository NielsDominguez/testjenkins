(function() {
	'use strict';

	angular
			.module('app', [
				'ui.router',
				'firebase',
				'monospaced.elastic',
				'rzModule',
				'ngFileUpload',
				'chart.js',
				'ui.sortable',
				'ngclipboard',
				'ngCookies',
				'ngSanitize',
				'ngCsv',
				'apMesa'
			]);

})();
