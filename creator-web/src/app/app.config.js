(function () {
	'use strict';

	angular
		.module('app')
		.config(config)
	config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'ChartJsProvider'];

	function config($stateProvider, $urlRouterProvider, $locationProvider, ChartJsProvider) {
		// $locationProvider.html5Mode(true);

		// Dashboard
		var dashboard = {
			name: 'dashboard',
			url: '/dashboard',
			templateUrl: './pages/home/dashboard.html',
			controller: 'dashboardCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		var me = {
			name: 'me',
			url: '/me',
			templateUrl: './pages/home/me.html',
			controller: 'dashboardCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		// Authentication
		var auth = {
			name: 'auth',
			url: '/auth',
			templateUrl: '/pages/auth/auth.html',
			controller: 'authCtrl',
			controllerAs: 'vm',
		}
		var signin = {
			name: 'auth.signin',
			url: '/signin',
			templateUrl: './pages/auth/signin.html',
			controller: 'authCtrl',
			controllerAs: 'vm',
			onEnter: checkVerifiedEmail
		}

		var signup = {
			name: 'auth.signup',
			url: '/signup',
			templateUrl: './pages/auth/signup.html',
			controller: 'authCtrl',
			controllerAs: 'vm',
			onEnter: checkVerifiedEmail
		}

		var forgetPassword = {
			name: 'auth.forgetPassword',
			url: '/forgetPassword',
			templateUrl: './pages/auth/forgetPassword.html',
			controller: 'authCtrl',
			controllerAs: 'vm',
			onEnter: checkVerifiedEmail
		}

		var resetPassword = {
			name: 'resetPassword',
			url: '/resetPassword?mode&oobCode&apiKey',
			templateUrl: './pages/auth/resetPassword.html',
			controller: 'passwordCtrl',
			controllerAs: 'vm',
		}

		var emailVerification = {
			name: 'verify',
			url: '/verify',
			templateUrl: './pages/auth/verify-email.html',
			controller: 'VerifyEmailController',
			controllerAs: 'vm',
			onEnter: checkVerifiedEmail
		}

		// Bot
		var bot = {
			name: 'bot',
			url: '/bot/:bot_id',
			templateUrl: './pages/bot/flowmanager.html',
			controller: 'flowController',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}
		var botBlock = {
			name: 'bot.block',
			url: '/blocks',
			templateUrl: './pages/bot/block.html',
			controller: 'botCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		// Broadcast
		var botBroadcast = {
			name: 'bot.broadcast',
			url: '/broadcast/:broadcast_id',
			templateUrl: './pages/bot/broadcast.html',
			controller: 'broadcastCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		// AI
		var botAi = {
			name: 'bot.ai',
			url: '/ai',
			templateUrl: './pages/bot/ai.html',
			controller: 'aiCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		// Analytics
		var botAnalytics = {
			name: 'bot.analytics',
			url: '/analytics',
			templateUrl: './pages/bot/analytics.html',
			controller: 'analyticsCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		// Config
		var botConfig = {
			name: 'bot.configure',
			url: '/configure',
			templateUrl: './pages/bot/configure.html',
			controller: 'configureCtrl',
			controllerAs: 'vm',
			resolve: getResolveRequireSignIn()
		}

		$stateProvider.state(dashboard);
		$stateProvider.state(me);
		$stateProvider.state(auth);
		$stateProvider.state(signin);
		$stateProvider.state(signup);
		$stateProvider.state(forgetPassword);
		$stateProvider.state(resetPassword);
		$stateProvider.state(emailVerification);
		$stateProvider.state(bot);
		$stateProvider.state(botBlock);
		$stateProvider.state(botBroadcast);
		$stateProvider.state(botAi);
		$stateProvider.state(botAnalytics);
		$stateProvider.state(botConfig);
		$urlRouterProvider.otherwise("/dashboard");

		ChartJsProvider = function () {
			ChartJsProvider.setOptions({ colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
		}

		function getResolveRequireSignIn() {
			return {
				"currentAuth": ["Auth", function (Auth) {
					return Auth.requireSignInWithEmailVerification();
				}]
			};
		}

		function checkVerifiedEmail(Auth, $state) {
			Auth.requireSignInWithEmailVerification().then(function () {
				$state.go('dashboard');
			});
		}

	}
})();
