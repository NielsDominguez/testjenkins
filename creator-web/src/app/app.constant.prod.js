(function () {
    'use strict';

    angular.module('app')
        .constant('ApiEndpointURL', 'https://api-my.hbot.io/v1')
        .constant('FacebookAppId', 1862327030646757)
        .constant('SiteURL', 'https://sid.hbot.io')
        .constant('firebaseConfig',{
            apiKey: 'AIzaSyD4qsDj_JHZSvwddWQ-t1XzUoJt4lpy8Ng',
            authDomain: 'bot-platform-a5a5a.firebaseapp.com',
            databaseURL: 'https://bot-platform-a5a5a.firebaseio.com',
            projectId: 'bot-platform-a5a5a',
            storageBucket: 'bot-platform-a5a5a.appspot.com',
            messagingSenderId: '240686906278'})
        .constant('enableSID', true)
})();
