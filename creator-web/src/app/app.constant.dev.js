(function () {
    'use strict';

    angular.module('app')
        .constant('ApiEndpointURL', 'https://api.dev.hbot.io/v1')
        .constant('FacebookAppId', 119744278746273)
        .constant('SiteURL', null)
        .constant('firebaseConfig', {
            apiKey: 'AIzaSyCn2Yd9M1cTj5QE-4LDC95LMc-YSXTFOtY',
            authDomain: 'bot-platform-dev.firebaseapp.com',
            databaseURL: 'https://bot-platform-dev.firebaseio.com',
            projectId: 'bot-platform-dev',
            storageBucket: 'bot-platform-dev.appspot.com',
            messagingSenderId: '328721655708'})
        .constant('enableSID', false)
})();
