(function () {
	'use strict';

	angular
		.module('app')
		.run(['$rootScope', 'firebaseConfig', appRun])
		.run(['FacebookAppId', initFacebook])
		.run(["$rootScope", "$state", onAuthStateChangeError]);

	function appRun($rootScope, firebaseConfig) {
		$rootScope.layout = {};
		moment.locale('th');
		firebase.initializeApp(firebaseConfig);
		Chart.defaults.global.defaultFontColor = '#fff'; 
	}

	function onAuthStateChangeError($rootScope, $state) {
		$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
			if (error === "AUTH_REQUIRED") {
				$state.go("auth.signin");
			}

			if (error === "EMAIL_VERIFICATION_REQUIRED") {
				// redirect to resend email verification page
				$state.go("verify");
			}
		});
	}

	function initFacebook(appId) {
		window.fbAsyncInit = function () {
			FB.init({
				appId: appId,
				autoLogAppEvents: true,
				xfbml: true,
				version: 'v2.8'
			});
			FB.AppEvents.logPageView();
		};

		(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) { return; }
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		} (document, 'script', 'facebook-jssdk'));
	}
})();
