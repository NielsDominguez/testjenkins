(function() {
	'use strict';

	angular
			.module('app')
			.filter('toArray', toArray);

	function toArray() {
		return function (obj) {
			if (!(obj instanceof Object)) return obj;
			var objArr = _.map(obj, function (obj, key) {
				obj.key = key;
				return obj;
			});
			objArr.sort(function (a, b) {
				return a.order - b.order;
			});
			return objArr;
		}
	}

})();

