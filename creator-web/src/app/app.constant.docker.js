(function () {
    'use strict';

    angular.module('app')
        .constant('ApiEndpointURL', '$API_ENDPOINT_URL')
        .constant('FacebookAppId', $FACEBOOK_APP_ID)
        .constant('SiteURL', '$SITE_URL')
        .constant('firebaseConfig', {
            apiKey: '$FIREBASE_APIKEY',
            authDomain: '$FIREBASE_AUTH_DOMAIN',
            databaseURL: '$FIREBASE_DATABASE_URL',
            projectId: '$FIREBASE_PROJECT_ID',
            storageBucket: '$FIREBASE_STORAGE_BUCKET',
            messagingSenderId: '$FIREBASE_MESSAGING_SENDER_ID'})
            .constant('enableSID', '$ENABLE_SID')
})();
