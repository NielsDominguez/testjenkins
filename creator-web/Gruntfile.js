module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    uglify: {
      options: {
        mangle: false
      },
      dist: {
        files: {
          'public/js/main.min.js': ['public/js/main.js']
        }
      }
    },
    concat: {
      dist: {
        src: [
          'src/js/jquery.min.js',
          'src/js/jquery-ui.min.js',
          'src/js/lodash.min.js',
          'src/js/moment-with-locales.min.js',
          'src/js/image-compressor.min.js',
          'src/js/angular.min.js',
          'src/js/angular-ui-router.js',
          'src/js/angular-sanitize.min.js',
          'src/js/ng-csv.min.js',
          'src/js/elastic.js',
          'src/js/rzslider.js',
          'src/js/ng-file-upload.min.js',
          'src/js/sortable.js',
          'src/js/clipboard.min.js',
          'src/js/ngclipboard.min.js',
          'src/js/angular-cookies.min.js',
          'src/js/Chart.bundle.min.js',
          'src/js/angular-chart.min.js',
          'src/js/firebase.js',
          'src/js/angularfire.min.js',
          'src/js/ap-mesa.min.js',
          'src/app/app.module.js',
          'src/app/app.run.js',
          'src/app/app.config.js',
          'src/app/filters/*.js',
          'src/app/factory/*.js',
        ],
        dest: 'public/js/main.js',
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', [ 'concat', 'uglify' ]);
};