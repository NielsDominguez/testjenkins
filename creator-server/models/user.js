const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    user_id: { type: String, index: true },
    bot_id: String,
    FACEBOOK: String,
    create_at: { type: String, index: true },
    last_update: String,
    attr: {
        first_name: String,
        last_name: String,
        profile_pic: String,
        locale: String,
        timezone: Number,
        gender: String,
        id: String,
        flow_block_list: Array,
        flow_block: String,
        flow_node: String,
        flow_node_state: String
    }
});

module.exports = function (connection) {
    return connection.model('User', userSchema, 'Users');
};