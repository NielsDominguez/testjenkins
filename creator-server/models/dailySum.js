const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dailySumSchema = Schema({
	time: Date,
	bot_id: String,
	activeUsers: [String],
	countNewUser: Number,
	countMessageIn: Number,
	countMessageOut: Number
});

module.exports = function (connection) {
    return connection.model('dailySum', dailySumSchema, 'dailySum');
};