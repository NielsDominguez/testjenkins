const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const botSchema = Schema({
    channels: {
        FACEBOOK: {
            id          :String,
            name        :String,
            accessToken :String
        },
        LINE: {
            accessToken :String
        },
        API: {
            accessToken :String
        },
        CHATFUEL: {
            accessToken :String
        }
    },
    bot_id   :String,
    bot_name :String,
    admins   :[String],
    api_token:String,
    web_hook_url: String,
    web_hook_custom_headers: { 
        key :String, 
        value: String
    },
    comment_rules: Array,
});

module.exports = function (connection) {
    return connection.model('Bots', botSchema, 'Bots');
};