const config = require('../env');
const Mongoose = require("mongoose").Mongoose;

let tunnelConn = new Mongoose();
tunnelConn.connect(config.MONGO_URI);
tunnelConn.set("debug", true);

let externalLogConn = new Mongoose();
externalLogConn.connect(config.MONGO_EXTERNAL_LOG_URI);
externalLogConn.set("debug", true);

let botPermissionConn = new Mongoose();
botPermissionConn.connect(config.MONGO_BOT_PERMISSION_URL);
botPermissionConn.set("debug", true);

module.exports.AuthUser = require("./auth_user")(tunnelConn);
module.exports.Bots = require("./db")(tunnelConn);
module.exports.Log = require("./log")(tunnelConn);
module.exports.User = require("./user")(tunnelConn);
module.exports.DailySum = require("./dailySum")(externalLogConn);
module.exports.BotPermission = require("./botPermission")(botPermissionConn);

