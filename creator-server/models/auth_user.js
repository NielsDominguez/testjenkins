const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs');

const authUserSchema = mongoose.Schema({
    uid      : { type: String, index: true },
    email    :String,
    firstName:String,
    lastName :String,
    facebook :{
        id   :String,
        token:String,
        name :String
    }
});

module.exports = function (connection) {
    return connection.model('Auth_User', authUserSchema, 'Auth_User');
};
