const mongoose = require('mongoose');

const botPermissionSchema = mongoose.Schema({
    bot_id: String,
    start_block: String,
    default_block: Array,
    ai: Array,
    livechat: {
        start_keyword: String,
        stop_keyword: String,
        timeout: String,
        enable: Boolean,
    },
});

module.exports = function (connection) {
    return connection.model('bots', botPermissionSchema, 'bots');
};