const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logSchema = Schema({
    bot_id: String,
    msg_id: String,
    msg: String,
    set_attributes: String,
    msg_type: String,
    from_module_name: String,
    from_module_id: String,
    user_id: String,
    channel: String,
    replyRef: String,
    buttons: Array,
    attr: Schema.Types.Mixed,
    sent_at: Date
});

module.exports = function (connection) {
    return connection.model('Log', logSchema, 'messages');
};