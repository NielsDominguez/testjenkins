var db = require("../models");
var mongoose = require('mongoose');
var FB = require('fb');
var https = require('https');
const config = require('../env');
module.exports = function (router, passport, firebase) {
    function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }
    router.get("/longLiveToken", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        console.log(token, 'TOKEN......')
        authenFirebase(token)
            .then(function (user) {
                console.log(user, 'user...')
                FB.api('oauth/access_token', 'get', {
                    client_id: config.facebook.clientID,
                    client_secret: config.facebook.clientSecret,
                    grant_type: 'fb_exchange_token',
                    fb_exchange_token: req.query.token
                }, function (response) {
                    console.log(response, 'response.. from FB api')
                    if (!response || response.error) {
                        console.log(!response ? 'error occurred' : response.error);
                    } else {
                        db.AuthUser.update(
                            { uid: user.uid },
                            {
                                $set: {
                                    facebook: { token: response.access_token, id: req.query.userId }
                                }
                            }, (err) => {
                                if (err) {
                                    return res.status(500).send('Something went wrong');
                                }
                                res.send(response.access_token)
                            });
                    }
                });
            }).catch(err => res.status(400).send(err))

    });

    router.get("/user/facebook", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.AuthUser.findOne({
                    uid: user.uid
                }, (err, data) => {
                    if (err) {
                        return res.status(500).send('Something went wrong');
                    }
                    if (!data) {
                        return res.send({});
                    }

                    return res.send(data.facebook);
                })
            });
    });

    router.delete("/user/facebook", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.AuthUser.update(
                    { uid: user.uid },
                    {
                        $set: {
                            facebook: {}
                        }
                    }, (err) => {
                        if (err) {
                            return res.status(500).send('Something went wrong');
                        }
                        res.send()
                    });
            });
    });
}

