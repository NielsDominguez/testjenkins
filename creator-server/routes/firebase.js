const monk = require('monk');
const admin = require("firebase-admin");
const config = require('../env');

var flowDB = monk(config.MONGO_FLOW_URL);
var botPermissionDB = monk(config.MONGO_BOT_PERMISSION_URL);
var botsCollection = botPermissionDB.get('bots');
var broadcastCollection = flowDB.get('broadcast');

var firebaseDatabase = undefined;

function convertBlocksToBlockIds(blocks) {
	let block_ids = [];
	if (blocks != null && Array.isArray(blocks)) {
		for (let i = 0 ; i < blocks.length ; i++) {
			if (!blocks[i] || !blocks[i].hasOwnProperty('block_id') || blocks[i].block_id == '')
				continue;
			block_ids.push(blocks[i].block_id);
		}
	}
	return block_ids;
}

function convertButtons(buttons) {
	if (buttons != null && Array.isArray(buttons)) {
		for (let i = 0 ; i < buttons.length ; i++) {
			if (!buttons[i] || !buttons[i].hasOwnProperty('type') || buttons[i].type != 'show_block') 
				continue;
			buttons[i].block_ids = convertBlocksToBlockIds(buttons[i].blocks);
		}
	}
	return buttons;
}

function handleChangeForBotId(botId) {
	firebaseDatabase.ref('bots/' + botId).once('value', function (data) {
		const value = data.val();
		console.log('value: ' + value);
		if (value !== null) {
			parseData(value, botId);
		} else {
			firebaseDatabase.ref('isUpdate/' + botId).remove(function () {
				console.log('delete ' + botId + ' due to nonexistent');
			});
		}
	});
}

function parseData(data, botId) {
	try {
			let bot = data;

			if (typeof(bot.bot_id) == 'number')
				bot.bot_id = bot.bot_id.toString();

			console.log(" [X] FirebaseListener Update bot name : ", bot.bot_id);
			let bot_update = {
				'bot_id': bot.bot_id,
				'start_block': bot.start_block,
				'default_block': bot.default_block,
				'ai': [],
				'livechat': bot.livechat,
			};

			//Loop ai
			if (bot.hasOwnProperty('ai') && bot.ai) {
				for (let j = 0 ; j < bot.ai.length ; j++) {
					bot_update.ai.push({
						'block_ids': convertBlocksToBlockIds(bot.ai[j].blocks),
						'texts': bot.ai[j].texts,
						'filters': bot.ai[j].filters
					});
				}
			}

			botsCollection.findOneAndUpdate({'bot_id':bot.bot_id}, bot_update).then((updated) => {
				if (updated == null || updated.hasOwnProperty('lastErrorObject')) {
					botsCollection.insert(bot_update);
					console.log(" [X] FirebaseListener Insert bot ", bot.bot_id, " config done.");
				} else {
					console.log(" [X] FirebaseListener Update bot ", bot.bot_id, " config done.");
				}
			});

			//Loop blocks
			let blocks_name = Object.keys(bot.blocks);
			for (let j = 0 ; j < blocks_name.length ; j++) {
				let block = bot.blocks[blocks_name[j]];

				if (block.hasOwnProperty('nodes')) {
					let nodes_name = Object.keys(block.nodes);
					for (let k = 0 ; k < nodes_name.length ; k++) {
						let node = block.nodes[nodes_name[k]];
						if (node.hasOwnProperty('buttons')) {
							//Normal node
							node.buttons = convertButtons(node.buttons);
						} else if (node.hasOwnProperty('nodeResponse')) {
							if (node.nodeResponse.type == 'button' || node.nodeResponse.type == 'location') {
								if (node.nodeResponse.type == 'button') {
									//Quick reply
									node.nodeResponse.response = convertButtons(node.nodeResponse.response);
								} else {
									//Location
									node.nodeResponse.response = 'location';
								}

								for (let l = 0 ; l < nodes_name.length ; l++) {
									if (!block.nodes[nodes_name[l]].hasOwnProperty('directions'))
										continue;

									let direction = block.nodes[nodes_name[l]].directions[0];
									if (!direction)
										continue;

									if (typeof(direction) != 'string')
										direction = direction.goto;

									if (direction == ('N' + node.node_id)) {
										block.nodes[nodes_name[l]].quick_reply = node.nodeResponse.response;
										block.nodes[nodes_name[l]].quick_reply_check_ai = node.use_ai;
										if (node.hasOwnProperty('attr_name'))
											block.nodes[nodes_name[l]].quick_reply_attr = node.attr_name;

										if (node.hasOwnProperty('directions'))
											block.nodes[nodes_name[l]].directions = node.directions;
										else
											block.nodes[nodes_name[l]].directions = [];
										break;
									}
								}
							} else if (node.nodeResponse.type == 'carousel' &&
								node.nodeResponse.hasOwnProperty('response') && 
								Array.isArray(node.nodeResponse.response)) {
								//Carousel
								let carousel = node.nodeResponse.response;
								for (let l = 0 ; l < carousel.length ; l++) {
									let item = carousel[l];
									if (item.hasOwnProperty('buttons'))
										node.nodeResponse.response[l].buttons = convertButtons(item.buttons);
								}
							}
						}

						block.nodes[nodes_name[k]] = node;
					}
				}

				flowDB.get(botId).findOneAndUpdate({'block_id':block.block_id}, block).then((updated) => {
					if (updated == null || updated.hasOwnProperty('lastErrorObject')) {
						flowDB.get(botId).insert(block);
						console.log(" [X] FirebaseListener Insert flow of bot ", botId, " block_id :", block.block_id, " done");
					} else {
						console.log(" [X] FirebaseListener Update flow of bot ", botId, " block_id :", block.block_id, " done");
					}
				});
			}

			if (!bot.hasOwnProperty('broadcast'))
				return;

			//Loop broadcast
			let broadcasts_name = Object.keys(bot.broadcast);
			for (let j = 0 ; j < broadcasts_name.length ; j++) {
				let broadcast = bot.broadcast[broadcasts_name[j]];
				if (!broadcast || broadcast == null)
					continue;

				flowDB.get('broadcast').findOneAndUpdate({'bot_id':broadcast.bot_id, 'broadcast_id':broadcast.broadcast_id}, broadcast).then((updated) => {
					if (updated == null || updated.hasOwnProperty('lastErrorObject')) {
						flowDB.get('broadcast').insert(broadcast);
						console.log(" [X] FirebaseListener Insert broadcast id ", broadcast.broadcast_id, " config done.");
					} else {
						console.log(" [X] FirebaseListener Update broadcast id ", broadcast.broadcast_id, " config done.");
					}
				});
			}
	} catch (e) {
		console.error(e);
	}
}

class FirebaseListener {
	constructor() {
		let serviceAccount = require(config.firebase.serviceAccountKey);
		this.firebaseApp = admin.initializeApp({
			credential: admin.credential.cert(serviceAccount),
			databaseURL: config.firebase.databaseURL
		});
		firebaseDatabase = this.firebaseApp.database();
		console.log(" [X] FirebaseListener Initial done");
	}

	start() {
		if (firebaseDatabase == undefined) {
			console.log(" [X] FirebaseListener can't connect to Firebase server");
			return;
		}
		firebaseDatabase.ref('isUpdate').on('value', function (data) {
			const isUpdates = data.val();

			if (isUpdates !== null) {
				for (let key in isUpdates) {
					if (isUpdates[key] === true) {
						firebaseDatabase.ref('isUpdate/' + key).set(false, function () {
							console.log('Successfully set isUpdate to false');
						});
						handleChangeForBotId(key);
					}
				}

			}
		});
	}


}

module.exports = { 
	FirebaseListener:FirebaseListener 
};