var db = require("../models");
const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = function (router, passport, firebase) {
    // signup
    router.post("/signup", function (req, res) {
        console.log('signup');
        authenFirebase(req.header('Authorization'))
            .then(function (decodeToken) {
                console.log(decodeToken);
                if (decodeToken.uid != req.body.uid) {
                    return res.status(401).send();
                }
                var newUser = new db.AuthUser();
                newUser.uid = decodeToken.uid;
                newUser.email = req.body.email;
                newUser.save(function (err, user) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    return res.json(user);
                });
            }, function (err) {
                return res.status(500).send(err);
            });
    });

    function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }

    // =============================================================================
    // AUTHENTICATE (FIRST LOGIN) ==================================================
    // =============================================================================

    // // Facebook auth routes
    router.get('/auth/facebook', function (req, res, next) {
        next();
    },
        passport.authenticate('facebook', { authType: 'rerequest', scope: ['manage_pages', 'pages_messaging'] })
    );

    router.get('/auth/facebook/callback', function (req, res, next) {
        console.log('callback')
        var authenticator = passport.authenticate('facebook', {
            successRedirect: '/#/dashboard',
            failureRedirect: '/'
        });

        authenticator(req, res, next);
    })

    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================
    // facebook -------------------------------
    // send to facebook to do the authentication
    router.get('/connect/facebook', function (req, res, next) {
        next();
    },
        passport.authorize('facebook', { scope: ['manage_pages', 'pages_messaging'] })
    );

    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // used to unlink accounts. for social accounts, just remove the token
    // for local account, remove email and password
    // user account will stay active in case they want to reconnect in the future

    // facebook -------------------------------
    router.get('/unlink/facebook', isLoggedIn, function (req, res) {
        db.AuthUser.findOne({ 'username': req.user.username }, function (err, user) {
            if (user) {
                user.facebook.token = undefined;
                user.save(function (err) {
                    res.redirect('/#' + req.query.returnTo);
                });
            } else {
                res.json(null);
                return;
            }
        });
    });


    // End module    
};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}