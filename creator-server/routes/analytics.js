const hbot = require('hbot-con');
const moment = require('moment');
const db = require("../models");
const csv = require('csv-express');
module.exports = function(router, firebase) {

    router.get('/bot/:botId/report/summary', function(req, res) {
        const botId = req.params.botId;

        let now = moment();
        if (!botId) {
            res.sendStatus(400);
            return;
        }

        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function(user) {
                db.Bots.findOne({
                    bot_id: botId,
                    admins: user.uid
                }).then((user) => {
                    if (!user) {
                        res.status(401).send('User unauthorize');
                        return Promise.reject();
                    }
                }).then(() => {
                    let thirtyDayBefore = moment().subtract(30, "day");
                    console.log(thirtyDayBefore.toDate());
                    return db.DailySum.find({
                        bot_id: botId,
                        time: { $gte: thirtyDayBefore.toDate() }
                    }).sort({ time: 1 });
                }).then((dailySums) => {
                    console.log(dailySums);
                    if (!dailySums) {
                        res.send({ result: [] });
                    } else {
                        res.send({ result: dailySums.map(getReportResponseFromDailySum) });
                    }
                });
            }, function() {
                res.sendStatus(401);
            });
    });

    function getReportResponseFromDailySum(dailySum) {
        return {
            time: dailySum.time,
            bot_id: dailySum.bot_id,
            countNewUser: dailySum.countNewUser,
            countMessageIn: dailySum.countMessageIn,
            countMessageOut: dailySum.countMessageOut,
            activeUserCount: dailySum.activeUsers.length
        };
    }

    function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }

    router.get('/bot/:botId/report/user', function(req, res) {
        const botId = req.params.botId;
        const size = req.query.limit || 10;
        const page = req.query.page || 0;

        db.User.count({ bot_id: botId }, function(err, totalCount) {
            if (err) {
                res.status(500).send('Error fetching data');
            }

            db.User.find({ bot_id: botId })
                .skip((page - 1) * size)
                .limit(size)
                .sort('-create_at')
                .select('user_id bot_id attr create_at last_update FACEBOOK')
                .then(users => {
                    const totalPages = Math.ceil(totalCount / size);
                    let result = {
                        users: users,
                        totalPages: totalPages,
                        totalUsers: totalCount
                    }
                    res.send(result);
                })
                .catch(err => {
                    console.error('report user; bot id = ' + botId, err);
                    res.status(500).send('Something went wrong. Please try again.');
                });
        })
    });

    router.post('/bot/:botId/report/user', function(req, res) {
        const botId = req.params.botId;
        const size = req.query.limit || 10;
        const page = req.query.page || 0;
        const filter = req.body.filter || {};

        let query = {};
        if (filter.filters) {
            let condition;
            for (var index = filter.filters.length - 1; index >= 0; index--) {
                const eachFilter = filter.filters[index];
                console.log(eachFilter);
                if (!hasFilterParameter(eachFilter)) {
                    break;
                }
                const conn = getConnection(eachFilter.connection);
                const cond = getCondition(eachFilter.operation);

                let tempConnection = {};
                tempConnection[conn] = [];
                let tempCondition = createCondition(eachFilter, cond);

                if (condition) {
                    condition[getConnection(filter.filters[index + 1].connection)].push(tempCondition);
                    tempConnection[conn].push(condition);
                } else {
                    tempConnection[conn].push(tempCondition);
                }
                condition = tempConnection;
            }
            if (condition) {
                query = condition.$and[0];
            }
        }

        if (filter.date && filter.date.from && filter.date.to) {
            query.last_update = {
                $gte: moment(filter.date.from).valueOf(),
                $lte: moment(filter.date.to).valueOf()
            }
        }
        query.bot_id = botId;
        db.User.count(query, function(err, totalCount) {

            if (err) {
                res.status(500).send('Error fetching data');
            }

            db.User.find(query)
                .skip((page - 1) * size)
                .limit(size)
                .sort('-create_at')
                .select('user_id bot_id attr create_at last_update FACEBOOK')
                .then(users => {
                    const totalPages = Math.ceil(totalCount / size);
                    let result = {
                        users: users,
                        totalPages: totalPages,
                        totalUsers: totalCount
                    }
                    res.send(result);
                })
                .catch(err => {
                    console.error('report user; bot id = ' + botId, err);
                    res.status(500).send('Something went wrong. Please try again.');
                });
        })
    });

    router.get('/bot/:botId/report/allUser', function(req, res) {
        const botId = req.params.botId;

        db.User.find({ bot_id: botId })
            .sort('-create_at')
            .select('user_id bot_id attr create_at last_update FACEBOOK')
            .then(result => {
                result.map(r => {
                    r.create_at = convertDateTime(r.create_at);
                    r.last_update = convertDateTime(r.last_update);
                })

                let resultString = mapAttribute(result)
                res.attachment('users.csv');
                res.send(resultString);
            })
            .catch(err => {
                console.error('report user; bot id = ' + botId, err);
                res.status(500).send('Something went wrong. Please try again.');
            });
    });

    router.get('/bot/:botId/report/filterUser', async function(req, res) {
        const botId = req.params.botId;
        const filterUser = req.query.filterUser || {};
        const filter = JSON.parse(filterUser);

        let query = {};
        if (filter.filters) {
            let condition;
            for (var index = filter.filters.length - 1; index >= 0; index--) {
                const eachFilter = filter.filters[index];
                console.log(eachFilter);
                if (!hasFilterParameter(eachFilter)) {
                    break;
                }
                const conn = getConnection(eachFilter.connection);
                const cond = getCondition(eachFilter.operation);

                let tempConnection = {};
                tempConnection[conn] = [];
                let tempCondition = createCondition(eachFilter, cond);

                if (condition) {
                    condition[getConnection(filter.filters[index + 1].connection)].push(tempCondition);
                    tempConnection[conn].push(condition);
                } else {
                    tempConnection[conn].push(tempCondition);
                }
                condition = tempConnection;
            }
            if (condition) {
                query = condition.$and[0];
            }
        }

        if (filter.date && filter.date.from && filter.date.to) {
            query.last_update = {
                $gte: moment(filter.date.from).valueOf(),
                $lte: moment(filter.date.to).valueOf()
            }
        }
        query.bot_id = botId;

        db.User.find(query)
            .sort('-create_at')
            .select('user_id bot_id attr create_at last_update FACEBOOK')
            .then(result => {
                result.map(r => {
                    r.create_at = convertDateTime(r.create_at);
                    r.last_update = convertDateTime(r.last_update);
                })

                let resultString = mapAttribute(result)
                res.attachment('users.csv');
                res.send(resultString);
            })
            .catch(err => {
                console.error('report user; bot id = ' + botId, err);
                res.status(500).send('Something went wrong. Please try again.');
            });
    });

    router.get('/bot/:botId/report/user/new', function(req, res) {
        const botId = req.params.botId;
        const todayDate = moment().toDate();

        db.User.find({ bot_id: botId, create_at: { $gte: todayDate } })
            .sort('-create_at')
            .select('user_id bot_id attr create_at FACEBOOK')
            .then(result => {
                res.send(result);
            })
            .catch(err => {
                console.error('report user; bot id = ' + botId, err);
                res.status(500).send('Something went wrong. Please try again.');
            });
    });

    router.get('/bot/:botId/clearAttrAll', function(req, res) {
        const botId = req.params.botId;
        let query = {};

        query.bot_id = botId;

        db.User.
        update(query, { $unset: { attr: 1 } }, false, function(err, user) {
            // after mongodb is done updating, you are receiving the updated file as callback
            console.log('2');
            // now you can send the error or updated file to client
            if (err)
                return res.send(err);

            //return res.json({ message: 'User updated!' });
        });

        db.User.
        update(query, { $unset: { control_attr: 1 } }, false, function(err, user) {
            // after mongodb is done updating, you are receiving the updated file as callback
            console.log('2');
            // now you can send the error or updated file to client
            if (err)
                return res.send(err);

            //return res.json({ message: 'User updated!' });
        });

        res.json({ message: 'Attribute reset!' });

    });

    router.get('/bot/:botId/clearAttr', function(req, res) {
        const botId = req.params.botId;
        
        let query = {};

        query.bot_id = botId;
        clearAttr(query,res);

        //res.json({ message: 'Attribute reset!' });
    });

    router.get('/bot/:botId/clearAttrPerUser/:fbId', function(req, res) {
        const botId = req.params.botId;
        const fbId = req.params.fbId;
        
        let query = {};

        query.bot_id = botId;
        query.attr = {fb_id:fbId};

        var queryStr = JSON.parse('{ "attr.fb_id": "'+fbId+'" , "bot_id": "'+botId+'" }');

        console.log(queryStr);
        removeUser(queryStr,res);

        //res.json({ message: 'Attribute reset!' });
    });

    router.get('/bot/:botId/delete/:fbId', function(req, res) {
        const botId = req.params.botId;
        const fbId = req.params.fbId;
        
        let query = {};

        query.bot_id = botId;
        query.attr = {fb_id:fbId};

        var queryStr = JSON.parse('{ "attr.fb_id": "'+fbId+'" , "bot_id": "'+botId+'" }');

        return removeUser(queryStr,res);
        
        //removeUser(queryStr,res);
        //res.json({ message: 'Attribute reset!' });
    });

    async function removeUser(q,res) {
        let docs = await db.User.remove(q);
        return res.json({ message: 'User removed!',q: q });
    }

    async function clearAttr(query,res) {

        await db.User.
        update(query, { $unset: { control_attr: 1 } }, false, function(err, user) {
            // after mongodb is done updating, you are receiving the updated file as callback
            console.log('2');
            // now you can send the error or updated file to client
            if (err)
                return res.send(err);

            //return res.json({ message: 'User updated!' });
        });

        unset = [];

        await db.User.find(query).stream()
            .on('data',  async function(doc) {
                // handle docs
                console.log("THE_DOC_DOT");
                console.log(doc.attr);
                console.log("THE_DOC_ARRAY");
                console.log(doc['attr']);
                // docs.forEach((doc) => {
                    

                    if(doc['attr'] != 'undefined')
                    for (k in doc['attr']) {
                        //console.log("THE_ATTR_IS");
                        //console.log(k);
                        if (k === 'fb_id') continue;
                        if (k === 'user_id') continue;
                        if (k === 'first_name') continue;
                        if (k === 'last_name') continue;
                        if (k === 'profile_pic') continue;
                        if (k === 'locale') continue;
                        if (k === 'time_zone') continue;
                        if (k === 'gender') continue;
                        // if (k === '$init') continue;
                        // if (k === 'flow_node_state') continue;
                        // if (k === 'flow_node') continue;
                        // if (k === 'flow_block') continue;
                        // if (k === 'flow_block_list') continue;
                        // if (k === 'is_continue_message') continue;
                        // if (k === 'block_jump_timestamp') continue;
                        // if (k === 'id') continue;
                        
                        

                        // //unset['attr.' + k] = 1;
                        // //doc.update({ $unset: {attr:k} });
                        // //doc.update({}, {$unset: {k:1}} , {multi: true});
                        //setTimeout(func, 500);

                        var attrSub = "attr."+k;
                        unset[attrSub] = 1;
                        
                    }

                    //await doc.update({},{$unset: {[attrSub]:1}},{multi: true},{ w : 0 });

                    console.log("attr._id:" + doc._id + " attr: "+k+" reset !");

                    // if(doc['control_attr'] != 'undefined')
                    // for (k2 in doc['control_attr']) {
                    //     // if (k2 === '$init') continue;
                    //     // if (k2 === 'flow_node_state') continue;
                    //     // if (k2 === 'flow_node') continue;
                    //     //setTimeout(func, 500);

                    //      var attrSub2 = "control_attr."+k2;
                    //      await doc.update({},{ $unset: {[attrSub2]:1} },{multi: true},{ w : 0 });
                    //     console.log("control_attr._id:" + doc._id + " attr: "+k+" reset !");
                    // }
                    
                //});
                
                
            })
            .on('error', function(err) {
                // handle error
                // if (err)
                //     return res.send(err);
            })
            .on('end',  async function() {
                // final callback
                console.log("END END END");
                
                var realUnsetObj = { "attr.flow_block_list": 1,
"attr.link_account_result": 1,
"attr.cars_count": 1,
"attr.2ndrefuel": 1,
"attr.register": 1,
"attr.record_count": 1,
"attr.car_count": 1,
"attr.count_access": 1,
"attr.status_user": 1,
"attr.register_result": 1 };

                var unsetObj = Object.assign({}, unset);
                //console.log(unsetObj);

                   await db.User.updateMany(query,{
                        $unset: realUnsetObj}, {multi: true}, function(err, obj) {
            // after mongodb is done updating, you are receiving the updated file as callback
            console.log('111');
            console.log(obj);

            //return res.json({ message: 'User updated!' });
        });

//                 await db.User.update(query,{$unset: { "attr.link_account_result": 1}}, {multi: false}, function(err, obj) {console.log("222");console.log(obj)});

// await db.User.update(query,{$unset: { "attr.cars_count": 1}}, {multi: false}, function(err, obj) {console.log("333");console.log(obj)});

// await db.User.update(query,{$unset: { "attr.2ndrefuel": 1}}, {multi: false}, function(err, obj) {console.log("444");console.log(obj)});

// await db.User.update(query,{$unset: { "attr.register": 1}}, {multi: false}, function(err, obj) {console.log("555");console.log(obj)});

// await db.User.update(query,{$unset: { "attr.record_count": 1}}, {multi: false}, function(err, obj) {console.log("666");console.log(obj)});

// await db.User.update(query,{$unset: { "attr.car_count": 1}}, {multi: false}, function(err, obj) {console.log("777");console.log(obj)});


// await db.User.update(query,{$unset: { "attr.count_access": 1}}, {multi: false}, function(err, obj) {console.log("888");console.log(obj)});


// await db.User.update(query,{$unset: { "attr.status_user": 1}}, {multi: false}, function(err, obj) {console.log("999");console.log(obj)});


// await db.User.update(query,{$unset: { "attr.register_result": 1}}, {multi: false}, function(err, obj) {console.log("000");console.log(obj)});



                 await db.User.updateMany(query, { $unset: { control_attr: 1 } }, false, function(err, attr) {
                    // after mongodb is done updating, you are receiving the updated file as callback
                    console.log(attr);
                    // now you can send the error or updated file to client
                    // if (err)
                    //     return res.send(err);

                    //return res.json({ message: 'User control_attr updated!' });
                    //return res.json({ message: 'User attr updated!' });
                });
            });


    }

    function func() {
        console.log('Do stuff here');
    }

    router.get('/bot/:botId/clearAllUser', function(req, res) {
        const botId = req.params.botId;
        let query = {};
        query.bot_id = botId;

        db.User.remove(query);
        return res.json({ message: 'All User removed!' });
    });

    async function mapAttr(result) {
        const mapResult = result.map(r => {
            let csv = {};
            for (let key in r) {
                if (key !== 'attr') {
                    csv[key] = r[key];
                } else {
                    for (let attrKey in r['attr']) {
                        csv[attrKey] = r['attr'][attrKey];
                    }
                }
            }
            return csv;
        });
    }

    function hasFilterParameter(filter) {
        return filter.connection && filter.first_var && filter.second_var && filter.operation;
    }

    function getConnection(conn) {
        return connectionConstant[conn] || "$and";
    }

    function getCondition(cond) {
        return conditionConstant[cond] || "$eq";
    }

    function createCondition(filter, cond) {
        let condition = {};
        condition[`attr.${filter.first_var}`] = {}
        condition[`attr.${filter.first_var}`][cond] = filter.second_var;
        return condition;
    }

    const connectionConstant = {
        "and": "$and",
        "or": "$or"
    }

    const conditionConstant = {
        "=": "$eq",
        "!=": "$ne",
        ">": "$gt",
        "<": "$lt",
        "<=": "$lte",
        ">=": "$gte"
    }
};


function convertDateTime(timestamp) {
    var date = new Date(timestamp * 1000);
    var dateString = date.toDateString();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    var dateTime = dateString + " " + "|" + " " + strTime;

    return dateTime;
}

function objectToStringCsv(target, columnNames) {
    let result = "";
    for (let columnName of columnNames) {
        let value = String(target[columnName] || "");

        value = value.replace(/\n/g, ' ').replace(/,/g, '');
        result = result + value + ",";
    }
    result = result + "\n";
    return result;
}



function mapAttribute(result) {
    let columnNames = [];
    result = result.map(r => { return r.toObject(); });
    const mapResult = result.map(r => {
        let csv = {};
        for (let key in r) {
            if (key !== 'attr') {
                if (columnNames.indexOf(key) === -1) {
                    columnNames.push(key);
                }
                csv[key] = r[key];
            } else {
                for (let attrKey in r['attr']) {
                    if (columnNames.indexOf(attrKey) === -1) {
                        columnNames.push(attrKey);
                    }
                    csv[attrKey] = r['attr'][attrKey];
                }
            }
        }
        return csv;
    });

    let resultString = columnNames.reduce((prev, curr) => {
        return prev + curr + ","
    }, "") + "\n";

    mapResult.forEach((result) => {
        resultString = resultString + objectToStringCsv(result, columnNames);
    });

    return resultString;
}