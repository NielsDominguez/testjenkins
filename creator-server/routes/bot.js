var db = require("../models");
var mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');
const ObjectId = mongoose.Types.ObjectId;
const channel = require('./fb');
const unirest = require('unirest');

const generator = require('generate-password');
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const passIndex = 30;
const passLength = 16;

function encrypt(text){
    let password = generator.generate({ length: passLength, numbers: true });
    let cipher = crypto.createCipher(algorithm, password)
    let crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    crypted = crypted.substr(0, passIndex) + password + crypted.substr(passIndex);
    return crypted;
}

function decrypt(text){
    let password = text.substr(passIndex, passLength);
    text = text.substr(0, passIndex) + text.substr(passIndex + passLength);
    let decipher = crypto.createDecipher(algorithm, password)
    let dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
}

module.exports = function (router, passport, firebase) {
    function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }

    router.get("/bot/:botId/export", function (req, res) {
        console.log("Export backup for bot id ", req.params.botId);
        let token = req.header('Authorization');
        if (!token) {
           return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                let firebaseDB = firebase.database();
                firebaseDB.ref('bots')
                    .child('bot_' + req.params.botId)
                    .once('value').then(data => {
                        let bot = data.val();
                        if (bot.users[user.uid] == undefined || !bot.users[user.uid]) {
                           return res.status(401).send('Permission denied.');
                        }

                        let exportData = {};

                        console.log(req.query);
                        
                        if (Boolean(req.query.flow)) {
                            exportData.blocks = bot.blocks;
                            exportData.default_block = bot.default_block;
                            exportData.groups = bot.groups;
                            exportData.start_block = bot.start_block;
                        }

                        if (Boolean(req.query.ai)) {
                            //if (req.query.flow == "f") {
                                for (let i = 0 ; i < bot.ai.length ; i++) {
                                    let ai = bot.ai[i];
                                    if (ai.blocks) {
                                        ai.blocks = [];
                                    }
                                    bot.ai[i] = ai;
                                }
                            //}
                            exportData.ai = bot.ai;
                        }

                        let encrypted = encrypt(JSON.stringify(exportData));
                        //let encrypted = JSON.stringify(exportData);

                        console.error(encrypted);

                        res.set({'Content-Disposition': 'attachment; filename="backup.hbot"'});
                        res.send(encrypted);
                    });
            },
            function() {
                console.log("Token : " + token);
                return res.status(401).send('Unauthorize');
            });
    });

    router.post("/bot/:botId/import", function (req, res) {
        console.log("Import from backup for bot id ", req.params.botId);
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                let firebaseDB = firebase.database();
                firebaseDB.ref('bots')
                    .child('bot_' + req.params.botId)
                    .once('value').then(bot => {
                        bot = bot.val();
                        if (bot.users[user.uid] == undefined || !bot.users[user.uid]) {
                            return res.status(401).send('Permission denied.');
                        }
                        
                        let data = req.body.data;
                        let decrypted = decrypt(data);
                        try {
                            let importData = JSON.parse(decrypted);

                            if (importData.blocks)
                                bot.blocks = importData.blocks;
                            if (importData.start_block)
                                bot.start_block = importData.start_block;
                            if (importData.default_block)
                                bot.default_block = importData.default_block;
                            if (importData.groups)
                                bot.groups = importData.groups;
                            if (importData.ai)
                                bot.ai = importData.ai;
                            if (importData.broadcast)
                                bot.broadcast = importData.broadcast;

                            console.log(bot);
                            
                            bot.is_update = true;
                            firebaseDB.ref('bots').child('bot_' + req.params.botId).update(bot);
                            return res.send("success");
                        } catch (e) {
                            console.error(e);
                            return res.status(402).send('Data error');
                        }
                    });
            },
            function() {
                console.log("Token : " + token);
                return res.status(401).send('Unauthorize');
            });
    });

    router.get("/bot", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.Bots.aggregate([
                    {
                        $match: {
                            admins: user.uid
                        }
                    }, {
                        $lookup: {
                            from: "Auth_User",
                            localField: "admins",
                            foreignField: "uid",
                            as: "admins"
                        }
                    }
                ], function (err, bots) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Something went wrong.');
                    }
                    res.json(bots);
                });
            }, function () {
                return res.status(401).send('Unauthorize');
            });
    });

    router.get("/bot/:botId", function (req, res) {
        console.log('Get bot');
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.Bots.aggregate([
                    {
                        $match: {
                            admins: user.uid,
                            bot_id: req.params.botId
                        }
                    }, {
                        $limit: 1
                    }, {
                        $lookup: {
                            from: "Auth_User",
                            localField: "admins",
                            foreignField: "uid",
                            as: "admins"
                        }
                    }
                ], function (err, bots) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Something went wrong.');
                    }

                    if (!bots) {
                        return res.send();
                    }

                    res.send(bots[0]);
                });
            }, function () {
                return res.status(401).send('Unauthorize');
            });
    });

    router.delete("/bot/:botId/revoke", function (req, res) {
        console.log("revoke bot id : ", req.params.botId);
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.Bots.findOne({
                    bot_id: req.params.botId,
                    admins: user.uid
                }, function (err, bot) {
                    if (err) {
                        return res.status(500).send('Something went wrong.');
                    }

                    if (!bot) {
                        return res.status(404).send('Bot not found');
                    }

                    bot.api_token = uuidv4();
                    bot.save(function (err, bot) {
                        if (err) {
                            console.log(err);
                            res.status(500).send('Unable to reset api token');
                            return;
                        }
                        res.send(bot.api_token);
                    });
                });
            }, function () {
                return res.status(401).send('Unauthorize');
            });
    });

    router.post("/bot", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                var newBot = new db.Bots();
                newBot.bot_name = req.body.name;
                newBot.admins = [user.uid];
                newBot.channels.FACEBOOK.name = req.body.facebook_page_name || '';
                newBot.channels.FACEBOOK.accessToken = req.body.facebook_page || '';
                newBot.channels.FACEBOOK.id = req.body.facebook_id || '';
                newBot.channels.LINE.accessToken = req.body.line_page || '';
                newBot._id = ObjectId();
                newBot.bot_id = newBot._id.toHexString();
                newBot.api_token = uuidv4();
                newBot.save(function (err, bot) {
                    if (err) {
                        return res.status(500).send('Something went wrong.');
                    }
                    res.json(bot);
                });
            }, function () {
                return res.status(401).send('Unauthorize');
            });
    });

    router.delete("/bot/:botId", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (user) {
                db.Bots.findOne({
                    bot_id: req.params.botId,
                    admins: user.uid
                }, function (err, bot) {
                    if (err) {
                        return res.status(500).send('Something went wrong.');
                    }

                    if (!bot) {
                        return res.status(404).send('Bot not found');
                    }
                    channel.disconnectFacebookPage(bot);

                    bot.remove(function (err) {
                        if (err) {
                            return res.status(500).send('Internal server error.');
                        }
                        res.status(200).send();
                    });

                });
            });
    });

    router.put("/bot/:botId/admin", function (req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (owner) {
                if (owner.email === req.body.email) {
                    return res.status(404).send('This user has already existed in this bot.');
                }

                db.AuthUser.findOne({ email: req.body.email }, function (err, user) {
                    if (err) {
                        return res.status(500).send('Something went wrong.' + err);
                    }

                    if (!user) {
                        return res.status(404).send('User not found');
                    }

                    db.Bots.updateOne({
                        bot_id: req.params.botId,
                        admins: owner.uid
                    }, {
                            $addToSet: {
                                admins: user.uid
                            }
                        }, function (err) {
                            if (err) {
                                return res.status(500).send('Something went wrong.' + err);
                            }

                            db.Bots.aggregate([
                                {
                                    $match: {
                                        bot_id: req.params.botId,
                                        admins: owner.uid
                                    }
                                }, {
                                    $limit: 1
                                }, {
                                    $lookup: {
                                        from: "Auth_User",
                                        localField: "admins",
                                        foreignField: "uid",
                                        as: "admins"
                                    }
                                }
                            ], function (err, bots) {
                                if (err) {
                                    return res.status(500).send('Something went wrong.' + err);
                                }

                                if (!bots) {
                                    return res.status(404).send('Bot not found');
                                }
                                let bot = bots[0];
                                let admins = {};
                                admins[user.uid] = true;

                                let db = firebase.database();
                                let ref = db.ref(`bots/bot_${req.params.botId}/users`);
                                ref.update(admins, function (err) {
                                    if (err) {
                                        bot.admins = bot.admins.filter(function (admin) {
                                            return admin != user.uid;
                                        });

                                        bot.save(function (err, bot) {
                                            if (err) {
                                                console.log(err);
                                                return res.status(500).send('Something went wrong.' + err);
                                            }
                                        });
                                        return res.status(500).send('Something went wrong.');
                                    }
                                    return res.send(bot);
                                });
                            });
                        });
                });
            });
    });

    router.post("/bot/:botId/webHook", function (req, res) {
        var webHookUrl = req.body.url;
        var headerKey = req.body.headerKey;
        var headerValue = req.body.headerValue;
        var apiToken = req.body.apiToken;
        var header = {};
        if (headerKey) {
            header[headerKey] = headerValue;
        }
        unirest.get(webHookUrl, header, {}, function(response) {
            if (response.code != 200 || response.body != apiToken) {
                res.sendStatus(400); 
            } else {
                let token = req.header('Authorization');
                if (!token) {
                    return res.status(401).send('User unauthorize');
                }
                authenFirebase(token)
                    .then(function (data) {
                        db.Bots.update({
                            bot_id: req.params.botId
                        },{ $set: {
                            web_hook_url: webHookUrl,
                            web_hook_custom_headers: { 
                                key: headerKey, 
                                value: headerValue
                             },
                    }}, (err) => {
                        if(!err) {
                            res.send("success");
                        } else {
                            res.sendStatus(500);
                        }
                    })
                    })
            }
        })

    });

    router.delete("/bot/:botId/admin", function (req, res) {
        console.log("remove admin :", req.query.uid);
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (owner) {
                db.Bots.updateOne({
                    bot_id: req.params.botId,
                    admins: owner.uid
                }, {
                        $pull: {
                            admins: req.query.uid
                        }
                    }, function (err) {
                        if (err) {
                            return res.status(500).send('Something went wrong.');
                        }
                        console.log("hay");
                        db.Bots.aggregate([
                            {
                                $match: {
                                    bot_id: req.params.botId,
                                    admins: owner.uid
                                }
                            }, {
                                $limit: 1
                            }, {
                                $lookup: {
                                    from: "Auth_User",
                                    localField: "admins",
                                    foreignField: "uid",
                                    as: "admins"
                                }
                            }
                        ], function (err, bots) {
                            if (err) {
                                return res.status(500).send('Something went wrong.' + err);
                            }

                            if (!bots) {
                                return res.status(404).send('Bot not found');
                            }
                            console.log(bots);
                            let bot = bots[0];
                            let admins = {};
                            admins[req.query.uid] = null;
                            let db = firebase.database();
                            let ref = db.ref(`bots/bot_${req.params.botId}/users`);
                            ref.update(admins, function (err) {
                                if (err) {
                                    bot.admins.push(req.query.uid);
                                    bot.save(function (err, bot) {
                                        if (err) {
                                            return res.status(500).send('Unable to invite user.');
                                        }
                                        return res.status(500).send('Something went wrong.');
                                    });
                                }
                                return res.send(bot);
                            });
                        });
                    });
            });
    });

    router.post("/bot/:botId/livechat", authen, function (req, res) {
        if(!req.params.botId){
            console.error(req.url, 'Bot id not found')
            return res.status(500).send('Something went wrong.');
        }

        db.BotPermission.findOneAndUpdate({
            bot_id: req.params.botId
        }, {
            livechat: {
                start_keyword: req.body.startKeyword,
                stop_keyword: req.body.stopKeyword,
                timeout: req.body.timeout,
                enable: req.body.enable,
            }
        }, (err, botPermission) => {
            if (err) {
                console.error(req.url, 'Something went wrong.')
                return res.status(500).send('Something went wrong.');    
            }
            res.send('success');            
        });
    });

    router.get("/bot/:botId/livechat", authen, function (req, res) {
        if(!req.params.botId){
            console.error(req.url, 'Bot id not found')
            return res.status(500).send('Something went wrong.');
        }

        db.BotPermission.findOne({
            bot_id: req.params.botId
        }, (err, botPermission) => {
            if (err) {
                console.error(req.url, 'Something went wrong.')
                return res.status(500).send('Something went wrong.');    
            }
            res.send(botPermission.livechat);            
        });
    });

    router.post("/bot/enable/:botId", authen, function (req, res) {
        if (!req.params.botId) {
            console.error(req.url, 'Bot id not found');
            return res.status(405).send('Bot id not found');
        }

        let enable = false;
        if (req.body.enable)
            enable = true;

        db.Bots.findOneAndUpdate({bot_id: req.params.botId}, {enable_bot:enable}, (err, bot) => {
            if (err) {
                console.error(req.url, "Error when query bot :", err);
                return res.status(500).send("Error when query bot");
            }
            res.send('success');
        });
    });

    router.get("/bot/enable/:botId", authen, function (req, res) {
        if (!req.params.botId) {
            console.error(req.url, 'Bot id not found');
            return res.status(405).send('Bot id not found');
        }

        db.Bots.findOne({bot_id: req.params.botId}, (err, bot) => {
            if (err) {
                console.error(req.url, "Error when query bot :", err);
                return res.status(500).send("Error when query bot");
            }
            if (bot.enable_bot)
                return res.send('enabled');
            res.send('disabled');
        });
    });

    router.post("/bot/comment/enable/:botId", authen, function (req, res) {
        if (!req.params.botId) {
            console.error(req.url, 'Bot id not found');
            return res.status(405).send('Bot id not found');
        }

        let enable = false;
        if (req.body.enable)
            enable = true;

        db.Bots.findOneAndUpdate({bot_id: req.params.botId}, {enable_comment:enable}, (err, bot) => {
            if (err) {
                console.error(req.url, "Error when query bot :", err);
                return res.status(500).send("Error when query bot");
            }
            res.send('success');
        });
    });

    router.get("/bot/comment/enable/:botId", authen, function (req, res) {
        if (!req.params.botId) {
            console.error(req.url, 'Bot id not found');
            return res.status(405).send('Bot id not found');
        }

        db.Bots.findOne({bot_id: req.params.botId}, (err, bot) => {
            if (err) {
                console.error(req.url, "Error when query bot :", err);
                return res.status(500).send("Error when query bot");
            }
            if (bot.enable_comment)
                return res.send('enabled');
            res.send('disabled');
        });
    });

    /* 
     * Request body
     * {
     *     "rule_name": "XXXXXX"
	 *     "post_id": "123422323_21213131212", //Can be {post id} or "all"
	 * 	   "keywords": [  //If empty mean reponse for all comment
	 *        "hello",
	 *        "haha",
	 *        ...
	 *     ],
     *     "reply_with": "comment" //Can be "comment", "inbox" or "all"
	 *     "response": "asdasdfasdfasdf" //Max 300 characters
	 * }
     * Response
     * string id of rule
     */
    router.post("/bot/comment/rule/:botId", authen, function (req, res) {
        if (!req.params.botId) {
            console.error(req.url, 'Bot id not found');
            return res.status(405).send('Bot id not found');
        }

        let id = req.params.botId + "_" + Date.now();
        let rule = req.body
        rule.rule_id = id;
        db.Bots.findOne({bot_id: req.params.botId}, (err, bot) => {
            if (err || !bot) {
                console.error(req.url, "Error when query bot :", err);
                return res.status(500).send("Error when query bot");
            }
            if (!bot.comment_rules || !Array.isArray(bot.comment_rules)) {
                bot.comment_rules = [];
            }
            bot['comment_rules'].push(rule);
            
            db.Bots.findOneAndUpdate({bot_id: bot.bot_id}, bot, (err, updated) => {
                if (err) {
                    console.error(req.url, "Error when update bot :", err);
                    return res.status(500).send("Error when update bot");
                }

                res.send(id);
            });
        });
    });

    /*
    * Toggle rule
    */
    router.put("/bot/comment/rule/:ruleId", authen, function (req, res) {
        if (!req.params.ruleId) {
            console.error(req.url, 'Rule id not found');
            return res.status(405).send('Rule id not found');
        }

        let botId = req.params.ruleId.split("_")[0];
        let ruleBody = req.body;

        db.Bots.findOne({bot_id: botId}, (err, bot) => {

            for (let i = 0 ; i < bot.comment_rules.length ; i++) {
                if (bot.comment_rules[i].rule_id == req.params.ruleId) {
                    bot.comment_rules[i] = ruleBody
                    break;
                }
            }
            
            db.Bots.findOneAndUpdate({bot_id: bot.bot_id}, {comment_rules:bot.comment_rules}, (err, updated) => {
                if (err) {
                    console.error(req.url, "Error when update bot :", err);
                    return res.status(500).send("Error when update bot");
                }

                res.send('success');
            });
        });
    })

    router.delete("/bot/comment/rule/:ruleId", authen, function (req, res) {
        if (!req.params.ruleId) {
            console.error(req.url, 'Rule id not found');
            return res.status(405).send('Rule id not found');
        }

        let botId = req.params.ruleId.split("_")[0];
        console.log(req.params.ruleId);
        
        db.Bots.findOne({bot_id: botId}, (err, bot) => {
            let rules = [];
            for (let i = 0 ; i < bot.comment_rules.length ; i++) {
                let rule = bot.comment_rules[i];
                if (rule.rule_id == req.params.ruleId)
                    continue;
                rules.push(rule);
            }
            db.Bots.findOneAndUpdate({bot_id: bot.bot_id}, {comment_rules:rules}, (err, updated) => {
                if (err) {
                    console.error(req.url, "Error when update bot :", err);
                    return res.status(500).send("Error when update bot");
                }

                res.send('success');
            });
        });
    });

    function authen(req, res, next){
        let token = req.header('Authorization');
        if (!token) {
            console.error(req.url, 'User unauthorized')
            return res.status(401).send('User unauthorized');
        }
        firebase.auth().verifyIdToken(token).then((user) => {
            req.user = user;
            next()
        }).catch(() => {
            console.error(req.url, 'User unauthorized')
            return res.status(401).send('User unauthorized');
        });
    }
}
