var configAuth = require('../config/oauth');
var db = require("../models");
var FB = require('fb');
var https = require('https');

function callFBgraphAPI(endpoint, body, callback) {
    const host = "graph.facebook.com";
    const headers = {
        'Content-Type': 'application/json'
    };
    const jsonBody = JSON.stringify(body);
    const options = {
        hostname: host,
        path: endpoint,
        method: "POST",
        headers: headers
    };

    let req = https.request(options, function (res) {
        res.setEncoding('utf-8');
        let responseString = '';

        res.on('data', function (data) {
            responseString += data;
        });

        res.on('end', function () {
            if (callback) {
                callback(null, responseString);
            }
        });
    }).on('error', (e) => {
        console.error(`Got error: ${e.message}`);

        if (callback) {
            callback(e.message, null);
        }
    });

    req.write(jsonBody);
    req.end();
}

module.exports = { init, disconnectFacebookPage }
function init(app, passport, firebase) {
    app.post('/graph/facebook/subscribed_apps', function subscribed_apps(req, res) {
        FB.options({ version: 'v2.9' });
        FB.options({ accessToken: req.body.access_token });

        var body = 'object=page&callback_url=https%3A%2F%2Fconnect.unicornonzen.com%2Ffacebook%2Fcallback&fields=page&verify_token=rF5S9zAZjxppsTuX';
        page_id = req.body.page_id;
        page_token = req.body.access_token;
        post_url = page_id + "/subscribed_apps/?access_token=" + page_token;
        FB.api(post_url, 'post', {"subscribed_fields":"messages, message_echoes, message_deliveries, messaging_optins, messaging_optouts, messaging_postbacks, message_reads, messaging_page_feedback", message: body }, function (facebook_res) {
            if (!facebook_res || facebook_res.error) {
                console.error(!facebook_res ? 'error occurred' : facebook_res.error);
                res.status(500).send('Something went wrong. Please try again. \n' + JSON.stringify(facebook_res.error));
                return;
            }
            db.Bots.update({
                bot_id: req.body.bot_id
            }, {
                    $set: {
                        channels: {
                            FACEBOOK: {
                                id: page_id,
                                name: req.body.page_name,
                                accessToken: page_token
                            }
                        }
                    }
                }, (err) => {
                    if (!err) {
                        res.send("success");
                    } else {
                        res.sendStatus(500);
                    }
                })
        });
    });

    app.post('/graph/facebook/get_started', function get_started(req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize');
        }
        authenFirebase(token)
            .then(function (data) {
                if (!req.body.accessTokenPage) {
                    return res.status(500).send('Have no access token')
                }
                if (req.body.accessTokenPage) {
                    var endpoint = "/v2.6/me/messenger_profile?access_token=" + req.body.accessTokenPage;
                    var body = {
                        "get_started": { "payload": "from_get_started" },
                        "greeting": [
                            {
                                "locale": "default",
                                "text": "Hello! I'm " + req.body.pageName + " chatbot."
                            }
                        ]
                    };
                    callFBgraphAPI(endpoint, body, function (err, response) {
                        if (err) {
                            console.error(err);
                            res.status(500).send("Something went wrong. Please try again.");
                        }
                        res.send(response);
                    });
                }
            }, function (err) {
                console.error(err);
                res.status(401).send('User unauthorize.');
            });
    });

    app.delete('/channel/:channel', function unsubscribed_apps(req, res) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(401).send('User unauthorize.');
        }

        authenFirebase(token)
            .then(function (user) {
                let botId = req.query.bot_id;
                if (!botId) {
                    return res.status(400).send('No bot id in request');
                }
                db.Bots.findOne({
                    bot_id: botId,
                    admins: user.uid
                }, function (err, bot) {
                    if (err) {
                        return res.status(500).send('Internal server error');
                    }

                    disconnectFacebookPage(bot, function (err, data) {
                       // edit 30/11/2018 about some user cannot disconnect page
			// if (err) {
                         //   console.error(err);
                           // return res.status(500).send('Something went wrong. Please try again.');
                       // }

                        bot.channels.FACEBOOK = {};
                        bot.save(function (err, bot) {
                            if (err) {
                                res.status(500).send('Internal server error');
                            }
                            res.status(200).send();
                        });
                    });
                });
            });
    });
    function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }
};

function disconnectFacebookPage(bot, callback) {
    if (!callback) {
        return;
    }

    page_token = bot.channels.FACEBOOK.accessToken;
    page_id = bot.channels.FACEBOOK.id;

    if (!page_token || !page_id) {
        callback(null, true);
        return;
    }

    FB.options({ version: 'v3.2' });
    FB.options({ accessToken: page_token });

    let del_url = page_id + "/subscribed_apps/?access_token=" + page_token;
    FB.api(del_url, 'delete', function (facebook_res) {
        callback(facebook_res.error, facebook_res.data);
    });
}


