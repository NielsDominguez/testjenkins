var db = require("../models");
var mongoose = require('mongoose');
var FB = require('fb');
var https = require('https');
const config = require('../env');
const token = "1212312121";
var admin = require("firebase-admin");

// const MongoClient = require('mongodb').MongoClient

//var serviceAccount = require("./serviceAccountKey.json");
// var serviceAccount = require(config.firebase.serviceAccountKey)

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://bot-platform-dev.firebaseio.com"
// });

module.exports = function(router, passport, firebase) {

    console.log(config.MONGO_BOT_PERMISSION_URL);

    const main = async () => {
        console.log(config.MONGO_BOT_PERMISSION_URL);
        db = await MongoClient.connect(config.MONGO_BOT_PERMISSION_URL)
        //m = name => db.collection(name)
        db.auth_user = await db.find({});
        console.log("THE MAIN");
        console.log(db.auth_user);
        // db.users = db.collection('users')
        // let ret = await db.collection('posts').aggregate([
        //   { $lookup: { from: 'users', localField: 'user_id', foreignField: 'id', as: 'user' } },
        //   { $unwind: '$user' },
        //   { $unwind: '$property_ids' },
        //   { $lookup: { from: 'properties', localField: 'property_ids', foreignField: 'id', as: 'properties' } },

        //   // {$unwind: '$properties'},
        //   { $limit: 1 }
        // ]).toArray()



        // let ret = await Post.query()

        // console.log(ret);
        // db.news.update({}, {$rename: {categoyIds: 'categoryIds'}})
        // db.collection('news').updateMany({id: {$lt: 5}}, {$set: {categoyIds: [1,2,3]}})
        // db.collection('news').updateMany({id: {$gte: 5}}, {$set: {categoyIds: [3,4,5]}})
    }

    async function authenFirebase(token) {
        return firebase.auth().verifyIdToken(token);
    }

    router.get("/firebase-user", function(req, res) {
        // let token = req.header('Authorization');
        // if (!token) {
        //     return res.status(401).send('User unauthorize');
        // }
        // authenFirebase(token)
        //     .then(function(user) {

        admin.auth().listUsers(10)
            .then(function(listUsersResult) {
                res.json(listUsersResult);
                if (listUsersResult.pageToken) {
                    // List next batch of users.
                    listAllUsers(listUsersResult.pageToken)
                }
            })
            .catch(function(error) {
                console.log("Error listing users:", error);
            });

        // });
    });

    //EAAadxs0eBZBUBAJ2CH4OFwJuAeaUg2VCa3fULGm94ARzaMhFQwmomeAIpqIaWV12EXZBtlhg7gdOk4dnxXajdzJkoJkKOedIofjZCzJzFykyDPd0S4asfGMrIksLe1BDTk1vuWqpnATttkE4X5ZAiXWzZAVB94c4ZD

    router.get("/auth-user-email", async function(req, res) {
        var email = req.query.email;
        the_user = await getUserByEmail(email);
        if (the_user.firebase)
            res.json(the_user);
        else
            res.json({ "error": "user not found" });
    });

    const asyncMiddleware = fn =>
        (req, res, next) => {
            Promise.resolve(fn(req, res, next))
                .catch(next);
        };

    router.get("/auth-user", asyncMiddleware(async (req, res, next) => {
        if (req.query.token != "1212312121")
            res.json({ "error": "token is no valid" });

        var result = { "users": [] };

        try {
            data = await db.AuthUser.find({}).limit(2000);
            console.log("===THE DATA====")
            console.log(data);

            for (let i = 0; i <= data.length; i++) {
                var user = data[i];

                if (user) {
                    //the_user = await getUserByEmail(user.email);
                    the_user = await getUserByUid(user.uid);
                    if(the_user) {
                        the_user.mongo = user;
                        result.users.push(the_user);
                    } 
                }
            }
            var real_result = [];
            //var fetch_users = result.users;
            //res.json(fetch_users);
            //console.log(fetch_users);
            var users = await result.users.map(item => {
                if(item && item.mongo)
                return {
			uid: item.mongo.uid || "",
                    email: item.mongo.email || "",
                    fb_id: item.mongo.facebook.id || "",
                    fb_token: item.mongo.facebook.token || "",
                    // lastSignInTime: item.firebase.metadata.lastSignInTime || "",
                    // creationTime: item.firebase.metadata.creationTime || "",
                    bots: item.bots.map(bot => {
                        if(bot.channels.FACEBOOK)
                        return {
                            bot_id: bot._id,
                            bot_name: bot.bot_name,
                            bot_fb_id: bot.channels.FACEBOOK.id || "",
                            bot_fb_page_name: bot.channels.FACEBOOK.name || "",
                            bot_fb_page_access_token: bot.channels.FACEBOOK.accessToken || ""
                        }
                    })
                };
            });
            res.json({ "users": users });
            // fetch_users.forEach(async (item) => {
            //     var obj = {};
            //     //obj.item = item;
            //     obj.uid = item.firebase.uid;
            //     obj.email = item.firebase.email;
            //     obj.fb = item.mongo.facebook;
            //     obj.lastSignInTime = item.firebase.metadata.lastSignInTime;
            //     obj.creationTime = item.firebase.metadata.creationTime;
            //     obj.bots = item.bots;
            //     await real_result.push(obj);
            //     console.log(real_result);
            // });
            // res.json(real_result);

        } catch (e) {
            next(e);
        }
    }));


    async function getUserByEmail(email) {
        var the_user = {};
        try {
            let userRecord = await admin.auth().getUserByEmail(email);
            bots = await db.Bots.aggregate([{
                $match: {
                    admins: userRecord.uid
                }
            }, {
                $lookup: {
                    from: "Auth_User",
                    localField: "admins",
                    foreignField: "uid",
                    as: "admins"
                }
            }]);

            the_user.firebase = userRecord;
            the_user.bots = bots;
            return the_user;
        } catch (e) {
            the_user.firebase = null;
            the_user.bots = null;
        }
    }

    async function getUserByUid(uid) {
        var the_user = {};
        try {
            //let userRecord = await admin.auth().getUserByEmail(email);
            bots = await db.Bots.aggregate([{
                $match: {
                    admins: uid
                }
            }, {
                $lookup: {
                    from: "Auth_User",
                    localField: "admins",
                    foreignField: "uid",
                    as: "admins"
                }
            }]);

            //the_user.firebase = userRecord;
            the_user.bots = bots;
            return the_user;
        } catch (e) {
            the_user.firebase = null;
            the_user.bots = null;
        }
    }

    function listAllUsers(nextPageToken) {
        // List batch of users, 1000 at a time.
        admin.auth().listUsers(1000, nextPageToken)
            .then(function(listUsersResult) {

                listUsersResult.users.forEach(function(userRecord) {
                    //console.log("user", userRecord.toJSON());

                });
                if (listUsersResult.pageToken) {
                    // List next batch of users.
                    listAllUsers(listUsersResult.pageToken)
                }
            })
            .catch(function(error) {
                console.log("Error listing users:", error);
            });
    }

}
