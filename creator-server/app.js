#!/usr/bin/env nodejs
// Express
var express = require("express")
var app = express()
var expressWs = require("express-ws")(app)

const io = require("@pm2/io")

io.init({
  transactions: true, // will enable the transaction traci
  http: true // will enable metrics about the http server (optional)
})

// Init Firebase
const FirebaseListener = require("./routes/firebase.js").FirebaseListener
var firebase = new FirebaseListener()

// Passport
var passport = require("passport")
require("./config/passport")(passport, firebase.firebaseApp) // pass passport for configuration

// Cookie and session
var cookieParser = require("cookie-parser")
var session = require("express-session")
app.use(
  session({
    secret: "this is the secret"
  })
)
app.use(cookieParser())
app.use(passport.initialize())
app.use(passport.session())

// Body-parser
var bodyParser = require("body-parser")
app.use(
  bodyParser.json({
    limit: "5mb"
  })
) //for parsing application/json
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

// Allow Cross Origin
const config = require("./env")

var cors = require("cors")

// use it before all route definitions
//app.use(cors({origin: 'https://my.hbot.io'}));

app.use(function(req, res, next) {
  //if (config.ACCESS_CONTROL_ALLOW_ORIGINS.indexOf("*") != -1) {
  if (true) {
    res.header("Access-Control-Allow-Origin", "*")
  } else {
    let matchedOriginName = config.ACCESS_CONTROL_ALLOW_ORIGINS.find(each => {
      return each === req.headers.origin
    })

    if (matchedOriginName) {
      res.header("Access-Control-Allow-Origin", matchedOriginName)
    } else {
      res.header(
        "Access-Control-Allow-Origin",
        config.ACCESS_CONTROL_ALLOW_ORIGINS[0]
      )
    }
  }
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  )
  res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
  next()
})

var router = express.Router()
// routes
require("./routes/auth.js")(router, passport, firebase.firebaseApp) // load our routes and pass in our app and fully configured passport
require("./routes/bot.js")(router, passport, firebase.firebaseApp)
require("./routes/fb.js").init(router, passport, firebase.firebaseApp)
require("./routes/analytics.js")(router, firebase.firebaseApp)
require("./routes/user.js")(router, passport, firebase.firebaseApp)
require("./routes/bot-user.js")(router, passport, firebase.firebaseApp)

app.use("/v1", router)

app.get("/health-check", function(req, res) {
  res.sendStatus(200)
})

app.listen(5000)
firebase.start()
