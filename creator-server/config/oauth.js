const config = require('../env');
module.exports = {
    facebookAuth: {
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: "https://cms-ssh.unicornonzen.com/auth/facebook/callback",
        enableProof: false
    }
}
