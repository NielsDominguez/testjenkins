var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var configAuth = require('./oauth');
var AuthUser = require('../models/auth_user');
var jwt = require('jsonwebtoken');
var passportJWT = require("passport-jwt");

var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "We need to change this";

module.exports = function (passport, firebase) {
    passport.serializeUser(function (user, done) {
        user.password = undefined;
        console.log("serializeUser ", user);
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use('local-login', new LocalStrategy(
        function (username, password, done) {
            AuthUser.findOne({
                username: username.toLowerCase()
            }, function (err, user) {
                // if there are any errors, return the error before anything else
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    return done(null, false);

                // if the user is found but the password is wrong
                if (!user.validPassword(password))
                    return done(null, false);
                
                firebase.auth().createCustomToken(`${user._id}`)
                .then(function(customToken) {

                    let userData = {
                        username: user.username,
                        _id: user._id,
                        facebook: user.facebook
                    }
                    let token = jwt.sign(userData, jwtOptions.secretOrKey, { expiresIn: "7d" });                
                    let editedUser = {
                        token: token,
                        firebaseToken: customToken
                    }
                    Object.assign(editedUser, user._doc)
                    
                  // Send token back to client
                  return done(null, editedUser);
                })
                .catch(function(error) {
                  console.log("Error creating custom token:", error);
                  return done(err);                  
                });
                
            });
        }
    ));

    passport.use('jwt', new JwtStrategy(jwtOptions, function (jwt_payload, done) {
        console.log('payload received', jwt_payload);
        // usually this would be a database call:
        AuthUser.findOne({
            _id: jwt_payload._id
        }, function (err, user) {
            // if there are any errors, return the error before anything else
            if (err) {
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        });
    }));

    // // Facebook strategy
    // passport.use(new FacebookStrategy({

    //    // pull in our app id and secret from our auth.js file
    //    clientID        : configAuth.facebookAuth.clientID,
    //    clientSecret    : configAuth.facebookAuth.clientSecret,
    //    callbackURL     : configAuth.facebookAuth.callbackURL

    // },

    // // facebook will send back the token and profile
    // function(token, refreshToken, profile, done) {
    //    // asynchronous
    //    process.nextTick(function() {

    //        // find the user in the database based on their facebook id
    //        User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

    //            // if there is an error, stop everything and return that
    //            // ie an error connecting to the database
    //            if (err)
    //                return done(err);

    //            // if the user is found, then log them in
    //            if (user) {
    //                console.log("found exist user: user.facebook.token = "+user.facebook.token);
    //                return done(null, user); // user found, return that user
    //            } else {
    //                // if there is no user found with that facebook id, create them
    //                var newUser            = new User();

    //                // set all of the facebook information in our user model
    //                newUser.facebook.id    = profile.id; // set the users facebook id                   
    //                newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
    //                newUser.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned

    //                // save our user to the database
    //                newUser.save(function(err) {
    //                    if (err)
    //                        throw err;

    //                    // if successful, return the new user
    //                    return done(null, newUser);
    //                });
    //            }

    //        });
    //    });

    // }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    var fbStrategy = configAuth.facebookAuth;
    fbStrategy.passReqToCallback = true;  // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    passport.use(new FacebookStrategy(fbStrategy,
        function (req, token, refreshToken, profile, done) {
            // asynchronous
            process.nextTick(function () {
                // check if the user is already logged in                
                if (!req.user) {
                    AuthUser.findOne({ 'facebook.id': profile.id }, function (err, user) {
                        if (err)
                            return done(err);

                        if (user) {
                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.facebook.token) {
                                user.facebook.token = token;
                                user.facebook.name = profile.displayName;

                                user.save(function (err) {
                                    if (err)
                                        return done(err);

                                    return done(null, user);
                                });
                            }
                            return done(null, user); // user found, return that user
                        }
                        else {
                            // if there is no user, create them
                            var newUser = new AuthUser();
                            newUser.facebook.id = profile.id;
                            newUser.facebook.token = token;
                            newUser.facebook.name = profile.displayName;
                            newUser.save(function (err) {
                                if (err)
                                    return done(err);

                                return done(null, newUser);
                            });
                        }
                    });
                }
                else {
                    // user already exists and is logged in, we have to link accounts
                    AuthUser.findOne({ 'username': req.user.username }, function (err, olduser) {
                        if (err)
                            return done(err);

                        if (olduser) {
                            olduser.facebook.id = profile.id;
                            olduser.facebook.token = token;
                            olduser.facebook.name = profile.displayName;
                            olduser.save(function (err) {
                                if (err)
                                    return done(err);

                                return done(null, olduser);
                            });
                        }
                        else {
                            console.log("no one found");
                        }
                    });
                }
            });
        }));
};