## Prerequisites
1. [NPM & NPM](https://nodejs.org/en/download/)
2. [mongodb](https://www.mongodb.com/)
3. Have fun.

## Config mongodb
- env.json

## Start server
1. Run mongodb `mongod` or `sudo mongod`
2. Start server `nodejs app.js`


## Production
`NODE_ENV=production node app.js`

## Ubuntu
```bash
sudo apt install nodejs
sudo apt install npm
npm install
nodejs app.js
```

## Notes
1. `env2` is a copy of `env`. Because it was untracked and I track it, but git does not 
see the deep root of it. Git just shallow add the file.