var env = process.env;

var targetEnv = {
    mongodb: {
        host: process.env.MONGODB_HOST,
        port: process.env.MONGODB_PORT,
        username: process.env.MONGODB_USR ? process.env.MONGODB_USR : undefined,
        password: process.env.MONGODB_PWD ? process.env.MONGODB_PWD : undefined,
        replicaSet: process.env.MONGODB_RS ? process.env.MONGODB_RS : undefined
    },
    ACCESS_CONTROL_ALLOW_ORIGINS: getAccessControlAllowOrigins(),
    facebook: {
	clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET
    },
    firebase: {
        databaseURL: process.env.FIREBASE_DATABASE_URL,
        serviceAccountKey : process.env.FIREBASE_SERVICE_ACCOUNT_FILE_PATH
    },
};

targetEnv.MONGO_URI = generateMongoUri(targetEnv.mongodb, '/Tunnel');
targetEnv.MONGO_EXTERNAL_LOG_URI = generateMongoUri(targetEnv.mongodb, '/externalLog');
targetEnv.MONGO_FLOW_URL = generateMongoUri(targetEnv.mongodb, '/Flows');
targetEnv.MONGO_BOT_PERMISSION_URL = generateMongoUri(targetEnv.mongodb, '/BotPermission');

console.log( "CORS: " + getAccessControlAllowOrigins());

module.exports = targetEnv;

console.log(targetEnv, 'targetEnv...')

function generateMongoUri(mongodbConfig, path) {
    let uri = "";
    if (mongodbConfig.username) {
        uri += 'mongodb://' + mongodbConfig.username + ':' + mongodbConfig.password + '@' + mongodbConfig.host + ':' + mongodbConfig.port + path + '?authMechanism=SCRAM-SHA-1&authSource=admin';
    } else {
        uri += 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + path;
    }

    if (mongodbConfig.replicaSet) {
        if (mongodbConfig.username) {
            uri += '&replicaSet=' + mongodbConfig.replicaSet;
        } else {
            uri += '?replicaSet=' + mongodbConfig.replicaSet;
        }
    }

    return uri;
}

function getAccessControlAllowOrigins() {
    return process.env.ACCESS_CONTROL_ALLOW_ORIGINS.split(',');
}
